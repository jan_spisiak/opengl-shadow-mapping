# Large-scale rendering using shadow mapping
Prototype of a OpenGL renderer to test various shadow mapping techniques.

The code is mostly procedural and simple (read dirty) as architecture changed very quickly with iterations.

Video here: https://www.youtube.com/watch?v=g67neN1GlZc

Lacks libs used to build (glm, sdl2, assimp, glew)

Shaders are in res/shaders/

Rendering system is in src/render/RenderSystem.*