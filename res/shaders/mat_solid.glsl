#version 430 core
#extension GL_ARB_shader_draw_parameters : enable

layout(binding = 0) uniform GeomPass {
	mat4 view_proj;
	mat4 shadow_mvp;
	mat4 real_mvp;
	vec4 params;
} pass;

struct PerObject {
	mat4 model;
	vec3 color;
	float smoothness;
};

layout(std140, binding = 1) uniform Object {
	PerObject[256] objs;
};
//layout(binding = 1) uniform Object {
//	mat4 model;
//	float smoothness;
//} obj;

#ifdef GLX_VERTEX_SHADER
layout(location = 0) in vec3 vert_pos;
layout(location = 1) in vec3 vert_normal;
layout(location = 3) in vec2 vert_texc;
layout(location = 4) in uint inst_id;

out VertexData {
	//vec3 pos_w;
	vec3 normal_w;
	//vec4 shadc;
	vec2 texc;
	flat int draw_id;
	flat uint inst_id;
} vout;

void main() {
	int draw_id = gl_InstanceID;
	mat4 model = objs[draw_id].model;
	//vout.pos_w = vec3(model * vec4(vert_pos, 1.0));
	vout.normal_w = mat3(model) * vert_normal;
	//vout.shadc = pass.shadow_mvp * vec4(vert_pos, 1.0); 
	vout.texc = vert_texc;
	vout.draw_id = draw_id;
	vout.inst_id = inst_id;
	gl_Position = pass.view_proj * model * vec4(vert_pos, 1.0);
}
#endif

#ifdef GLX_FRAGMENT_SHADER
in VertexData {
	//vec3 pos_w;
	vec3 normal_w;
	//vec4 shadc;
	vec2 texc;
	flat int draw_id;
	flat uint inst_id;
} vin;

out vec4 texel_albedo;
out vec4 texel_normal;

uniform sampler2D diffuse_tex;
uniform sampler2D normal_tex;
uniform sampler2D specular_tex;
uniform sampler2D shadow_tex;

void main() {
	vec3 normal_w = normalize(vin.normal_w);

	texel_albedo = vec4(objs[vin.draw_id].color, objs[vin.draw_id].smoothness);
	//vec3 color = vec3(float(vin.inst_id) / 100.0);
	//texel_albedo = vec4(color, 0.1);
	texel_normal = vec4(normal_w * 0.5 + 0.5, 1.0);

	//vec3 diffuse = vec3(frag_shadc.z - texture( shadow_tex, frag_shadc.xy ).r );
	//texel_albedo = vec4(vec3(texture( shadow_tex, frag_shadc.xy ).r), 1.0);
	//texel_albedo = vec4(vec3(mod(abs(vec2(frag_shadc.x, 0)), 0.5) / 0.5, 0.0), 1.0);
	//texel_albedo = vec4(vec3(mod(abs(visibility), 1.0)/1.0), 1.0);
	//texel_color = vec4(abs(normalize(bitangent_w+tangent_w)), 1.0);
}

#endif