#version 430 core

#ifdef GLX_VERTEX_SHADER
layout(location = 0) in vec2 position;
layout(location = 1) in vec2 texcoord;
out vec2 Texcoord;

void main() {
   Texcoord = texcoord;
   gl_Position = vec4(position, 0.0, 1.0);
}
#endif

#ifdef GLX_FRAGMENT_SHADER
in vec2 Texcoord;
out vec4 outColor;
layout(binding = 0)uniform sampler2D texFramebuffer;

void main() {
   outColor = texture(texFramebuffer, Texcoord);
}
#endif