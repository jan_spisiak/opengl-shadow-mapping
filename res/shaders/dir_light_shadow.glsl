#version 430 core

layout(binding = 0) uniform sampler2D depth_tex;
layout(binding = 1) uniform sampler2D normal_tex;
layout(binding = 2) uniform sampler2D albedo_tex;
layout(binding = 3) uniform sampler2DArrayShadow shadow_tex;
layout(binding = 4) uniform samplerCube probe_tex;

struct SunLight {
	vec4 color;
	vec3 direction;
	mat4 vp[4];
	vec4 splits;
	vec4 scales;
	float slider;
};

layout(std140, binding = 0) uniform LightPass {
	mat4 vp_inv;
	mat4 view_inv;
	vec3 lin_depth_coef;
	vec3 cam_pos_w;
	SunLight sun_light;
} pass;

struct PerLight {
	vec4 color;
	vec3 pos_w;
};

layout(std140, binding = 1) uniform Light {
	PerLight lights[256];
};

struct PerTile {
	uvec2 off_size;
};

layout(std140, binding = 2) uniform Tile {
	PerTile tiles[]; 
};


#ifdef GLX_VERTEX_SHADER
layout(location = 0) in vec2 vert_pos;
layout(location = 1) in vec2 vert_texc;
out vec2 frag_texc;
out vec3 frag_world_dir;
out vec3 frag_light_dir;

void main() {
	vec3 world_dir = vec3(pass.vp_inv * vec4(vert_pos, 1, 1));
	frag_texc = vert_texc;
	frag_world_dir = world_dir;
	//frag_light_dir = vec3(view_inv[3]);
	//frag_light_dir = vec3(pass.view_inv * vec4(world_dir, 0));
	gl_Position = vec4(vert_pos, 1.0, 1.0);
}
#endif

#ifdef GLX_FRAGMENT_SHADER
in vec2 frag_texc;
in vec3 frag_world_dir;
//in vec3 frag_light_dir;

out vec4 out_color;

float linearise_depth(float zLog)
{
	return pass.lin_depth_coef.x / (1 - zLog * pass.lin_depth_coef.y);
}

const float darkening_coefs[4] = {64.0, 186.0, 64.0, 256.0};
//float shadow_exp(vec3 shadow_coords, int spi)
//{
//	float light_depth = texture( shadow_tex, vec3(shadow_coords.xy, spi) ).r;
//	float depth_exp = darkening_coefs[spi];
//	return clamp(exp(depth_exp * (light_depth - shadow_coords.z)), 0.0, 1.0);
//}

const int smpl_count[4] = {3, 2, 2, 1};
//const int smpl_count[4] = {5, 4, 3, 1};
const float pcf_offset = 1.0 / 1024.0;
float shadow_pcf(vec3 shadow_coords, int spi)
{
	vec2 offx = vec2(pcf_offset, 0);
	vec2 offy = vec2(0, pcf_offset);
	const int smps = smpl_count[spi];
	float term;
	shadow_coords.xy += -vec2((smps - 1)/2.0 * pcf_offset);
	float wsum;
	for (int x=0; x < smps; ++x) {
		for (int y=0; y < smps; ++y) {
			vec2 coords = shadow_coords.xy + x * offx + y * offy;
			float w = 1.0;//0.5 - abs(length(vec2(x,y))/smps - 0.5);
			//float depth = texture(shadow_tex, vec3(coords, spi)).r;
			wsum += w;
			//term += w * step(shadow_coords.z, depth + 0.005);
			//term += w * texture(shadow_tex, vec4(coords, spi, shadow_coords.z));// - 0.0005));
			term += w * texture(shadow_tex, vec4(coords, spi, shadow_coords.z));
	}}
	return term / wsum;
}

//float off_scales[4] = {0.025, 0.08, 0.25, 0.5};
float off_scales[4] = {0.025, 0.08, 0.15, 0.3};
float shadow_term(vec3 frag_pos_w, vec3 frag_normal_w, vec3 light_dir_w, float cam_depth)
{
	int spi = 0;
	//for (int i=0; i<2; ++i)
		spi += int(step(pass.sun_light.splits[0], cam_depth));
		spi += int(step(pass.sun_light.splits[1], cam_depth));
		spi += int(step(pass.sun_light.splits[2], cam_depth));
		spi += int(step(pass.sun_light.splits[3], cam_depth));

	if (spi > 3)
		return 1.0;

	float cos_alpha = max(dot(frag_normal_w, light_dir_w), 0);
	float sin_alpha = sqrt(1 - cos_alpha * cos_alpha);
	float normal_off_coef = smpl_count[spi] * pass.sun_light.scales[spi] * sin_alpha;
	vec4 frag_shadc = pass.sun_light.vp[spi] * vec4(frag_pos_w + frag_normal_w * normal_off_coef + pass.sun_light.slider * light_dir_w * min(2, sin_alpha / cos_alpha), 1.0);

	vec3 shadow_coords = frag_shadc.xyz;
	if (shadow_coords.z > 1.0)
		return 0.0;
	return shadow_pcf(shadow_coords, spi);
	//return float(light_depth + 0.002 > shadow_coords.z);
}

vec3 point_light(vec3 dir_w, vec3 pos_w, vec3 norm_w, vec3 albedo, vec4 mat_par, vec4 light_color, vec3 light_pos_w) {
	vec3 frag_to_light = light_pos_w - pos_w;
	vec3 light_dir_w = normalize(frag_to_light);
	float NoL = dot(norm_w, light_dir_w);
	if (NoL > 0) {
		vec3 half_dir_w = normalize(light_dir_w - dir_w);
		float mNoL = max(NoL, 0);
		float mNoH = max(dot(half_dir_w, norm_w), 0);
		float spec_fact = pow(mNoH, mat_par.y) * mNoL * mat_par.x;

		//float visibility = shadow_term(pos_w, norm_w, light_dir_w, linear_depth / pass.lin_depth_coef.z);
		//float visibility = shadow_term(vec4(frag_pos_w, 1.0), linear_depth / pass.lin_depth_coef.z);
		vec3 specular = mat_par.w  * light_color.rgb * spec_fact; 
		vec3 diffuse = mat_par.z  * light_color.rgb * albedo * mNoL;

		float light_dist = length(frag_to_light);
		return (diffuse + specular) * light_color.a / (1 + light_dist * light_dist);
	}
	return vec3(0); 
}
vec3 directional_light(vec3 dir_w, vec3 pos_w, float linear_depth, vec3 norm_w, vec3 albedo, vec4 mat_par, vec4 light_color, vec3 light_dir_w) {
	float NoL = dot(norm_w, light_dir_w);
	if (NoL > 0 && light_color.a > 0) {
		vec3 half_dir_w = normalize(light_dir_w - dir_w);
		float mNoL = max(NoL, 0);
		float mNoH = max(dot(half_dir_w, norm_w), 0);
		float spec_fact = pow(mNoH, mat_par.y) * mNoL * mat_par.x;

		//float visibility = shadow_term(vec4(frag_pos_w, 1.0), linear_depth / pass.lin_depth_coef.z);
		vec3 specular = vec3(mat_par.w * spec_fact); 
		vec3 diffuse = mat_par.z * albedo * mNoL;
		float visibility = shadow_term(pos_w, norm_w, light_dir_w, linear_depth / pass.lin_depth_coef.z);

		return (diffuse + specular) * light_color.rgb * visibility * light_color.a;
	}
	return vec3(0);
}
vec3 probe_light(vec3 dir_w, vec3 pos_w, vec3 norm_w, vec3 albedo, vec4 mat_par) {
	// mat_par is km, kr, kd, ks
	vec3 probe_color = textureLod(probe_tex, norm_w, 0).rgb;
    vec3 diffuse = mat_par.z * probe_color * albedo * 0.16;
	return diffuse;
}

void main()
{
	vec3 norm_w = normalize(texture(normal_tex, frag_texc).xyz * 2 - 1);
	vec3 dir_w = normalize(frag_world_dir);
	float linear_depth = linearise_depth(texture(depth_tex, frag_texc).r);
	vec3 pos_w = frag_world_dir * linear_depth + pass.cam_pos_w;

	vec4 albedo_f = texture(albedo_tex, frag_texc);
	float mat_rough = 2/pow(1 - albedo_f.a, 4) - 2;
	//vec4 frag_shadc = light.shadow_vp * vec4(frag_pos_w, 1.0);
	//vec3 frag_shadc_b = frag_light_dir * linear_depth + vec3(light.cam_pos_l);
	//frag_shadc_b.xyz = frag_shadc_b.xyz
	//frag_shadc_b = frag_shadc_b / frag_shadc_b.w;

	//vec3 cs = max(ToLinear(cspec), vec3(0.01));
    //vec3 cd = ToLinear(cdiff) * (1.0 - cs);
	float ks = albedo_f.a;
	float kd = 1 - albedo_f.a;
	vec4 mat_par = vec4((mat_rough + 6) / 8, mat_rough, kd, ks);

	vec3 col;
	for (int i = 0; i < 16; ++i)
		col += point_light(dir_w, pos_w, norm_w, albedo_f.xyz, mat_par, lights[i].color, lights[i].pos_w);
	col += directional_light(dir_w, pos_w, linear_depth, norm_w, albedo_f.xyz, mat_par, pass.sun_light.color, pass.sun_light.direction);
	col += probe_light(dir_w, pos_w, norm_w, albedo_f.xyz, mat_par);

	out_color = vec4(col, 1.0);
	//out_color = vec4(vec3(mod(abs(frag_shadc.x), 1.0) / 1.0), 1.0);
	//out_color = vec4(vec3(linear_depth, 0.5, 1.0), 1.0);
}

#endif