#version 430



layout(std140, binding = 0) uniform SkyPass {
	mat4 vp_inv;
	vec3 sun_pos;
	vec3 cam_pos;
} pass;

layout(binding = 0) uniform sampler2D depth_tex;

#ifdef GLX_VERTEX_SHADER
layout(location = 0) in vec2 vert_pos;
layout(location = 1) in vec2 vert_texc;

smooth out vec3 v_pos_w;
smooth out vec3 v_dir_w;
out vec2 v_texc;

void main() {
    //mat4 inverseProjection = inverse(pass.proj);
    //mat3 inverseModelview = transpose(mat3(pass.view));
    //vec3 unprojected = (inverseProjection * vec4(vert_pos, 1, 1)).xyz;
	vec3 world_dir = vec3(pass.vp_inv * vec4(vert_pos, 1, 1));
    v_dir_w = world_dir;
	v_pos_w = world_dir * 100000.0; //128.0;
	v_texc = vert_texc;

    gl_Position = vec4(vert_pos, 1.0, 1.0);
} 
#endif

#ifdef GLX_FRAGMENT_SHADER

in vec3 v_pos_w;
in vec3 v_dir_w;
in vec2 v_texc;
out vec4 out_color;

uniform sampler2D skySampler;
//uniform vec3 sunPosition = vec3(0.5, 0.5, -0.01);

// uniform sampler2D sDiffuse;
const float turbidity = 10.0; //
const float reileigh = 2.; //
const float luminance = 1.0; //
const float mieCoefficient = 0.005;
const float mieDirectionalG = 0.8;

// uniform float luminance;
// uniform float turbidity;
// uniform float reileigh;
// uniform float mieCoefficient;
// uniform float mieDirectionalG;
// constants for atmospheric scattering
const float e = 2.71828182845904523536028747135266249775724709369995957;
const float pi = 3.141592653589793238462643383279502884197169;
const float n = 1.0003;
// refractive index of air
const float N = 2.545E25;
// number of molecules per unit volume for air at
						// 288.15K and 1013mb (sea level -45 celsius)
const float pn = 0.035;
// depolatization factor for standard air

// wavelength of used primaries, according to preetham
const vec3 lambda = vec3(680E-9, 550E-9, 450E-9);
// mie stuff
// K coefficient for the primaries
const vec3 K = vec3(0.686, 0.678, 0.666);
const float v = 4.0;
// optical length at zenith for molecules
const float rayleighZenithLength = 8.4E3;
const float mieZenithLength = 1.25E3;
const vec3 up = vec3(0.0, 1.0, 0.0);
const float EE = 1000.0;
const float sunAngularDiameterCos = 0.999956676946448443553574619906976478926848692873900859324;
// 66 arc seconds -> degrees, and the cosine of that

// earth shadow hack
const float cutoffAngle = pi/1.95;
//const float steepness = 1.5;
const float steepness = 3;
vec3 totalRayleigh(vec3 lambda)
{
    return (8.0 * pow(pi, 3.0) * pow(pow(n, 2.0) - 1.0, 2.0) * (6.0 + 3.0 * pn)) / (3.0 * N * pow(lambda, vec3(4.0)) * (6.0 - 7.0 * pn));
}


// see http://blenderartists.org/forum/showthread.php?321110-Shaders-and-Skybox-madness
// A simplied version of the total Reayleigh scattering to works on browsers that use ANGLE
vec3 simplifiedRayleigh()
{
	return 0.0005 / vec3(94, 40, 18);
	// return 0.00054532832366 / (3.0 * 2.545E25 * pow(vec3(680E-9, 550E-9, 450E-9), vec3(4.0)) * 6.245);
}

float rayleighPhase(float cosTheta)
{
	return (3.0 / (16.0*pi)) * (1.0 + pow(cosTheta, 2.0));
	//	return (1.0 / (3.0*pi)) * (1.0 + pow(cosTheta, 2.0));
	//	return (3.0 / 4.0) * (1.0 + pow(cosTheta, 2.0));
}

vec3 totalMie(vec3 lambda, vec3 K, float T)
{
	float c = (0.2 * T ) * 10E-18;
	return 0.434 * c * pi * pow((2.0 * pi) / lambda, vec3(v - 2.0)) * K;
}


float hgPhase(float cosTheta, float g)
{
	return (1.0 / (4.0*pi)) * ((1.0 - pow(g, 2.0)) / pow(1.0 - 2.0*g*cosTheta + pow(g, 2.0), 1.5));
}


float sunIntensity(float zenithAngleCos)
{
	return EE * max(0.0, 1.0 - exp(-((cutoffAngle - acos(zenithAngleCos))/steepness)));
}


// float logLuminance(vec3 c)
// {
// 	return log(c.r * 0.2126 + c.g * 0.7152 + c.b * 0.0722);
// }

float linearise_depth(float zLog)
{
	return 0.25 / (1 - zLog * 0.999755859375);
}

void main() 
{
	vec3 sunPosition = pass.sun_pos;
	vec3 cameraPos = pass.cam_pos;
	float ndc_depth = texture(depth_tex, v_texc).r;
	float linear_depth = linearise_depth(ndc_depth);
	vec3 frag_pos_w = v_dir_w * linear_depth + pass.cam_pos;
	vec3 vWorldPosition = frag_pos_w;
	float dist = length(vWorldPosition - cameraPos);
    float sunfade = 1.0-clamp(1.0-exp((sunPosition.y/1.0)),0.0,1.0);
    // luminance =  1.0 ;// vWorldPosition.y / 450000. + 0.5; //sunPosition.y / 450000. * 1. + 0.5;

	float reileighCoefficient = reileigh - (1.0* (1.0-sunfade));
    vec3 sunDirection = normalize(sunPosition);
    float sunE = sunIntensity(dot(sunDirection, up));
    // extinction (absorbtion + out scattering) 
	// rayleigh coefficients

	// vec3 betaR = totalRayleigh(lambda) * reileighCoefficient;
	vec3 betaR = simplifiedRayleigh() * reileighCoefficient;
    // mie coefficients
	vec3 betaM = totalMie(lambda, K, turbidity) * mieCoefficient;
    // optical length
	// cutoff angle at 90 to avoid singularity in next formula.
	float zenithAngle = acos(max(0.0, dot(up, normalize(vWorldPosition - cameraPos))));
    float sR = rayleighZenithLength / (cos(zenithAngle) + 0.15 * pow(93.885 - ((zenithAngle * 180.0) / pi), -1.253));
    float sM = mieZenithLength / (cos(zenithAngle) + 0.15 * pow(93.885 - ((zenithAngle * 180.0) / pi), -1.253));
    // combined extinction factor	
	vec3 Fex = exp(-(betaR * sR + betaM * sM));
    // in scattering
	float cosTheta = dot(normalize(vWorldPosition - cameraPos), sunDirection);
    float rPhase = rayleighPhase(cosTheta*0.5+0.5);
    vec3 betaRTheta = betaR * rPhase;
    float mPhase = hgPhase(cosTheta, mieDirectionalG);
    vec3 betaMTheta = betaM * mPhase;
    vec3 Lin = pow(sunE * ((betaRTheta + betaMTheta) / (betaR + betaM)) * (1.0 - Fex),vec3(1.5));
    Lin *= mix(vec3(1.0),pow(sunE * ((betaRTheta + betaMTheta) / (betaR + betaM)) * Fex,vec3(1.0/2.0)),clamp(pow(1.0-dot(up, sunDirection),5.0),0.0,1.0));

    //nightsky
	vec3 direction = normalize(vWorldPosition - cameraPos);
    float theta = acos(direction.y); // elevation --> y-axis, [-pi/2, pi/2]
	float phi = atan(direction.z, direction.x); // azimuth --> x-axis [-pi/2, pi/2]
	vec2 uv = vec2(phi, theta) / vec2(2.0*pi, pi) + vec2(0.5, 0.0);
    // vec3 L0 = texture2D(skySampler, uv).rgb+0.1 * Fex;
	vec3 L0 = vec3(0.1) * Fex;

    // composition + solar disc
	//if (cosTheta > sunAngularDiameterCos)
	float sundisk = smoothstep(sunAngularDiameterCos,sunAngularDiameterCos+0.00002,cosTheta);
    //if (normalize(vWorldPosition - cameraPos).y>0.0)
		L0 += (sunE * 19000.0 * Fex)*sundisk;
    vec3 whiteScale = 1.0/vec3(1);
    vec3 texColor = (Lin+L0);
    texColor *= 0.04 ;
    texColor += vec3(0.0,0.001,0.0025)*0.3;
    //float g_fMaxLuminance = 1.0;
    //float fLumScaled = 0.1 / luminance;
    //float fLumCompressed = (fLumScaled * (1.0 + (fLumScaled / (g_fMaxLuminance * g_fMaxLuminance)))) / (1.0 + fLumScaled);
    //float ExposureBias = fLumCompressed;
    vec3 curr = (log2(2.0/pow(luminance,4.0)))*texColor;
    vec3 color = curr*whiteScale;
	//color = mix(color, vec3(0), 1-pow(max(normalize(vWorldPosition).y, 0.2), 0.5));
    vec3 retColor = pow(color,vec3(2.2/(1.2+(1.2*sunfade))));
	out_color = vec4(retColor, 1.0);
	//out_color = vec4(color, 1.0);
}

#endif