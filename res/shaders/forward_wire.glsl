#version 430 core
#extension GL_ARB_shader_draw_parameters : enable

layout(binding = 0) uniform GeomPass {
	mat4 view_proj;
	mat4 shadow_mvp;
} pass;

struct PerObject {
	mat4 model;
	vec3 color;
	float smoothness;
};

layout(std140, binding = 1) uniform Object {
	PerObject[256] objs;
};
#ifdef GLX_VERTEX_SHADER
layout(location = 0) in vec3 vert_pos;

out VertexData {
	vec3 pos_w;
	flat int draw_id;
} vout;

void main() {
	int draw_id = gl_InstanceID;
    vout.pos_w = vec3(objs[draw_id].model * vec4(vert_pos, 1.0));
	vout.draw_id = draw_id;
    gl_Position = pass.view_proj * objs[draw_id].model * vec4(vert_pos, 1.0);
}
#endif

#ifdef GLX_FRAGMENT_SHADER
in VertexData {
	vec3 pos_w;
	flat int draw_id;
} vin;

out vec4 texel_color;

void main() {
	texel_color = vec4(objs[vin.draw_id].color, 1);
}

#endif