#version 430 core
#extension GL_ARB_shader_viewport_layer_array : enable

layout(std140, binding = 0) uniform Pass {
	mat4 view_proj;
	mat4 cam_vp;
	int layer;
} pass;

struct PerObject {
	//mat4 mvp;
	mat4 model;
	//vec4 pad[8];
};

//layout(std140, binding = 1) uniform Object {
layout(std430, binding = 1) buffer Object {
	PerObject objs[];
};

layout(binding = 0) uniform sampler2D height_tex;

#ifdef GLX_VERTEX_SHADER /****************/
layout(location = 0) in vec3 vert_pos;
layout(location = 1) in uint inst_id;

out VertexData{
	vec3 pos;
	flat int draw_id;
} vout;

void main() {
	#ifdef GL_ARB_shader_viewport_layer_array
	gl_Layer = pass.layer;
	#endif
	vout.pos = vert_pos;
	vout.draw_id = int(inst_id);
	//gl_Position =  vec4(vert_pos, 1.0);
}
#endif

#ifdef GLX_TESS_CONTROL_SHADER /****************/
layout(vertices = 4) out;
in VertexData {
	vec3 pos;
	flat int draw_id;
} vin[];

out VertexData {
	vec3 pos;
	flat int draw_id;
} vout[];
#define ID gl_InvocationID

const vec2 screen_size = vec2(1600, 1024);
const float lod_factor = 16.0;
vec4 project(vec4 vertex){
	vec4 result = pass.cam_vp * objs[vin[0].draw_id].model * vertex;
	result /= result.w;
	return result;
}
vec2 screen_space(vec4 vertex){
	//return (clamp(vertex.xy, -1.3, 1.3)+1) * (screen_size*0.5);
	return (vertex.xy+1) * (screen_size*0.5);
}
float level(vec2 v0, vec2 v1){
	float l = distance(v0, v1)/lod_factor;
	l = pow(2, ceil(log2(l)));
	return clamp(l, 16, 64);
}

void main()
{
	vout[ID].pos = vin[ID].pos;
	vout[ID].draw_id = vin[ID].draw_id;
	if (ID == 0) {
		vec4 v0 = project(vec4(vin[0].pos, 1.0));
		vec4 v1 = project(vec4(vin[1].pos, 1.0));
		vec4 v2 = project(vec4(vin[2].pos, 1.0));
		vec4 v3 = project(vec4(vin[3].pos, 1.0));

		vec2 ss0 = screen_space(v0);
		vec2 ss1 = screen_space(v1);
		vec2 ss2 = screen_space(v2);
		vec2 ss3 = screen_space(v3);

		float e1 = level(ss1, ss2);
		float e0 = level(ss0, ss1);
		float e3 = level(ss3, ss0);
		float e2 = level(ss2, ss3);

		//float inner0 = mix(e1, e2, 0.5);
		//float inner1 = mix(e0, e3, 0.5);
		//gl_TessLevelInner[0] = inner0;// + mod(inner0, 2) - 1;
		//gl_TessLevelInner[1] = inner1;// + mod(inner1, 2) - 1;
		gl_TessLevelInner[0] = max(e1, e2);
		gl_TessLevelInner[1] = max(e0, e3);
		gl_TessLevelOuter[0] = e0;
		gl_TessLevelOuter[1] = e1;
		gl_TessLevelOuter[2] = e2;
		gl_TessLevelOuter[3] = e3;
	}
}
#endif

#ifdef GLX_TESS_EVALUATION_SHADER /****************/
layout(quads, ccw) in;
in VertexData {
	vec3 pos;
	flat int draw_id;
} vin[];

void main()
{
	vec3 tc1 = mix(vin[0].pos, vin[3].pos, gl_TessCoord.x);
	vec3 tc2 = mix(vin[1].pos, vin[2].pos, gl_TessCoord.x);
	vec3 tc = mix(tc2, tc1, gl_TessCoord.y);

	vec4 pw = objs[vin[0].draw_id].model * vec4(tc.xyz, 1.0);
	vec2 texc = pw.xz / 256 * 0.5 + 0.5;
	float scale = 48.0;
	//gl_Position = pass.view_proj * pw;
	gl_Position = pass.view_proj * objs[vin[0].draw_id].model * vec4(tc.xyz + vec3(0, texture(height_tex, texc).r, 0), 1.0);;
}
#endif

#ifdef GLX_GEOMETRY_SHADER

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

void main()
{
	gl_Layer = pass.layer;

	for (int i = 0; i < gl_in.length(); ++i)
	{
		gl_Position = gl_in[i].gl_Position;
		EmitVertex();
	}

	EndPrimitive();
}

#endif

#ifdef GLX_FRAGMENT_SHADER
// Ouput data
layout(location = 0) out float texel_depth;
/*
in VertexData {
	vec4 pos;
	noperspective vec3 wire_dist;
} vin;
*/
void main() {
	/*vec3 d = fwidth(vin.wire_dist);
	vec3 a3 = smoothstep(vec3(0.0), d * 0.5, vin.wire_dist);
	float edgeFactor = min(min(a3.x, a3.y), a3.z);
	gl_FragDepth = mix(vin.pos.z/vin.
	pos.w, 1.0, edgeFactor);*/
}

#endif