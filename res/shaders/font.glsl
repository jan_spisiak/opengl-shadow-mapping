#version 430 core
#ifdef GLX_VERTEX_SHADER

layout(location = 0) in int Character;
out int vCharacter;
out int vPosition;

void main()
{
    vCharacter = Character;
    vPosition = gl_VertexID;
    gl_Position = vec4(0, 0, 0, 1);
}
#endif
#ifdef GLX_GEOMETRY_SHADER

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

in int vCharacter[1];
in int vPosition[1];
out vec2 gTexCoord;
uniform sampler2D Sampler;

uniform vec2 cell_size = vec2(1.0/16.0, (300.0/384.0)/6.0);
uniform vec2 cell_offset = vec2(0.5/256.0, 0.5/256);
uniform int vertex_offset = 0;
uniform vec2 render_size = vec2(0.75 * 16 / 768.0, 0.75 * 33.33 / 1024.0);
uniform vec2 render_origin = vec2(-0.96, 0.9);

void main()
{
    // Determine the final quad's position and size:
    float x = render_origin.x + float(vPosition[0] - vertex_offset) * render_size.x * 2;
    float y = render_origin.y;
    vec4 P = vec4(x, y, 0, 1);
    vec4 U = vec4(1, 0, 0, 0) * render_size.x;
    vec4 V = vec4(0, 1, 0, 0) * render_size.y;

    // Determine the texture coordinates:
    int letter = vCharacter[0];
    letter = clamp(letter - 32, 0, 96);
    int row = letter / 16 + 1;
    int col = letter % 16;
    float S0 = cell_offset.x + cell_size.x * col;
    float T0 = cell_offset.y + 1 - cell_size.y * row;
    float S1 = S0 + cell_size.x - cell_offset.x;
    float T1 = T0 + cell_size.y;

    // Output the quad's vertices:
    gTexCoord = vec2(S0, T1); gl_Position = P - U - V; EmitVertex();
    gTexCoord = vec2(S1, T1); gl_Position = P + U - V; EmitVertex();
    gTexCoord = vec2(S0, T0); gl_Position = P - U + V; EmitVertex();
    gTexCoord = vec2(S1, T0); gl_Position = P + U + V; EmitVertex();
    EndPrimitive();
}
#endif
#ifdef GLX_FRAGMENT_SHADER

in vec2 gTexCoord;
out vec4 FragColor;

layout(binding = 0) uniform sampler2D Sampler;
uniform vec3 TextColor = vec3(1, 1, 1);

void main()
{
    float A = texture(Sampler, gTexCoord).r;
    FragColor = vec4(0, A, 0, A);
}
#endif