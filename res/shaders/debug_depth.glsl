#version 430 core

layout(binding = 0) uniform sampler2DArray texFramebuffer;

#ifdef GLX_VERTEX_SHADER
layout(location = 0) in vec2 position;
layout(location = 1) in vec2 texcoord;
out vec2 Texcoord;

void main() {
   Texcoord = texcoord;
   gl_Position = vec4(position, 0.0, 1.0);
}
#endif

#ifdef GLX_FRAGMENT_SHADER
in vec2 Texcoord;
out vec4 outColor;

float linearise_depth(float zLog)
{
	float zNear = 0.1;
	float zFar = 128.0;
    zLog = 2.0 * zLog - 1.0;
    float zLinear = 2.0 * zNear * zFar / (zFar + zNear - zLog * (zFar - zNear));
    return zLinear;
}

void main() {
   //outColor = vec4(vec3(mod(texture(texFramebuffer, vec3(Texcoord, 1)).r, 1.1) / 1.1 + 0.0), 1.0);
   outColor = vec4(vec3(texture(texFramebuffer, vec3(Texcoord, 0)).r), 1.0);
   //outColor = vec4(mod(vec3(linearise_depth(texture(texFramebuffer, Texcoord).r)), 0.2) / 0.2, 1.0);
}
#endif