#version 430 core
//#extension GL_ARB_shader_draw_parameters : enable

layout(binding = 0) uniform GeomPass {
	mat4 view_proj;
	mat4 shadow_mvp;
	mat4 real_view_proj;
} pass;

struct PerObject {
	mat4 model;
	vec3 color;
	float smoothness;
};

layout(std140, binding = 1) uniform Object {
	PerObject[256] objs;
};

layout(binding = 0) uniform sampler2D height_tex;
layout(binding = 1) uniform sampler2D normal_tex;

#ifdef GLX_VERTEX_SHADER
layout(location = 0) in vec3 vert_pos;
layout(location = 1) in vec3 vert_normal;
layout(location = 3) in vec2 vert_texc;

out VertexData {
	vec3 pos;
	flat int draw_id;
} vout;

void main() {
	vout.pos = vec3(vec4(vert_pos, 1.0));
	vout.draw_id = gl_InstanceID;
	//gl_Position =  vec4(vert_pos, 1.0);
}
#endif

#ifdef GLX_TESS_CONTROL_SHADER /*****************/
layout(vertices = 4) out;
in VertexData {
	vec3 pos;
	flat int draw_id;
} vin[];

out VertexData {
	vec3 pos;
	flat int draw_id;
} vout[];
#define ID gl_InvocationID

const vec2 screen_size = vec2(1600, 1024);
const float lod_factor = 16.0;
vec4 project(vec4 vertex){
	vec4 result = pass.view_proj * objs[vin[0].draw_id].model * vertex;
	result /= result.w;
	return result;
}
vec2 screen_space(vec4 vertex){
	//return (clamp(vertex.xy, -1.3, 1.3)+1) * (screen_size*0.5);
	return (vertex.xy+1) * (screen_size*0.5);
}
float level(vec2 v0, vec2 v1){
	float l = distance(v0, v1)/lod_factor;
	l = pow(2, ceil(log2(l)));
	return clamp(l, 16, 64);
}

void main()
{
	vout[ID].pos = vin[ID].pos;
	vout[ID].draw_id = vin[ID].draw_id;
	if (ID == 0) {
		vec4 v0 = project(vec4(vin[0].pos, 1.0));
		vec4 v1 = project(vec4(vin[1].pos, 1.0));
		vec4 v2 = project(vec4(vin[2].pos, 1.0));
		vec4 v3 = project(vec4(vin[3].pos, 1.0));

		vec2 ss0 = screen_space(v0);
		vec2 ss1 = screen_space(v1);
		vec2 ss2 = screen_space(v2);
		vec2 ss3 = screen_space(v3);

		float e1 = level(ss1, ss2);
		float e0 = level(ss0, ss1);
		float e3 = level(ss3, ss0);
		float e2 = level(ss2, ss3);

		//float inner0 = mix(e1, e2, 0.5);
		//float inner1 = mix(e0, e3, 0.5);
		//gl_TessLevelInner[0] = inner0;// + mod(inner0, 2) - 1;
		//gl_TessLevelInner[1] = inner1;// + mod(inner1, 2) - 1;
		gl_TessLevelInner[0] = max(e1, e2);
		gl_TessLevelInner[1] = max(e0, e3);
		gl_TessLevelOuter[0] = e0;
		gl_TessLevelOuter[1] = e1;
		gl_TessLevelOuter[2] = e2;
		gl_TessLevelOuter[3] = e3;
	}
}
#endif

#ifdef GLX_TESS_EVALUATION_SHADER /*************/
layout(quads, ccw) in;
in VertexData {
	vec3 pos;
	flat int draw_id;
} vin[];

out VertexData {
	vec3 pos_w;
	vec3 normal_w;
	vec4 shadc;
	vec2 texc;
	flat int draw_id;
} vout;

vec4 bump(vec2 tex_coord, float scale) {
	const vec2 size = vec2(0.02, 0);
	//const ivec3 off = ivec3(-1,0,1);

	float s11 = texture(height_tex, tex_coord).r * scale;
	float sx0 = textureOffset(height_tex, tex_coord, ivec2(-1,0)).r * scale;
	float sx1 = textureOffset(height_tex, tex_coord, ivec2(1,0)).r * scale;
	float sy0 = textureOffset(height_tex, tex_coord, ivec2(0,-1)).r * scale;
	float sy1 = textureOffset(height_tex, tex_coord, ivec2(0,1)).r * scale;
	vec3 dx = normalize(vec3(size.x, sx1-sx0, size.y));
	vec3 dy = normalize(vec3(size.y, sy1-sy0, size.x));
	//return vec4( cross(dy,dx), s11 );
	return vec4( normalize(cross(dy,dx)), s11 );
}

void main()
{
	vec3 tc1 = mix(vin[0].pos, vin[3].pos, gl_TessCoord.x);
	vec3 tc2 = mix(vin[1].pos, vin[2].pos, gl_TessCoord.x);
	vec3 tc = mix(tc2, tc1, gl_TessCoord.y);

	vec4 pw = objs[vin[0].draw_id].model * vec4(tc.xyz, 1.0);
	vec2 texc = pw.xz / 256 * 0.5 + 0.5;
	float scale = 48.0;
	float bmp = texture(height_tex, texc).r;
	pw.y += bmp;// * scale * 2;
	//gl_Position = pass.view_proj * pw;
	//vec4 pos_w = objs[vin[0].draw_id].model * vec4(tc.x, tc.y + bmp * scale / 32.0, tc.z, 1.0);
	vec4 pos_w = objs[vin[0].draw_id].model * vec4(tc.xyz + vec3(0, bmp, 0), 1.0);
	gl_Position = pass.view_proj * pos_w;
	vout.pos_w = pos_w.xyz;
	//vec3 norm = texture(normal_tex, texc).rgb * 2 - 1;
	vec3 norm = bump(texc, 1).xyz;
	//vec3 te_normal = normalize(vec3(norm.r * 32.0/scale, norm.b, -norm.g * 32.0/scale));
	vout.shadc.xy = gl_TessCoord.xy;
	vout.normal_w = mat3(objs[vin[0].draw_id].model) * norm; 
	vout.texc = texc;
	vout.draw_id = vin[0].draw_id;
}
#endif
/*
#ifdef GL/_GEOMETRY_SHADER
layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;
 
in VertexData {
	vec3 pos_w;
	vec3 normal_w;
	vec4 shadc;
	vec2 texc;
	flat int draw_id;
} vin[];
out VertexData {
	vec3 pos_w;
	vec3 normal_w;
	vec4 shadc;
	vec2 texc; 
	noperspective vec3 wire_dist;
	flat int draw_id;
} vout;
 
void main() {
		for(int i = 0; i < 3; i++) {
				gl_Position = gl_in[i].gl_Position;
				vout.pos_w = vin[i].pos_w;
				vout.normal_w = vin[i].normal_w;
				vout.shadc = vin[i].shadc;
				vout.texc = vin[i].texc;
				vout.draw_id = vin[i].draw_id;
				vout.wire_dist = vec3(0.0);
				vout.wire_dist[i] = 1.0;
				EmitVertex();
		}
}
#endif
*/
#ifdef GLX_FRAGMENT_SHADER
in VertexData {
	vec3 pos_w;
	vec3 normal_w;
	vec4 shadc;
	vec2 texc; 
//    vec3 wire_dist;
	flat int draw_id;
} vin;

out vec4 texel_albedo;
out vec4 texel_normal;

vec3 blend_udn(vec3 n1, vec3 n2)
{
	vec3 c = vec3(2, 1, 0);
	vec3 r;
	r = n2*c.yyz + n1.xyz;
	r =  r*c.xxx -  c.xxy;
	return normalize(r);
}
void main() {
	vec3 normal_w = normalize(vin.normal_w);

	//vec3 norm = mat3(objs[vin.draw_id].model) * bump(vin.texc).xyz;

	//vec3 te_normal = blend_udn(normal_w , bump(vin.texc).xyz);
	vec3 t = texture(normal_tex, vin.texc).rgb * 2 - 1;
	t = mat3(objs[vin.draw_id].model) * normalize(vec3(t.r * 24.0 / 32, t.b, -t.g * 24.0 / 32));
	vec3 te_normal = normal_w;//normalize(t);
	//vec3 te_normal = mat3(objs[vin.draw_id].model) * bump(vin.texc).xyz;

	float steep = max(dot(te_normal, vec3(0, 1, 0)), 0);
	float height = clamp((vin.pos_w.y - 8) / 24.0, 0, 1);
	vec3 color = mix(vec3(0.001, 0.5, 0.001), vec3(0.4, 0.4, 0.4), height);
	//color = mix(color, mix(vec3(0.001, 0.6, 0.001), vec3(0.4, 0.4, 0.4), height), smoothstep(0.4, 0.9, steep));
	float smth = 0.15;
	//mix(0.05, 0.15, sqrt(steep));

	/*
	vec3 d = fwidth(vin.wire_dist);
	vec3 a3 = smoothstep(vec3(0.0), d * 0.5, vin.wire_dist);
	float edgeFactor = min(min(a3.x, a3.y), a3.z);
	color = mix(vec3(1.0), color.rgb, edgeFactor);
	*/

	texel_albedo = vec4(color, smth);
	texel_normal = vec4(te_normal * 0.5 + 0.5, 1);
}

#endif