#version 430 core

#ifdef GLX_COMPUTE_SHADER
#define MAX_LIGHTS 1024
#define MAX_LIGHTS_PER_TILE 40

#define WORK_GROUP_SIZE 16

struct PointLight
{
	vec3 color;
	float intensity;
	vec3 position;
	float radius;
};

struct SunLight {
	vec4 color;
	vec3 direction;
	mat4 vp[8];
	float splits[8];
	float scales[8];
	int max_split;
	float slider;
};

layout(std140, binding = 0) uniform LightPass {
	mat4 vp_inv;
	mat4 proj_inv;
	mat4 proj;
	mat4 view;
	vec3 lin_depth_coef;
	vec3 cam_pos_w;
	uvec2 screen_size;
	uint num_active_lights;
	SunLight sun_light;
};

layout (binding = 0, rgba16f) uniform writeonly image2D outTexture;
//layout (binding = 1, r32f) uniform readonly image2D depth_img;

layout(binding = 0) uniform sampler2D depth_tex;
layout(binding = 1) uniform sampler2D normal_tex;
layout(binding = 2) uniform sampler2D albedo_tex;
layout(binding = 3) uniform sampler2DArrayShadow shadow_tex;
layout(binding = 4) uniform samplerCube probe_tex;
layout(binding = 5) uniform sampler2DArray exp_shadow_tex;

//layout (std430, binding = 5) buffer BufferObject
layout (std140, binding = 1) uniform BufferObject
{
	PointLight plights[1024];
};

layout (local_size_x = WORK_GROUP_SIZE, local_size_y = WORK_GROUP_SIZE) in;

shared uint minDepth = 0xFFFFFFFF;
shared uint maxDepth = 0;
shared uint plight_index[MAX_LIGHTS];
shared uint plight_count = 0;
shared uint id = 0;
shared vec4 frustum_planes[6];
shared vec3 aabb_min, aabb_max;

float linearise_depth(float z_t)
{
	return lin_depth_coef.x / (1 - z_t * lin_depth_coef.y);
}
vec3 ndc_to_world(float z, vec2 uv_f)
{
	vec4 ndc = vec4(uv_f * 2.0 - 1.0, z, 1.0);
	ndc = vp_inv * ndc;
	return (ndc.xyz / ndc.w);
}

const float darkening_coefs[8] = {32.0, 64.0, 64.0, 256.0, 256.0, 256.0, 256.0, 256.0};
float shadow_exp(vec3 shadow_coords, int spi)
{
	float light_depth = texture( exp_shadow_tex, vec3(shadow_coords.xy, spi) ).r;
	return clamp(exp(darkening_coefs[spi] * (light_depth - shadow_coords.z)), 0.0, 1.0);
}

const int smpl_count[8] = {4, 3, 2, 2, 1, 1, 1, 1};
const float pcf_offset = 1.0 / 2048.0;
float shadow_pcf(vec3 shadow_coords, int spi)
{
	vec2 offx = vec2(pcf_offset, 0);
	vec2 offy = vec2(0, pcf_offset);
	const int smps = smpl_count[spi];
	float term = 0;
	shadow_coords.xy += -vec2((smps - 1)/2.0 * pcf_offset);
	for (int x=0; x < smps; ++x) {
		for (int y=0; y < smps; ++y) {
			vec2 coords = shadow_coords.xy + x * offx + y * offy;
			// texture(tex, vec4(uv, layer, depth))
			term += texture(shadow_tex, vec4(coords, spi, shadow_coords.z));
	}}
	return term / (smps * smps);
}

vec4 shadow_space(vec3 pos_w, vec3 normal_w, vec3 light_dir_w, float texel_s, mat4 view_proj)
{
	float cos_alpha = max(dot(normal_w, light_dir_w), 0);
	float sin_alpha = sqrt(1 - cos_alpha * cos_alpha);
	float normal_off_coef = texel_s * max(sin_alpha, 0.5);
	return view_proj * vec4(pos_w +  normal_w * normal_off_coef, 1.0);
}
float casc_shadow_term(vec3 pos_w, vec3 normal_w, vec3 light_dir_w, float cam_depth)
{
	float sp = 0;
	for (int i = 0; i <= sun_light.max_split; ++i)
		sp += (step(sun_light.splits[i], cam_depth));

	// Try lower split
	int spi = int(max(sp - 1, 0));
#if 1//CASCADE_TEST
	vec4 frag_shadc = shadow_space(pos_w, normal_w, light_dir_w, sun_light.slider * smpl_count[spi] * sun_light.scales[spi], sun_light.vp[spi]);
#else
	vec4 frag_shadc = sun_light.vp[spi] * vec4(pos_w, 1.0);
#endif
	// so it doesn't interfere with tests
	frag_shadc.w = 0.5;

	#ifdef EXP_SHADOWS
	vec4 bounds = vec4(0.5 - 6/1024.0);
	#else
	vec4 bounds = vec4(0.5 - 0.5*smpl_count[spi]/1024.0);
	#endif
	vec4 dist = abs(frag_shadc - vec4(0.5));
	if (any(greaterThan(dist, bounds))) {
		spi = spi + 1; // we didn't fit in lower split
		if (spi > sun_light.max_split) // outside of last split
			return 1;
		// Recalculate coords for next split
#if 1//CASCADE_TEST
		frag_shadc = shadow_space(pos_w, normal_w, light_dir_w, sun_light.slider * smpl_count[spi] * sun_light.scales[spi], sun_light.vp[spi]);
#else
		frag_shadc = sun_light.vp[spi] * vec4(pos_w, 1.0);
#endif
	}

#ifdef EXP_SHADOWS
	return shadow_exp(frag_shadc.xyz, spi);
#else
	return shadow_pcf(frag_shadc.xyz, spi);
#endif
}

vec3 point_light(vec3 dir_w, vec3 pos_w, vec3 norm_w, vec3 albedo, vec4 mat_par, vec4 light_color, vec4 light_pos_w) {
	vec3 frag_to_light = light_pos_w.xyz - pos_w;
	vec3 light_dir_w = normalize(frag_to_light);
	float NoL = dot(norm_w, light_dir_w);
	if (NoL > 0) {
		vec3 half_dir_w = normalize(light_dir_w - dir_w);
		float mNoL = max(NoL, 0);
		float mNoH = max(dot(half_dir_w, norm_w), 0);
		float spec_fact = pow(mNoH, mat_par.y) * mNoL * mat_par.x;

		vec3 specular = mat_par.w  * light_color.rgb * spec_fact; 
		vec3 diffuse = mat_par.z  * light_color.rgb * albedo * mNoL;

		float light_dist = length(frag_to_light);
		float att = max(1 - light_dist / light_pos_w.w, 0);
		return (diffuse + specular) * light_color.a * att*att;
	}
	return vec3(0); 
}
vec3 directional_light(vec3 dir_w, vec3 pos_w, float linear_depth, vec3 norm_w, vec3 albedo, vec4 mat_par, vec4 light_color, vec3 light_dir_w) {
	float NoL = dot(norm_w, light_dir_w);
	if (NoL > 0 && light_color.a > 0) {
		vec3 half_dir_w = normalize(light_dir_w - dir_w);
		float mNoL = max(NoL, 0);
		float mNoH = max(dot(half_dir_w, norm_w), 0);
		float spec_fact = pow(mNoH, mat_par.y) * mNoL * mat_par.x;

		vec3 specular = vec3(mat_par.w * spec_fact); 
		vec3 diffuse = mat_par.z * albedo * mNoL;
		vec3 collect = (diffuse + specular) * light_color.rgb * light_color.a;
		float visibility = casc_shadow_term(pos_w, norm_w, light_dir_w, linear_depth / lin_depth_coef.z);

		//return collect;
		return collect * visibility;
		//return vec3(NoL, 1-visibility, 0);
	}
	return vec3(0);
}
vec3 probe_light(vec3 dir_w, vec3 pos_w, vec3 norm_w, vec3 albedo, vec4 mat_par) {
	// mat_par is km, kr, kd, ks
	vec3 probe_color = textureLod(probe_tex, norm_w, 0).rgb;
	vec3 diffuse = mat_par.z * probe_color * albedo * 0.16;
	return diffuse;
}


void main()
{
	ivec2 pixel = ivec2(gl_GlobalInvocationID.xy);
	float d = texelFetch(depth_tex, pixel, 0).x;;

	uint depth = floatBitsToUint(d);
	atomicMin(minDepth, depth);
	atomicMax(maxDepth, depth);

	barrier();

	if (gl_LocalInvocationIndex == 0) {
		float min_depth = linearise_depth(uintBitsToFloat(minDepth));
		float max_depth = linearise_depth(uintBitsToFloat(maxDepth));
		vec2 tile_scale = screen_size * (1.0f / float( 2 * WORK_GROUP_SIZE));
		vec2 tile_bias = tile_scale - vec2(gl_WorkGroupID.xy);
		vec4 col1 = vec4(-proj[0][0] * tile_scale.x, proj[0][1], tile_bias.x, proj[0][3]);
		vec4 col2 = vec4(proj[1][0], -proj[1][1] * tile_scale.y, tile_bias.y, proj[1][3]);
		vec4 col4 = vec4(proj[3][0], proj[3][1],  -1.0f, proj[3][3]);
		frustum_planes[0] = col4 + col1; //Left plane
		frustum_planes[1] = col4 - col1; //right plane
		frustum_planes[2] = col4 - col2; //top plane
		frustum_planes[3] = col4 + col2; //bottom plane
		frustum_planes[4] = vec4(0.0f, 0.0f, -1.0f, -min_depth); //near
		frustum_planes[5] = vec4(0.0f, 0.0f, 1.0f, max_depth); //far

		for(int i = 0; i < 4; i++)
		{
			frustum_planes[i] *= 1.0f / length(frustum_planes[i].xyz);
		}
	}
	barrier();

	uint threadCount = WORK_GROUP_SIZE * WORK_GROUP_SIZE;
	uint passCount = (num_active_lights + threadCount - 1) / threadCount;
	for (uint passIt = 0; passIt < passCount; ++passIt)
	{
		uint lightIndex =  passIt * threadCount + gl_LocalInvocationIndex;

		if (lightIndex < num_active_lights) { // don't try to access lights beyond max
			PointLight pl = plights[lightIndex];
			vec4 pos = view * vec4(pl.position, 1.0f);
			float rad = pl.radius;
			if (plight_count < MAX_LIGHTS_PER_TILE) {
				bool in_frustum = true;
				for (uint i = 5; i >= 0 && in_frustum; i--)
				{
					float dist = dot(frustum_planes[i], pos);
					in_frustum = (-rad <= dist);
				}

				if (in_frustum) {
					id = atomicAdd(plight_count, 1);
					plight_index[id] = lightIndex;
				}
			}
		}
	}

	barrier();
	vec2 uv = (vec2(pixel)+0.5f) / screen_size;
	vec3 pos_w = ndc_to_world(d * 2 - 1.0, uv);
	float linear_depth = linearise_depth(d);
	vec4 albedo_f = texture(albedo_tex, uv);
	float mat_rough = 2/pow(1 - albedo_f.a * 0.82327, 4) - 2;
	float ks = albedo_f.a;
	float kd = 1 - albedo_f.a;
	vec4 mat_par = vec4((mat_rough + 6) / 8, mat_rough, kd, ks);

	vec3 norm_w = normalize(texture(normal_tex, uv).xyz * 2 - 1);
	vec3 dir_w = normalize(pos_w - cam_pos_w);

	vec4 color = vec4(0.0f, 0.0f, 0.0f, 1.0f);
	color.xyz += probe_light(dir_w, pos_w, norm_w, albedo_f.xyz, mat_par);
	for (int i = 0; i < plight_count; i++)
	{
		PointLight p = plights[plight_index[i]];
		color.xyz += point_light(dir_w, pos_w, norm_w, albedo_f.xyz, mat_par, vec4(p.color, p.intensity), vec4(p.position, p.radius));
	}
	if (sun_light.color.a > 0.01) {
		color.xyz += directional_light(dir_w, pos_w, linear_depth, norm_w, albedo_f.xyz, mat_par, sun_light.color, sun_light.direction);
	}

	imageStore(outTexture, pixel, color);
}
#endif //GL/_COMPUTE_SHADER