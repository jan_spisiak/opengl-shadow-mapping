#version 430 core

layout(std140, binding = 0) uniform IrradComp {
	mat4 view;
} pass;

layout(binding = 0) uniform samplerCube probe_tex;

#ifdef GLX_VERTEX_SHADER
layout(location = 0) in vec2 vert_pos;
layout(location = 1) in vec2 vert_texc;
out vec2 frag_texc;
out vec3 frag_pos;

void main() {
	frag_texc = vert_texc;
	frag_pos = vec3(pass.view * vec4(vert_pos, -1.0, 1.0));
	gl_Position = vec4(vert_pos, 0.0, 1.0);
}
#endif

#ifdef GLX_FRAGMENT_SHADER
in vec2 frag_texc;
in vec3 frag_pos;
out vec4 outColor;

void main() {
	vec3 texc = frag_pos;
	vec3 smp = vec3(0, 0, 0);
	float sum_cfs = 0;
	const int div = 32;
	vec3 norm = normalize(texc);
	vec3 bitang = normalize(vec3(0,norm.y,0));
	//vec3 tang = normalize(vec3(norm.z, norm.y, -norm.x));
	//tang = normalize(tang - dot(tang, norm) * norm);
	bitang = normalize(bitang - dot(bitang, norm) * norm);
	//vec3 bitang = cross(norm, tang);
	vec3 tang = cross(norm, bitang);
	const mat3 tbn = mat3(tang, bitang, norm);

	for (int j=-div/2; j<div/2; ++j) {
	for (int i=-div/2; i<div/2; ++i) {
		vec2 o = sin(vec2(i,j)/div * 3.14159265);
		vec3 smp_texc = tbn * vec3(o.x, o.y, sqrt(1-o.x*o.x-o.y*o.y));
		float cf = max(dot(norm, normalize(smp_texc)), 0);
		smp += cf * texture(probe_tex, normalize(smp_texc)).rgb;
		sum_cfs += cf;
	}}
	//sum_cfs += 0.1;
	smp /= sum_cfs;

	//diff=0;
	//for (int j=-div/2; j<0/2; ++j) {
	//	diff+=1;
	//	float o = j;
	//	o = sin(o/16.0 * 3.14159265);
	//	smp = vec3(abs(o));
	//}
	outColor = vec4(smp, 1);
	//outColor = vec4(vec3(sum_cfs/100.0), 1);
}

#endif