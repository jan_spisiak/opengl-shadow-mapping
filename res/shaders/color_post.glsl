#version 430 core

layout(location = 0, binding = 0) uniform sampler2D color_tex;
layout(location = 1) uniform float exposure = 0.5;

#ifdef GLX_VERTEX_SHADER
layout(location = 0) in vec2 vert_pos;
layout(location = 1) in vec2 vert_texc;
out vec2 frag_texc;

void main() {
   frag_texc = vert_texc;
   gl_Position = vec4(vert_pos, 0.0, 1.0);
}
#endif

#ifdef GLX_FRAGMENT_SHADER
in vec2 frag_texc;
out vec4 out_color;

vec3 Uncharted2Tonemap(vec3 x)
{
    float A = 0.15;
    float B = 0.50;
    float C = 0.10;
    float D = 0.20;
    float E = 0.02;
    float F = 0.30;
    return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

vec3 luminance(vec3 color)
{
	float lum = 0.299 * color.r + 0.587 * color.g + 0.114 * color.b;
	//lum = log(lum + 1) / 2;
	vec3 pm = vec3(1/2, 1, 2);
	vec3 om = vec3(3, 2, 1);
	//return vec3(lum,0,1-lum);
	//return vec3(lum / 10, 0, lum / 2);
	return pow(Uncharted2Tonemap(vec3(lum) * exposure), vec3(1.0 / 2.2));
}

void main() {
	vec3 scene_color = texture(color_tex, frag_texc).xyz; 
	out_color = vec4(pow(Uncharted2Tonemap(scene_color * exposure), vec3(1.0 / 2.2)), 1.0);
	//out_color = vec4(luminance(scene_color), 1.0);
}

#endif