#version 430 core
#extension GL_ARB_shader_viewport_layer_array : enable
#extension GL_NV_geometry_shader_passthrough : enable

layout(std140, binding = 0) uniform Pass {
	mat4 view_proj;
	mat4 cam_vp;
	int layer;
} pass;

struct PerObject {
	//mat4 mvp;
	mat4 model;
	//vec4 pad[8];
};

//layout(std140, binding = 1) uniform Object {
layout(std430, binding = 1) buffer Object {
	PerObject objs[];
};

#ifdef GLX_VERTEX_SHADER
layout(location = 0) in vec3 vert_pos;
layout(location = 1) in uint inst_id;

void main() {
	#ifdef GL_ARB_shader_viewport_layer_array
	gl_Layer = pass.layer;
	#endif
	gl_Position = pass.view_proj * objs[inst_id].model * vec4(vert_pos, 1.0);
}
#endif

#ifdef GLXX_GEOMETRY_SHADER

layout(triangles) in;

#ifdef GL_NV_geometry_shader_passthrough
// Redeclare gl_PerVertex to pass through "gl_Position".
layout(passthrough) in gl_PerVertex {
	vec4 gl_Position;
};

void main() {
	gl_Layer = pass.layer;
}
#else
layout(triangle_strip, max_vertices = 3) out;

void main()
{	
	gl_Layer = pass.layer;

	for(int i = 0; i < gl_in.length(); ++i)
	{
		gl_Position = gl_in[i].gl_Position;
		EmitVertex();
	}

	EndPrimitive();
}
#endif //GL_NV_geometry_shader_passthrough
#endif //GLX_GEOMETRY_SHADER

#ifdef GLX_FRAGMENT_SHADER
// Ouput data
layout(location = 0) out float texel_depth;

void main(){
	// Implicitly writing depth
}
#endif