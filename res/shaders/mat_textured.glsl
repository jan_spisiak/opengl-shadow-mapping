#version 430 core

layout(binding = 0) uniform GeomPass {
	mat4 view_proj;
	mat4 shadow_mvp;
} pass;

layout(binding = 1) uniform Material {
	mat4 model;
	float smoothness;
} mat;

#ifdef GLX_VERTEX_SHADER
layout(location = 0) in vec3 vert_pos;
layout(location = 1) in vec3 vert_normal;
layout(location = 2) in vec3 vert_tang;
layout(location = 3) in vec2 vert_texc;

out vec3 frag_pos_w;
out vec3 frag_normal_w;
out vec3 frag_tang_w;
out vec4 frag_shadc;
out vec2 frag_texc;

void main() {
	mat3 model3 = mat3(mat.model);
	frag_pos_w = vec3(mat.model * vec4(vert_pos, 1.0));
	frag_normal_w = model3 * vert_normal;
	frag_tang_w = model3 * vert_tang;
	frag_shadc = pass.shadow_mvp * vec4(vert_pos, 1.0); 
	frag_texc = vert_texc;
	gl_Position = pass.view_proj * mat.model * vec4(vert_pos, 1.0);
}
#endif

#ifdef GLX_FRAGMENT_SHADER
in vec3 frag_pos_w;
in vec3 frag_normal_w;
in vec3 frag_tang_w;
in vec4 frag_shadc;
in vec2 frag_texc;

out vec4 texel_albedo;
out vec3 texel_normal;

uniform sampler2D diffuse_tex;
uniform sampler2D normal_tex;
uniform sampler2D specular_tex;
uniform sampler2D shadow_tex;

float shadow()
{
	float light_depth = texture( shadow_tex, frag_shadc.xy ).r;
	float depth_exp = 2048.0;
	return clamp(exp(depth_exp * (light_depth - frag_shadc.z)), 0.0, 1.0);	
}

void main() {
	vec3 normal_w = normalize(frag_normal_w);
	vec3 tangent_w = normalize(frag_tang_w);
	tangent_w = normalize(tangent_w - dot(tangent_w, normal_w) * normal_w);
	vec3 bitangent_w = cross(tangent_w, normal_w);
	mat3 TBN = mat3(tangent_w, bitangent_w, normal_w);
	vec3 normal_shape_w = normal_w;
	normal_w = normalize(normal_w * 1.2 + TBN * (2.0 * texture(normal_tex, frag_texc).xyz - 1.0));

	vec3 albedo = texture(diffuse_tex, frag_texc).rgb;

	texel_albedo = vec4(albedo, mat.smoothness);
	texel_normal = vec3(normal_w);
}
#endif