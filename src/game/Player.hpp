#pragma once

#include "engine/Entity.hpp"
#include "engine/EntitySystem.hpp"

namespace grow
{
	class Actor
	{
	protected:
		EntitySystem& entsys;
	public:
		Actor(EntitySystem& entsys) : entsys(entsys) {}
		virtual void Update();
	};

	class Player : public Actor
	{
	protected:
		Entity entity;
	public:
		Player(EntitySystem& entsys, CameraUnit&& camu) : Actor(entsys), entity(entsys.create())
		{
			entity.add(std::move(camu));
		}
		virtual void Update()
		{

		}
	};
}