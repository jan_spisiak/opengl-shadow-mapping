#pragma once

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>

#include <glm/gtx/norm.hpp>
#include <glm/gtx/string_cast.hpp>

#include "Expected.hpp"

namespace glm {
	template <typename T, precision P>
	GLM_FUNC_DECL tquat<T,P> rotationb(const tvec3<T, P>& a, const tvec3<T, P>& b) {
		tvec3<T, P> n = cross(a, b);

		return normalize(angleAxis(sqrt(length2(a) * length2(b)) + dot(a, b), n));
	}
}

template<typename Unique, typename T>
struct strong_type {
	T t;
	using C = strong_type<Unique, T>;
	T& value() { return t; }
	const T& value() const { return t; }

	bool operator<(const C& other) const { return t < other.t; }
	bool operator>(const C& other) const { return t > other.t; }
	bool operator==(const C& other) const { return t == other.t; }
	bool operator!=(const C& other) const { return !(*this == other); }
};

template<typename Unique, typename Base, typename T>
struct narrow_type {
	T t;
	using C = narrow_type<Unique, Base, T>;
	using B = Base;
	//T& value() { return t; }
	//const B& value() const { return t; }
	C(Base b) : t(b.t) {};
	operator B() { return B{ t }; }

	bool operator<(const C& other) const { return t < other.t; }
	bool operator>(const C& other) const { return t > other.t; }
	bool operator==(const C& other) const { return t == other.t; }
	bool operator!=(const C& other) const { return !(*this == other); }
};

void die(const std::string &msg);

template<typename T>
std::string toString(T value)
{
	std::ostringstream oss;
	oss << value;
	return oss.str();
}

template<typename T>
T align(T offset, T alignment)
{
	int remainder = offset % alignment;
	offset += ((remainder != 0) ? (alignment - remainder) : 0);
	return offset;
}

template <typename C>
float time_diff_set(std::chrono::time_point<C>& begin, std::chrono::time_point<C>& end) {
	end = C::now();
	std::swap(begin, end);
	return static_cast<std::chrono::duration<float, std::milli>>(begin - end).count();
}

std::ostream &operator<< (std::ostream &out, const glm::vec3 &vec);
std::ostream &operator<< (std::ostream &out, const glm::vec4 &vec);
std::ostream &operator<< (std::ostream &out, const glm::quat &quat);

std::string str_merge(std::initializer_list<std::string> strList);

template< typename ... Args >
void die(const Args& ... args)
{
	die(str_merge({ toString(args)... }));
}

Expected<std::string> load_file(const char *filename);