#include "Util.hpp"

void die(const std::string &msg)
{
	throw std::runtime_error(msg);
}

std::ostream &operator<< (std::ostream &out, const glm::vec3 &vec) {
	return out << glm::to_string(vec);
}

std::ostream &operator<< (std::ostream &out, const glm::vec4 &vec) {
	return out << glm::to_string(vec);
}

std::ostream &operator<< (std::ostream &out, const glm::quat &quat) {
	return out << glm::to_string(quat);
}

std::string str_merge(std::initializer_list<std::string> strList)
{
	std::string ret = "";
	for (std::string s : strList) {
		ret += s;
	}
	return ret;
}

Expected<std::string> load_file(const char *filename)
{
	std::ifstream in(filename, std::ios::in | std::ios::binary);
	if (in)
	{
		std::string contents;
		in.seekg(0, std::ios::end);
		contents.resize(static_cast<unsigned int>(in.tellg()));
		in.seekg(0, std::ios::beg);
		in.read(&contents[0], contents.size());
		in.close();
		return contents;
	}
	return Expected<std::string>::fromException(std::runtime_error(str_merge({ "Reading file: ", filename})));
}