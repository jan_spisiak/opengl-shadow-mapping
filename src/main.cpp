#include "engine/Engine.hpp"

#include <stdio.h>
#include <stdlib.h>

extern "C" {
	_declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
}

int main(int argc, char **argv)
{
	int rc = EXIT_FAILURE;
	grow::Engine engine;
	try {
		rc = engine.Run(argc, argv);
	}
	catch ( const std::runtime_error & e ) {
#if defined(_WIN32) && !NDEBUG
		OutputDebugString(e.what());
#endif
		std::cout << e.what() << "\n";
	}
#if defined(_WIN32) && !NDEBUG
	//system("pause");
#endif
	return rc;
}