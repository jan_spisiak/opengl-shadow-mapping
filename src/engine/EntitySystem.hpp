#pragma once

#include "render/RenderSystem.hpp"
#include "engine/Entity.hpp"
#include <deque>

namespace grow
{
	template<typename T>
	struct StaticSwitch
	{
		T val;
		StaticSwitch() {}
		StaticSwitch(T& t) : val(t) {}
	};

	class RenderSystem;

	class EntitySystem
		:
		StaticSwitch<RenderSystem*>,
		StaticSwitch<Manager<LightUnit>>,
		StaticSwitch<Manager<SceneUnit>>,
		StaticSwitch<Manager<CameraUnit>>,
		StaticSwitch<Manager<RenderUnit>>
	{
	public:
		EntitySystem();
		Entity create();
		bool destroy(Entity& e);

		template<typename U>
		void addUnit(Entity& e, U&& u) { StaticSwitch<Manager<U>>::val.add(e, std::move(u)); }
		template<typename U>
		U& getUnit(Entity::Handle& e) { return StaticSwitch<Manager<U>>::val.get(e); }
		template<typename U>
		Manager<U>& getManager() { return StaticSwitch<Manager<U>>::val; }

	private:
		std::deque<EntityHandle> free_handles;
		EntityHandle highest_handle;
	};
}