#include "engine/EntitySystem.hpp"

namespace grow
{
	EntitySystem::EntitySystem()
		: highest_handle(0, 0)
	{
	}

	Entity EntitySystem::create()
	{
		EntityHandle handle{0, 0};
		if (free_handles.empty()) {
			handle = highest_handle;
			highest_handle = EntityHandle{ highest_handle.id() + 1, 0 };
		} else {
			auto freeHandle = free_handles.front();
			handle = EntityHandle{ freeHandle.id(), freeHandle.version() + 1 };
			free_handles.pop_front();
		}
		return Entity(*this, handle);
	}
	
	bool EntitySystem::destroy(Entity & e)
	{
		return false;
	}
}