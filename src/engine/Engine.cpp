#include "Engine.hpp"

#include <random>
#include <thread>
#include <gsl/gsl>
#include <glm/gtx/orthonormalize.hpp>
#include <glbinding/gl/gl.h>
#include <glbinding/ContextInfo.h>

//#include "system/File.h"
#include "EntitySystem.hpp"
#include "../render/RenderSystem.hpp"
#include "../render/UserInterface.hpp"
#include "../render/imgui/imgui_impl_sdl_gl3.h"
#include "Frame.hpp"

#include "../util/profile.h"

using namespace gl;
using namespace grow;

int Engine::Run(int argc, char **argv)
{
	const int winsx = 1600;
	const int winsy = 1024;
	//Window window("GrowTest", winsx, winsy, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	Window window("ShadowMapping", winsx, winsy, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	Context gl(window);
	ImGui_ImplSdlGL3_Init(window);
	const GLubyte *vendor = glGetString(GL_VENDOR);
	const GLubyte *renderer = glGetString(GL_RENDERER);
	const GLubyte *version = glGetString(GL_VERSION);
	std::cout << vendor << "\n" << renderer << "\n" << version << "\n";
	std::set<gl::GLextension> suppexts = glbinding::ContextInfo::extensions();
	Globals globals{
		suppexts
	};
	if (globals.rend.extensions.count(gl::GLextension::GL_ARB_shader_viewport_layer_array) > 0)
		std::cout << "GL_ARB_shader_viewport_layer_array supported.\n";
	else
		std::cout << "GL_ARB_shader_viewport_layer_array NOT supported.\n";
	EntitySystem entity_s;
	UserInterface ui(window, gl);
	RenderSystem render_s(window, gl, entity_s, ui);
	Terrain terrain{entity_s, render_s};

	ShaderProgram mat_shader(ShaderProgram(load_file("res/shaders/mat_solid.glsl")));
	ShaderProgram mat_test(ShaderProgram(load_file("res/shaders/mat_test.glsl")));

	Entity sphere = entity_s.create();
	Entity treer = entity_s.create();
	Entity camera = entity_s.create();
	Entity house = entity_s.create();
	Entity dlight = entity_s.create();

	Material wood_mat({ mat_shader,{  },{1.0,0.1,1.0}, 0.9f });
	Material concrete_mat({ mat_shader,{  },{0.1,0.4,0.1}, 0.4f });
	Material house_mat({ mat_shader,{},{0.1,0.1,0}, 0.2f });
	VertexStorage<MeshVertex> sphere_storage("res/pack/test/sphere.obj", 0);
	VertexStorage<MeshVertex> figure_storage("res/pack/test/tree.obj", 0);
	VertexStorage<MeshVertex> house_storage("res/pack/test/tree.obj", 0);
	VertexFormat def_format = VertexFormat::DefaultFormat();
	sphere.add(RenderUnit({ wood_mat, render_s.createMesh(sphere_storage, def_format) }));
	uint32_t tree_meshi = render_s.createMesh(figure_storage, def_format);
	treer.add(RenderUnit({ concrete_mat, tree_meshi }));
	house.add(RenderUnit({ house_mat, render_s.createMesh(house_storage, def_format) }));

	glm::vec3 center{ -1.5f, 9.5f, -12.0f };
	sphere.add(SceneUnit(center + glm::vec3(2.5f, 3.0f, 0.0f) ));
	treer.add(SceneUnit(center + glm::vec3(0.5f, -1.2f, 2.0f) ));
	camera.add(SceneUnit(center + glm::vec3(1.5f, 1.5f, 6.0f)));
	house.add(SceneUnit(center + glm::vec3(2.0f, -0.5f, -5.0f), glm::angleAxis(glm::pi<float>() / 4.0f, glm::vec3(0, 1, 0)), glm::vec3{2.0f}));

	dlight.add(LightUnit({ { 1, 0.984f, 0.757f }, 1.6f, 0.0f, SceneUnit(glm::vec3(0, 0, 0), glm::rotation(glm::vec3(0, 0, -1), glm::normalize(glm::vec3(-1, -1, 0.1)))) }));
	camera.add(CameraUnit{ glm::radians(60.0f), (float)window.size().x / window.size().y, 0.25f, 512.0f });

	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_real_distribution<float> dist(0.0, 1.0);
	// Add random lights
	for (int x = 0; x < 16; ++x)
	for (int y = 0; y < 16; ++y) {
		Entity plight = entity_s.create();
		float r1= dist(mt), r2= dist(mt), r3= dist(mt), r4= dist(mt);
		auto scu = SceneUnit(glm::clamp(glm::vec3(-50 + r1 * 100, 11.0f, -50 + r2*100), -50.0f, 50.0f), glm::normalize(glm::angleAxis(r3*3.14f, glm::vec3{0, 1, 0})));
		glm::vec3 color{r1,r2,r3};
		plight.add(LightUnit({ color, 6.4f, 3.0f + r4*3.0f, scu }));
	}
	// Add random trees
	//auto tree_meshi = render_s.createMesh(figure_storage, def_format);
	for (int i = 0; i < 512; ++i) {
		float x, y, z, r = dist(mt);
		do {
			x = dist(mt);
			z = dist(mt);
			auto off = glm::ivec2(glm::vec2{ x,z } * glm::vec2{1024, 1024});
			y = terrain.GetHeight(off);
		} while (y > 13.0);
		Entity tree = entity_s.create();
		tree.add(RenderUnit({ concrete_mat, tree_meshi }));
		tree.add(SceneUnit(glm::vec3{ (x * 2.0 - 1.0) * 256.0, y - 0.1f, (z * 2.0 - 1.0) * 256.0 }, glm::angleAxis(r*3.14f, glm::vec3{ 0,1,0 }), glm::vec3{0.5}));
	}

	std::cout << "Init " << glGetError() << " " << glGetError() << "\n";

	bool show_test_window = true;
	bool show_another_window = false;
	ImVec4 clear_color = ImColor(114, 144, 154);
	auto thread_id = std::this_thread::get_id();
	std::cout << "Main thread id: " << thread_id << "\n";

	std::vector<RenderSystem::FinalPassMode> final_pass_modes = {RenderSystem::Final, RenderSystem::Shadow, RenderSystem::Normal};
	std::vector<RenderSystem::FinalPassMode>::iterator final_pass_modes_it = final_pass_modes.begin();
	bool running = true, update_paused = false;
	RenderSystemOptions rsos = { 0, false, false, true, 1.0f, false, false };
	auto t_last = std::chrono::high_resolution_clock::now(), t_begin = std::chrono::high_resolution_clock::now();
	int32_t id = 1;
	Profiler profiler;
	while (running)
	{
		MICROPROFILE_SCOPEI("Engine", "MainLoop", 0xff00ff);
		Frame frame{ ++id, globals, {}, profiler };
		frame.profiler.begin_cpu("State/");
		SDL_Event event;
		while (SDL_PollEvent(&event)){
			ImGui_ImplSdlGL3_ProcessEvent(&event);
			switch (event.type){
			case SDL_KEYDOWN:
				if (event.key.keysym.sym == SDLK_m)
					if (final_pass_modes_it == final_pass_modes.end() - 1)
						final_pass_modes_it = final_pass_modes.begin();
					else
						++final_pass_modes_it;
				if (event.key.keysym.sym == SDLK_p)
					update_paused = !update_paused;
				if (event.key.keysym.sym == SDLK_c)
					rsos.is_camera_frozen = !rsos.is_camera_frozen;
				if (event.key.keysym.sym == SDLK_v)
					rsos.stabilize_shadows = !rsos.stabilize_shadows;
				if (event.key.keysym.sym == SDLK_l)
					rsos.layered_shadow_maps = !rsos.layered_shadow_maps;
				if (event.key.keysym.sym == SDLK_k)
					rsos.exponential_shadows = !rsos.exponential_shadows;
				if (event.key.keysym.sym == SDLK_o)
					rsos.sort_by_object = !rsos.sort_by_object;
				if (event.key.keysym.sym == SDLK_ESCAPE)
					running = false;
				break;
			case SDL_QUIT:
				running = false;
				break;
			default:
				break;
			}
		}
		const Uint8 *kstate = SDL_GetKeyboardState(NULL);
		ImGui_ImplSdlGL3_NewFrame(window);
		rsos.slider = glm::clamp(rsos.slider + 0.05f * (-kstate[SDL_SCANCODE_COMMA] + kstate[SDL_SCANCODE_PERIOD]), 0.0f, 1.0f);
		//ui.DrawString(toString(rsos.slider), 20, 20);

		// Calculate transformation
		auto t_now = std::chrono::high_resolution_clock::now();
		double time = std::chrono::duration<double, std::milli>(t_now - t_last).count();
		double abs_time = std::chrono::duration<double>(t_now - t_begin).count();
		t_last = t_now;
		//ui.DrawString("Frame " + toString(time) + " ms", 20, 95);
		frame.profiler.end_cpu("State/");

		{
			MICROPROFILE_SCOPEI("Engine", "Render", 0xff00ff);
			frame.profiler.begin_cgpu("Render/");
			// Rotate Camera
			SceneUnit& cam_scu = camera.get<SceneUnit>();
			glm::vec3 rot_axis_vec(
				-kstate[SDL_SCANCODE_LEFT] + kstate[SDL_SCANCODE_RIGHT],
				-kstate[SDL_SCANCODE_DOWN] + kstate[SDL_SCANCODE_UP],
				0);
			if (rot_axis_vec != glm::vec3(0, 0, 0))// && glm::length2(cam_scu.orientation) > 0.0001f)
			{
				//rot_axis_vec = glm::normalize(rot_axis_vec);
				//glm::quat qt = glm::angleAxis(0.02f, rot_axis_vec * cam_scu.orientation);
				//cam_scu.orientation = glm::normalize(cam_scu.orientation) * qt;
				float sens = 0.01f;
				const auto xRot = cam_scu.orientation * glm::angleAxis(sens, glm::vec3(0, -rot_axis_vec.x, 0));
				const auto xyRot = xRot * glm::angleAxis(sens, glm::vec3(rot_axis_vec.y, 0, 0));
				cam_scu.orientation = glm::normalize(xyRot);
			}

			// Move camera
			float move_speed = 0.02f;
			glm::vec3 move_vec(
				-kstate[SDL_SCANCODE_A] + kstate[SDL_SCANCODE_D],
				-kstate[SDL_SCANCODE_Q] + kstate[SDL_SCANCODE_E],
				-kstate[SDL_SCANCODE_W] + kstate[SDL_SCANCODE_S]
				);
			if (kstate[SDL_SCANCODE_LSHIFT])
				move_speed = 0.16f;
			if (kstate[SDL_SCANCODE_TAB])
				move_speed = 0.64f;
			cam_scu.position += cam_scu.orientation * move_vec * move_speed;

			// Move Sun
			auto& dlight_liu = dlight.get<LightUnit>();
			auto& dlight_scu = dlight_liu.scu;
			float sun_rot = 0.005f * (kstate[SDL_SCANCODE_R] - kstate[SDL_SCANCODE_F]);
			if (sun_rot) {
				dlight_scu.orientation = glm::angleAxis(sun_rot, glm::vec3(0, 0, 1)) * dlight_scu.orientation;
				float cos = glm::dot({ 0, -1, 0 }, glm::normalize(dlight_scu.fwd()));
				float power = sqrt(cos);
				dlight_liu.color = glm::mix(glm::vec3{ 0.7539, 0.3098, 0.0392 }, glm::vec3{ 1, 0.984f, 0.857f }, power);
				dlight_liu.power = glm::mix(3.2f, 6.4f, power);
				if (cos < 0.01)
					dlight_liu.power = 0;
			}
			//ui.DrawString(toString(dlight_scu.fwd()), 20, 50);

			if (!update_paused)
			{
				// Move props
				auto& sscu = sphere.get<SceneUnit>();
				sscu.position += glm::vec3(0.0f, 0.0f, sin(abs_time) / 10.0);
				auto& mscu = treer.get<SceneUnit>();
				mscu.orientation *= glm::angleAxis(0.04f, glm::vec3(0.0f, 1.0f, 0.0f));

				// Move Lights
				auto& lium = entity_s.getManager<LightUnit>();
				for (size_t i = 1; i < lium.size(); ++i)
				{
					auto eh = lium.getHandle(i);
					auto& liu = lium.getRef(i);
					auto& scu = liu.scu;

					glm::vec3 fwd = scu.fwd() * 0.2f;
					glm::vec3 pos = scu.position + fwd;
					if (glm::any(glm::greaterThan(glm::abs(pos), glm::vec3{ 50.0f }))) {
						scu.orientation *= glm::angleAxis(glm::pi<float>(), glm::vec3{0,1,0});
						scu.position += scu.fwd() * 0.2f;
						scu.position.y = 10.0f;
					}
					else {
						scu.position = pos;
					}
				}
			}
			render_s.Render(frame, rsos);
			auto t_end = std::chrono::high_resolution_clock::now();
			float render_all_time = time_diff_set(t_now, t_end);
			ui.Render();
			frame.profiler.end_cgpu("Render/");
		}

		{
			if (ImGui::Button("Test Window")) show_test_window ^= 1;
			ImGui::SameLine();
			if (ImGui::Button("Another Window")) show_another_window ^= 1;
			if (ImGui::CollapsingHeader("Profiler", ImGuiTreeNodeFlags_DefaultOpen)) {
				float content_width = ImGui::GetWindowContentRegionMax().x;
				float time_size = 70.0f;

				ImGui::Text("Imgui %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
				ImGui::Text("Mark"); ImGui::SameLine(content_width - 2 * time_size);
				ImGui::Text("CPU"); ImGui::SameLine(content_width - time_size);
				ImGui::Text("GPU");
				ImGui::Separator();
				gsl::cstring_span<> last_group;
				ptrdiff_t last_level{};
				std::vector<bool> visibles = {true};
				profiler.for_each_pop([&](const std::string& name, float cpu_time, float gpu_time) {
					auto namesp = gsl::cstring_span<>{ name };
					auto group_delim_it = std::find(namesp.rbegin(), namesp.rend(), '/');
					auto group_delim = std::distance(group_delim_it, namesp.rend());
					auto label_delim = group_delim;
					bool is_leaf = true;
					if (group_delim_it == namesp.rbegin()) {
						label_delim = std::distance(std::find(group_delim_it + 1, namesp.rend(), '/'), namesp.rend());
						is_leaf = false;
					}
					auto group = namesp.subspan(0, group_delim);
					auto label = namesp.subspan(label_delim, namesp.size() - label_delim);
					auto common_group_itp = std::mismatch(group.begin(), group.end(), last_group.begin(), last_group.end());
					auto common_level = std::count(group.begin(), common_group_itp.first, '/');
					for (auto i = last_level; i > common_level; --i) {
						if (visibles.back() == true) {
							ImGui::TreePop();
						}
						visibles.pop_back();
					}
					if (visibles.back() == true) {
						if (is_leaf) {
							ImGui::BulletText("%.*s", label.size(), label.data());
							last_level = common_level;
						}
						else {
							ImGui::PushStyleColor(ImGuiCol_Text, ImColor::HSV(0.6f, 0.7f, 0.9f));
							bool visible = ImGui::TreeNode(label.data());
							ImGui::PopStyleColor();
							visibles.push_back(visible);
							last_level = common_level + 1;
						}
						ImGui::SameLine(content_width - 2 * time_size);
						ImGui::Text("%.3f ms", cpu_time);
						ImGui::SameLine(content_width - time_size);
						ImGui::Text("%.3f ms", gpu_time);
						last_group = group;
					}
				});
				for (size_t i = 1; i < visibles.size(); ++i) {
					if (visibles.back() == true) {
						ImGui::TreePop();
					}
					visibles.pop_back();
				}
				ImGui::Columns(1);
				ImGui::Separator();
			}
			if (ImGui::CollapsingHeader("Renderer", ImGuiTreeNodeFlags_DefaultOpen)) {
				ImGui::Text("ShadowPass draw calls %i", frame.renderer.shadow_draw_calls);
				ImGui::Text("GeometryPass draw calls %i", frame.renderer.geom_draw_calls);
			}
		}
		terrain.GuiUpdate();

		if (show_test_window)
		{
			ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiSetCond_FirstUseEver);
			ImGui::ShowTestWindow(&show_test_window);
		}

		render_s.FinalPass(*final_pass_modes_it);
		// Swap buffers
		ImGui::Render();
		MicroProfileFlip(0);
		window.Swap();
	}
	std::cout << "Exit " << glGetError() << " " << glGetError() << "\n";
	ImGui_ImplSdlGL3_Shutdown();

	return 0;
}

Engine::Engine()
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0) /* Initialize SDL's Video subsystem */
		die("Unable to initialize SDL"); /* Or die on error */
}


Engine::~Engine()
{
	SDL_Quit();
}
