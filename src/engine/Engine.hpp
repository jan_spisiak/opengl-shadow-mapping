#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <memory>
#include <chrono>

#include <Windows.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <SDL2/SDL.h>

#include "../system/Window.hpp"

namespace grow
{
	class Engine
	{
	private:

	public:
		Engine();
		~Engine();

		int Run(int argc, char **argv);
	};
}