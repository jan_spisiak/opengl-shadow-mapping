#include "Terrain.hpp"
#include <render/RenderSystem.hpp>
#include <glm/gtx/transform.hpp>
#include "render/imgui/imgui.h"

namespace {
	// Terrain quad
	std::vector<MeshVertex> terrain_verts{
		{ { -0.5f, 0.0f,  0.5f },{ 0, 1, 0 },{ 1, 0, 0 },{ 0, 0 } },
		{ { 0.5f, 0.0f,  0.5f },{ 0, 1, 0 },{ 1, 0, 0 },{ 1, 0 } },
		{ { 0.5f, 0.0f, -0.5f },{ 0, 1, 0 },{ 1, 0, 0 },{ 1, 1 } },
		{ { -0.5f, 0.0f, -0.5f },{ 0, 1, 0 },{ 1, 0, 0 },{ 0, 1 } }
	};
	std::vector<uint16_t> terrain_indices{ 0, 1, 2, 3 };
	VertexStorage<MeshVertex> terrain_storage{ GL_PATCHES, terrain_verts, terrain_indices, { 0, 0.5 * 48.0 / 32.0, 0 }, { 0.5,0.5 * 48.0 / 32.0, 0.5} };

	const int terrain_patch_number = 16;
	std::array<grow::SceneUnit, 256> terrain_scus;

	glm::vec4 CalcBump(Image& img, glm::vec2 tex_coord, float scale) {
		const glm::vec2 size = glm::vec2(0.02, 0);
		//const ivec3 off = ivec3(-1,0,1);
		auto fetch = [&](glm::vec2 off) -> float {
			auto clamped = glm::clamp(tex_coord + off, glm::vec2(0, 0), glm::vec2(img.size) - glm::vec2{ 1,1 });
			return img.fetch<float>(clamped, 0) * scale;
		};

		float s11 = fetch({ 0,0 });
		float sx0 = fetch({ -1,0 });
		float sx1 = fetch({ 1,0 });
		float sy0 = fetch({ 0,-1 });
		float sy1 = fetch({ 0,1 });
		glm::vec3 dx = glm::normalize(glm::vec3(size.x, sx1 - sx0, size.y));
		glm::vec3 dy = glm::normalize(glm::vec3(size.y, sy1 - sy0, size.x));
		//auto res = glm::vec4( cross(dx,dy), s11 );
		auto res = glm::vec4(normalize(-cross(dx, dy)), s11);
		return res;
	}
}

namespace grow
{
	Terrain::Terrain(EntitySystem& es, RenderSystem& rs) :
		es(es), rs(rs),
		//terrain_vbo(GL_ARRAY_BUFFER, sizeof(terrain_vertices), terrain_vertices, GL_STATIC_DRAW), terrain_vao(),
		terrain_sp(load_file("res/shaders/terrain.glsl")),
		shadow_terrain_sp({ GL_VERTEX_SHADER, GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER, GL_FRAGMENT_SHADER }, load_file("res/shaders/shadow_terrain.glsl")),
		terrain_meshi(rs.createMesh(terrain_storage, VertexFormat::DefaultFormat())),
		terrain_tex("res/textures/tatras_height.png"), terrain_normal_tex("res/textures/tatras_normal2.png"),
		terrain_mat{ terrain_sp,{ &terrain_tex, &terrain_normal_tex } },
		shadow_terrain_mat{ shadow_terrain_sp, { &terrain_tex, &terrain_normal_tex } },
		terrain_img(Image::Random({ 1024, 1024, 4 }))
	{

		terrain_tex.Bind().Param(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER).Param(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER)
			.Param(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR).Param(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		GLfloat height_border[] = { 0.0, 0.0, 0.0, 1.0 };
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, height_border);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, 1024, 1024, 0, GL_RGBA, GL_FLOAT, &*terrain_img.data);
		glGenerateMipmap(GL_TEXTURE_2D);
		terrain_normal_tex.Bind().Param(GL_TEXTURE_MIN_FILTER, GL_LINEAR).Param(GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		std::vector<glm::vec4> new_normals;
		new_normals.reserve(1024 * 1024);
		// Calculate normals
		for (int x = 0; x < terrain_img.size.x; ++x) {
			for (int y = 0; y < terrain_img.size.y; ++y) {
				auto normal = CalcBump(terrain_img, { x, y }, 1.0f);
				new_normals.push_back({ glm::vec3{normal}, 1.0f });
			}
		}
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, 1024, 1024, 0, GL_RGBA, GL_FLOAT, new_normals.data());

		Generate();
	}

	void Terrain::Generate()
	{
		// precalculate terrain tiles
		for (int j = 0; j < terrain_patch_number; ++j) {
			for (int i = 0; i < terrain_patch_number; ++i) {
				const float size = patch_size;
				float x = i - (float)terrain_patch_number / 2, y = j - (float)terrain_patch_number / 2;
				glm::vec3 pos = glm::vec3{ size * (x + 0.5f), 0.0f, size * (y + 0.5f) };
				glm::mat4 model = glm::translate(pos) * glm::scale(glm::vec3{ size });

				glm::mat4 abs_model{ glm::abs(model[0]),glm::abs(model[1]),glm::abs(model[2]),glm::abs(model[3]) };
				glm::vec3 center = model * glm::vec4{ glm::vec3{ 0, 0.5 * 48.0 / patch_size, 0 }, 1.0 };
				glm::vec3 extent = abs_model * glm::vec4{ 0.5, 0.5 * 48.0 / patch_size, 0.5, 0.0 };
				terrain_scus[i + j * terrain_patch_number] = { pos, glm::quat{}, glm::vec3{ size }, center, extent };

				Entity tree = es.create();
				tree.add(RenderUnit({ terrain_mat, terrain_meshi, &shadow_terrain_mat }));
				tree.add(SceneUnit{ pos, glm::quat{}, glm::vec3{ size }, center, extent });

				terrain_tiles.push_back(std::move(tree));
			}
		}
	}

	float Terrain::GetHeight(glm::ivec2 off)
	{
		return terrain_img.fetch<float>(off, 0) * 32.0f;
	}

	void Terrain::GuiUpdate()
	{
		if (ImGui::CollapsingHeader("Terrain", ImGuiTreeNodeFlags_DefaultOpen)) {
			ImGui::Image((ImTextureID)terrain_tex.get().value(), ImVec2(512, 512), ImVec2(0, 0), ImVec2(1, 1), ImColor(0, 0, 0, 255));
			ImGui::Image((ImTextureID)terrain_normal_tex.get().value(), ImVec2(512, 512));
		}
	}
}