#pragma once
#include <cstdint>
#include <chrono>
#include <string>
#include <set>
#include <map>

#include <glbinding/gl/gl.h>
#include <glbinding/Meta.h>

#include "util\Util.hpp"

using namespace gl;

class Query
{
public:
	using Handle = strong_type<Query, gl::GLuint>;
protected:
	Handle handle;
	gl::GLenum target;
public:
	Query() : handle({ 0 }) { gl::glGenQueries(1, &handle.value()); }
	using C = Query;
	friend void swap(C& lhs, C& rhs) {
		std::swap(lhs.handle, rhs.handle);
		std::swap(lhs.target, rhs.target);
	}
	Query(Query&& other) { handle = other.handle; target = other.target; other.handle = { 0 }; }
	~Query() { gl::glDeleteQueries(1, &handle.value()); }

	//void Begin(GLenum targ) { target = targ; glBeginQuery(target, handle); }
	//void End() { glEndQuery(target); }
	//operator GLuint() const { return handle[(cur - 1) % capacity]; }
	//GLuint name(int32_t frame) const { return handle[frame % capacity]; }
};

struct Profiler {
	class QueryPool {
		std::vector<Query::Handle> handles;
	public:
		QueryPool(){
			handles.resize(64);
			glGenQueries(64, &(handles.data()->value()));
		}
		Query::Handle alloc() {
			if (handles.empty()) {
				size_t old_capa = handles.capacity();
				handles.reserve(old_capa * 2);
				handles.resize(old_capa);
				glGenQueries(old_capa, &(handles.data()->value()));
			}
			Query::Handle h = handles.back();
			handles.pop_back();
			return h;
		}
		void free(Query::Handle h) {
			handles.push_back(h);
		}
		~QueryPool() {
			glDeleteQueries(handles.size(), &handles.data()->value());
		}
	};
	QueryPool qpool;

	class GpuQuery {
		static constexpr int32_t max = 3;
		std::array<Query::Handle, max> begins = {};
		std::array<Query::Handle, max> ends = {};
		uint32_t it = 0;
		uint32_t last() { return (it + max - 1) % max; }
	public:
		float pop(QueryPool& pool) {
			float result = 0.0f;
			if (begins[it].value() && ends[it].value()) {
				gl::GLint begin_ready, end_ready;
				glGetQueryObjectiv(begins[it].value(), GL_QUERY_RESULT_AVAILABLE, &begin_ready);
				glGetQueryObjectiv(ends[it].value(), GL_QUERY_RESULT_AVAILABLE, &end_ready);
				if (begin_ready == gl::GLint{ GL_TRUE } && end_ready == gl::GLint{ GL_TRUE }) {
					gl::GLint64 end, begin;
					glGetQueryObjecti64v(begins[it].value(), GL_QUERY_RESULT, &begin);
					glGetQueryObjecti64v(ends[it].value(), GL_QUERY_RESULT, &end);
					result = (end - begin) / 1000000.0f;
				}
				pool.free(begins[it]); begins[it] = { 0 };
				pool.free(ends[it]); ends[it] = { 0 };
			}
			it = (it + 1) % max;
			return result;
		}
		void begin(QueryPool& pool) {
			begins[last()] = pool.alloc();
			glQueryCounter(begins[last()].value(), GL_TIMESTAMP);
		}
		void end(QueryPool& pool) {
			ends[last()] = pool.alloc();
			glQueryCounter(ends[last()].value(), GL_TIMESTAMP);
		}
	};

	class CpuQuery {
		using cpu_time_t = decltype(std::chrono::high_resolution_clock::now());
		cpu_time_t cpu_begin;
		cpu_time_t cpu_end;
	public:
		void begin() {
			cpu_begin = std::chrono::high_resolution_clock::now();
		}
		void end() {
			cpu_end = std::chrono::high_resolution_clock::now();
		}
		float pop() {
			float cpu_time = static_cast<std::chrono::duration<float, std::milli>>(cpu_end - cpu_begin).count();
			cpu_begin = {};
			cpu_end = {};
			return cpu_time;
		}
	};

	struct Mark {
		CpuQuery cpuq;
		GpuQuery gpuq;
		float cpu_avg;
		float gpu_avg;
	};

	using Key = std::string;

	std::map<Key, Mark> marks;
	float alpha = 0.8f;

	template<typename Fn>
	void for_each_pop(Fn f) {
		for (auto& pair : marks) {
			auto& mark = pair.second;
			float cpu_time = mark.cpuq.pop();
			float gpu_time = mark.gpuq.pop(qpool);
			mark.cpu_avg = alpha * mark.cpu_avg + (1 - alpha) * cpu_time;
			mark.gpu_avg = alpha * mark.gpu_avg + (1 - alpha) * gpu_time;
			f(pair.first, mark.cpu_avg, mark.gpu_avg);
		}
	}

	void begin_cgpu(Key name) {
		auto mark_it = marks.find(name);
		if (mark_it != marks.end()) {
			mark_it->second.cpuq.begin();
			mark_it->second.gpuq.begin(qpool);
		}
		else {
			Mark mark{};
			mark.cpuq.begin();
			mark.gpuq.begin(qpool);
			marks.emplace(std::make_pair(name, std::move(mark)));
		}
	}
	void end_cgpu(Key name) {
		auto mark_it = marks.find(name);
		if (mark_it != marks.end()) {
			mark_it->second.gpuq.end(qpool);
			mark_it->second.cpuq.end();
		}
		else {
			assert("Profiler: Missing begin call!");
		}
	}
	void begin_cpu(Key name) {
		auto mark_it = marks.find(name);
		if (mark_it != marks.end()) {
			mark_it->second.cpuq.begin();
		}
		else {
			Mark mark{};
			mark.cpuq.begin();
			marks.emplace(std::make_pair(name, std::move(mark)));
		}
	};
	void end_cpu(Key name) {
		auto mark_it = marks.find(name);
		if (mark_it != marks.end()) {
			mark_it->second.cpuq.end();
		}
		else {
			assert("Profiler: Missing begin call!");
		}
	};
};

struct Globals {
	struct RenderingGlobals {
		const std::set<gl::GLextension>& extensions;
	} rend;
};

struct Frame {
	int32_t id;
	Globals& glob;

	struct {
		int32_t geom_draw_calls;
		int32_t shadow_draw_calls;
	} renderer;

	Profiler& profiler;
};