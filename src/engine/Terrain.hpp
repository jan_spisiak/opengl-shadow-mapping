#pragma once

#include <render/Texture.hpp>
#include <render/Unit.hpp>
#include <render/Shader.hpp>

namespace grow
{
	class RenderSystem;
	class EntitySystem;

	class Terrain
	{
	public:
		Terrain(EntitySystem& es, RenderSystem& rs);

		float GetHeight(glm::ivec2 off);

		void Generate();
		void GuiUpdate();

	private:
		EntitySystem& es;
		RenderSystem& rs;
		//Buffer terrain_vbo;
		//VertexArray terrain_vao;
		ShaderProgram terrain_sp;
		ShaderProgram shadow_terrain_sp;

		float patch_size = 32.f;
		float height_scale = 32.f;
		std::vector<Entity> terrain_tiles;

		Texture terrain_tex;
		Texture terrain_normal_tex;
		uint32_t terrain_meshi;
		Material terrain_mat, shadow_terrain_mat;
		//StaticMesh tessterrain_mesh;
		Image terrain_img;
	};
}