#pragma once
#include <cstdint>
#include <vector>

#include "glm/glm.hpp"
#include "glm/gtc/quaternion.hpp"

namespace grow
{
	class EntitySystem;
	struct RenderUnit;

	class EntityHandle
	{
	public:
		using KeyType = uint64_t;

		EntityHandle(uint32_t id, uint32_t version);
		uint32_t id() const { return keyvalue & std::numeric_limits<uint32_t>::max(); }
		uint32_t version() const { return (uint32_t)(keyvalue >> 32); }
		KeyType key() const { return keyvalue; }

	private:
		KeyType keyvalue;
	};

	inline bool operator==(EntityHandle const& lhs, EntityHandle const& rhs)
	{
		return lhs.key() == rhs.key();
	}

	class Entity
	{
		friend class EntitySystem;
	public:
		using Handle = EntityHandle;

		~Entity();
		Entity(Entity const&) = delete;
		Entity(Entity&&) noexcept = default;
		Entity& operator=(Entity const&) = delete;
		Entity& operator=(Entity&&) noexcept = default;

		template<class U>
		void add(U&& u) { entity_s->addUnit(*this, std::move(u)); }
		template<class U>
		U& get() { return entity_s->getUnit<U>(ehandle); }

		operator Handle() const { return ehandle; }

	private:
		Entity(EntitySystem& entity_system, EntityHandle entity_handle);

		EntityHandle ehandle;
		EntitySystem* entity_s;
	};

	// Should split scene units for different systems?
	struct SceneUnit
	{
		typedef uint32_t handle;
		glm::quat orientation{};
		glm::vec3 position;
		glm::vec3 scale;
		glm::vec3 center;
		glm::vec3 extent;

		//	handle parent;
		//	handle next_sibling;
		//	handle first_child;
	public:
		SceneUnit() = default;
		SceneUnit(glm::vec3 position, glm::quat orientation = glm::quat(), glm::vec3 scale = glm::vec3(1.0f), glm::vec3 center = glm::vec3{ 0.0f }, glm::vec3 extent = glm::vec3{ 0.0f })
			: position(position), orientation(orientation), scale(scale), center(center), extent(extent) {};
		glm::vec3 fwd() const { return orientation * glm::vec3(0, 0, -1); }
		glm::vec3 up() const { return orientation * glm::vec3(0, 1, 0); }
	};
}

namespace std
{
	template<>
	struct hash<grow::EntityHandle>
	{
		size_t operator()(const grow::EntityHandle& key) const
		{
			return (size_t)key.key();
		}
	};
}