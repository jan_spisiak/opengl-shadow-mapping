#include "Entity.hpp"

#include "EntitySystem.hpp"
#include <render/RenderSystem.hpp>

namespace grow
{
	EntityHandle::EntityHandle(uint32_t id, uint32_t version)
		: keyvalue(((KeyType)version << 32) | (KeyType)id)
	{}

	Entity::~Entity()
	{
		entity_s->destroy(*this);
	}

	Entity::Entity(EntitySystem& entity_system, EntityHandle entity_handle)
		: entity_s(&entity_system)
		, ehandle(std::move(entity_handle))
	{}
}