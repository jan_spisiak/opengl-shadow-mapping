#pragma once
#include <SDL2/SDL.h>
//#include <GL/glew.h>
#include <glbinding/gl/gl.h>
#include <glm/glm.hpp>

#include "render/Shader.hpp"
#include "render/Buffer.hpp"
#include "render/Texture.hpp"
#include "util/Util.hpp"

class Window
{
	SDL_Window *handle;
	glm::ivec2 size_;
public:
	Window(const std::string& name, int sx, int sy, Uint32 flags);
	~Window() { SDL_DestroyWindow(handle); }

	operator SDL_Window*() const { return handle; }

	glm::uvec2 size() { SDL_GetWindowSize(handle, &(size_.x), &(size_.y)); return size_; }
	void Swap();
};

class Context
{
	SDL_GLContext handle;

	struct {
		ShaderProgram::Handle shader;
		VertexArray::Handle vao;
		std::array<Texture::Handle, 32> tex_slots;
	} bound;
public:
	struct State {

	};
	void Bind(const ShaderProgram& shaderp) {
		Bind(shaderp.get());
	}
	void Bind(const ShaderProgram::Handle& shader) {
		if (bound.shader != shader) {
			ShaderProgram::Bind(shader);
			bound.shader = shader;
		}
	}
	void Bind(const VertexArray& vao) {
		Bind(vao.raw());
	}
	void Bind(const VertexArray::Handle& vao) {
		if (bound.vao != vao) {
			VertexArray::Bind(vao);
			bound.vao = vao;
		}
	}
	void Bind(int slot, Texture const& tex ) {
		if (bound.tex_slots[slot] != tex.get()) {
			glActiveTexture(GL_TEXTURE0 + slot);
			tex.Bind();
			bound.tex_slots[slot] = tex.get();
		}
	}
	Context(const Window & window);
	~Context() { SDL_GL_DeleteContext(handle); }
};