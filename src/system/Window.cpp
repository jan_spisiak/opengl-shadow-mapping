#include "Window.hpp"

#include <glbinding/Binding.h>

using namespace gl;

Window::Window(const std::string& name, int sx, int sy, Uint32 flags)
{
	handle = SDL_CreateWindow(name.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, sx, sy, flags);

	if (!handle)
		die(__LINE__, " SDL_CreateWindow: ", SDL_GetError());

	size_ = glm::uvec2(sx, sy);
}

Context::Context(const Window & window)
{
	std::cout << "Creating 4.5 core context..\n";
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

	handle = SDL_GL_CreateContext(window);
	if (!handle)
		die(__LINE__, " SDL_GL_CreateContext: ", SDL_GetError());

	// Turn on vsync
	if (SDL_GL_SetSwapInterval(-1) == -1)
		SDL_GL_SetSwapInterval(1);

	//glewExperimental = GL_TRUE;
	//GLenum err = gl::glInitNames();
	//if (err != GLEW_OK)
	//	die(__LINE__, " glewInit: ", glewGetErrorString(err));
	glbinding::Binding::initialize();
}

void Window::Swap()
{
	SDL_GL_SwapWindow(handle);
}
