#pragma once

#include <string>
#include <vector>
#include <glbinding/gl/gl.h>

#include "system/Window.hpp"
#include "render/Texture.hpp"
#include "render/Shader.hpp"
#include "render/Buffer.hpp"
#include "util/Util.hpp"

struct FontParams
{
	glm::vec2 position;
	glm::vec2 size;
};

class UserInterface
{
protected:
	Window& window;
	Context& gl;

	ShaderProgram font_shader;
	Uniform<glm::vec2> font_uni_origin;
	Uniform<glm::vec2> font_uni_size;
	Uniform<GLint> font_vertex_offset;
	Texture font_tex;
	VertexArray font_vao;
	Buffer font_vbo;

	std::vector<std::string> font_strings;
	std::vector<FontParams> font_params;
public:
	UserInterface(Window& window, Context& gl)
		: window(window), gl(gl),
		font_shader(load_file("res/shaders/font.glsl")),
		font_vertex_offset((gl.Bind(font_shader), font_shader), "vertex_offset"),
		font_uni_origin(font_shader, "render_origin"),
		font_uni_size(font_shader, "render_size"),
		font_tex("res/textures/font.png"), font_vbo(GL_ARRAY_BUFFER, GL_DYNAMIC_DRAW)
	{
		gl.Bind(font_vao);
		font_vbo.Bind();
		glEnableVertexAttribArray(0);
		glVertexAttribIPointer(0, 1, GL_UNSIGNED_BYTE, 1, 0);
	}

	void LoadFont(const std::string& filename);
	void DrawString(const std::string& str, float x, float y, float size = 15.0f);
	void Render();
};