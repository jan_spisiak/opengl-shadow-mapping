#include "Buffer.hpp"
#include <iostream>

#include <glm/gtc/packing.hpp>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags
#include "util/Util.hpp"

using namespace gl;

Buffer::Buffer(GLenum target, GLsizeiptr size, const GLvoid* data, GLenum usage)
	: target(target), usage(usage), size(size)
{
	glGenBuffers(1, &handle);
	glBindBuffer(target, handle);
	glBufferData(target, size, data, usage);
}

bool operator==(const AttribFormat& lhs, const AttribFormat& rhs)
{
	if (lhs.count == rhs.count && lhs.type == rhs.type)
		return true;
	return false;
}
bool operator==(const VertexFormat& lhs, const VertexFormat& rhs)
{
	if (lhs.attribs == rhs.attribs)
		return true;
	return false;
}

//template<typename T>
bool VertexStorage<MeshVertex>::Load(const std::string& filename, uint32_t id)
{
	Assimp::Importer importer;

	const aiScene* scene = importer.ReadFile(filename,
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	if (!scene)
	{
		die("Assimp importer: ", importer.GetErrorString());
	}

	const aiMesh* ai_mesh = scene->mMeshes[id];
	std::cout << "Loading " << id << " " << ai_mesh->mName.C_Str() << ": " << ai_mesh->mNumVertices << " vertices " << ai_mesh->mNumFaces << " faces " << ai_mesh->HasTangentsAndBitangents() << "\n";

	glm::vec3 max = glm::vec3{ -std::numeric_limits<float>::max() }, min = glm::vec3{ std::numeric_limits<float>::max() };
	const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);
	assert(ai_mesh->mNumVertices <= UINT16_MAX);
	vertices.reserve(ai_mesh->mNumVertices);
	positions.reserve(ai_mesh->mNumVertices);
	for (unsigned int i = 0; i < ai_mesh->mNumVertices; i++) {
		const aiVector3D* pPos = &(ai_mesh->mVertices[i]);
		const aiVector3D* pNormal = &(ai_mesh->mNormals[i]);
		// checking inside loop is not nice
		const aiVector3D* pTangent = ai_mesh->HasTangentsAndBitangents() ? &(ai_mesh->mTangents[i]) : &Zero3D;
		const aiVector3D* pTexCoord = ai_mesh->HasTextureCoords(0) ? &(ai_mesh->mTextureCoords[0][i]) : &Zero3D;

		glm::vec3 pos = glm::vec3(pPos->x, pPos->y, pPos->z);
		max = glm::max(pos, max);
		min = glm::min(pos, min);
		MeshVertex v = {
			pos,
			glm::packHalf(glm::vec3(pNormal->x, pNormal->y, pNormal->z)),
			glm::packHalf(glm::vec3(pTangent->x, pTangent->y, pTangent->z)),
			glm::packHalf(glm::vec2(pTexCoord->x, pTexCoord->y))
		};

		vertices.push_back(v);
		positions.emplace_back(pPos->x, pPos->y, pPos->z);
	}

	center = (max + min) / 2.0f;
	extent = (max - min) / 2.0f;

	indices.reserve(ai_mesh->mNumFaces * 3);
	for (unsigned int i = 0; i < ai_mesh->mNumFaces; i++) {
		const aiFace& Face = ai_mesh->mFaces[i];
		assert(Face.mNumIndices == 3);
		indices.push_back(Face.mIndices[0]);
		indices.push_back(Face.mIndices[1]);
		indices.push_back(Face.mIndices[2]);
	}

	return true;
}

void MeshVertex::BindAttribs()
{
	gl::GLint pos_a = 0;
	gl::GLint normal_a = 1;
	gl::GLint tang_a = 2;
	gl::GLint uv_a = 3;

	glEnableVertexAttribArray(pos_a);
	glEnableVertexAttribArray(normal_a);
	glEnableVertexAttribArray(tang_a);
	glEnableVertexAttribArray(uv_a);

	glVertexAttribPointer(pos_a, 3, GL_FLOAT, GL_FALSE, sizeof(MeshVertex), (void*)offsetof(MeshVertex, pos));
	glVertexAttribPointer(normal_a, 3, GL_HALF_FLOAT, GL_FALSE, sizeof(MeshVertex), (void*)offsetof(MeshVertex, normal));
	glVertexAttribPointer(tang_a, 3, GL_HALF_FLOAT, GL_FALSE, sizeof(MeshVertex), (void*)offsetof(MeshVertex, tangent));
	glVertexAttribPointer(uv_a, 2, GL_HALF_FLOAT, GL_FALSE, sizeof(MeshVertex), (void*)offsetof(MeshVertex, uv));
}

void PosVertex::BindAttribs()
{
	GLint pos_a = 0;
	glEnableVertexAttribArray(pos_a);
	glVertexAttribPointer(pos_a, 3, GL_FLOAT, GL_FALSE, sizeof(PosVertex), (void*)offsetof(PosVertex, pos));
}

uint32_t AttribFormat::Sizeof(GLenum type)
{
	uint32_t type_size = 0;
	switch (type) {
	case GL_FLOAT:
		type_size = 4;
		break;
	case GL_HALF_FLOAT:
		type_size = 2;
		break;
	default:
		assert(false);
	}
	return type_size;
}
