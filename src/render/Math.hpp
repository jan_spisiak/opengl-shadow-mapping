#pragma once

#include <glm/glm.hpp>
#include <array>

struct AABB {
	glm::vec3 center;
	glm::vec3 extent;
};

void FrustumPlanes(const glm::mat4& mat, std::array<glm::vec4, 6>& planes);
int FrustumCullAABB(std::array<glm::vec4, 6>& planes, const glm::vec3& center, const glm::vec3& extent);
int FrustumCullSphere(std::array<glm::vec4, 6>& planes, const glm::vec3& pos, const float& radius);