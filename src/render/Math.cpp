#include "render/Math.hpp"

void FrustumPlanes(const glm::mat4& mat, std::array<glm::vec4, 6>& planes)
{
	glm::mat4 tmat = glm::transpose(mat);
	planes[0] = tmat[3] + tmat[0]; // left
	planes[1] = tmat[3] - tmat[0]; // right
	planes[2] = tmat[3] + tmat[1]; // bottom
	planes[3] = tmat[3] - tmat[1]; // top
	planes[4] = tmat[3] + tmat[2]; // near
	planes[5] = tmat[3] - tmat[2]; // far

	for (int p = 0; p < 6; ++p)
		planes[p] /= glm::length(static_cast<glm::vec3>(planes[p]));
}
int FrustumCullAABB(std::array<glm::vec4, 6>& planes, const glm::vec3& center, const glm::vec3& extent)
{
	int result = 2;
	for (int p = 0; p < 6; ++p) {
		float d = glm::dot(center, glm::vec3(planes[p]));
		float r = glm::dot(extent, glm::abs(glm::vec3(planes[p])));
		if (d + r < -planes[p].w)
			return 0;
		else if (d - r > -planes[p].w)
			result = 1;
	}
	return result;
}
int FrustumCullSphere(std::array<glm::vec4, 6>& planes, const glm::vec3& pos, const float& radius)
{
	int result = 2;
	for (int p = 0; p < 6; ++p) {
		float dist = glm::dot(planes[p], glm::vec4{ pos, 1.0f }) + radius;
		if (dist < 0.0f)
			return 0;
		else if (dist < radius)
			result = 1;
	}
	return result;
}
bool AABBTest(const AABB& a, const AABB& b)
{
	return glm::all(glm::lessThanEqual(abs(a.center - b.center), a.extent + b.extent));
}