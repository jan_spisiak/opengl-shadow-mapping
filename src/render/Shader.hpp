#pragma once

#include <array>
#include <string>
#include <unordered_map>

//#include <GL/glew.h>
//#include <GL/gl.h>
#include <glbinding/gl/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "util/Util.hpp"

using namespace gl;

class Shader
{
protected:
	using Handle = strong_type<Shader, gl::GLuint>;
protected:
	gl::GLenum type;
	Handle handle;
public:
	Shader() : handle({ 0 }) {}
	Shader(gl::GLenum type, const std::string& source, const std::vector<std::pair<std::string, std::string>>& defines = {});
	Shader(Shader&& other) : type(other.type), handle(other.handle) { other.handle = { 0 }; }
	~Shader() { gl::glDeleteShader(handle.value()); };
	gl::GLuint get() { return handle.value(); }
	operator gl::GLuint() const { return handle.value(); }

	//static void Bind(Handle handle) { gl::glUseProgram(handle.value()); }
	gl::GLenum getType() const { return type; }
	static std::string TypeToString(gl::GLenum type);
	static const std::array<gl::GLenum, 6> types;
};

class ShaderProgram
{
public:
	using Handle = strong_type<ShaderProgram, gl::GLuint>;
	Handle handle;
public:
	friend void swap(ShaderProgram& first, ShaderProgram& second) {
		using std::swap;
		swap(first.handle, second.handle);
	}

	ShaderProgram() : handle({ 0 }) {};
	ShaderProgram(const std::string& source, const std::vector<std::pair<std::string, std::string>>& defines = {});
	ShaderProgram(const std::vector<gl::GLenum>& types, const std::string& source);
	ShaderProgram(ShaderProgram&& other) : ShaderProgram() { swap(*this, other); }
	~ShaderProgram() { gl::glDeleteProgram(handle.value()); };
	ShaderProgram& operator=(ShaderProgram other) {
		using std::swap;
		swap(*this, other); return *this;
	}

	//void Compile();

	Handle get() const { return handle; }
	operator gl::GLuint() const { return handle.value(); }
	friend bool operator< (ShaderProgram const& lhs, ShaderProgram const& rhs) { return lhs.handle <  rhs.handle; }
	friend bool operator==(ShaderProgram const& lhs, ShaderProgram const& rhs) { return lhs.handle == rhs.handle; }
	friend bool operator!=(ShaderProgram const& lhs, ShaderProgram const& rhs) { return !(lhs.handle == rhs.handle); }
protected:
	friend class Context;
	const ShaderProgram& Bind() const { gl::glUseProgram(handle.value()); return *this; };
	static void Bind(Handle handle) { gl::glUseProgram(handle.value()); }
};

template<typename T>
class Uniform
{
protected:
	GLint loc_;
	void update_(float f1, float f2, float f3) { glUniform3f(loc_, f1, f2, f3); }
	void update_(const glm::mat3& val, GLsizei count = 1, GLboolean transpose = GL_FALSE) { glUniformMatrix3fv(loc_, count, transpose, glm::value_ptr(val)); }
	void update_(const glm::mat4& val, GLsizei count = 1, GLboolean transpose = GL_FALSE) { glUniformMatrix4fv(loc_, count, transpose, glm::value_ptr(val)); }
	void update_(const glm::vec2& val, GLsizei count = 1) { glUniform2fv(loc_, count, glm::value_ptr(val)); }
	void update_(const glm::vec3& val, GLsizei count = 1) { glUniform3fv(loc_, count, glm::value_ptr(val)); }
	void update_(const glm::vec4& val, GLsizei count = 1) { glUniform4fv(loc_, count, glm::value_ptr(val)); }
	void update_(const float& val) { glUniform1f(loc_, val); }
	void update_(const GLint& val) { glUniform1i(loc_, val); }
public:
	Uniform(GLuint shader, const std::string& target) { loc_ = glGetUniformLocation(shader, target.c_str()); };
	//void Link(GLuint shader, const std::string& target) { loc_ = glGetUniformLocation(shader, target.c_str()); }
	void Update(const T& val) { update_(val); }
	template<typename... Args>
	void Update(Args&& ... args) { update_(T(std::forward<Args>(args)...)); }
	//void Update(const T* val) { update_(val); };
	operator GLint() const { return loc_; }
};
