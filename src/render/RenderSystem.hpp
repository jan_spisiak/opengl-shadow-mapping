#pragma once

#include <numeric>
//#include <Windows.h>
//#include <GL/glew.h>
#include <glbinding/gl/gl.h>
//#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "render/Shader.hpp"
#include "render/Texture.hpp"
#include "system/Window.hpp"
#include "render/Buffer.hpp"
#include "render/Unit.hpp"
#include "util/Util.hpp"
#include "engine/Entity.hpp"
#include "engine/EntitySystem.hpp"
#include "render/UserInterface.hpp"
#include "engine/Frame.hpp"
#include "engine/Terrain.hpp"

namespace grow
{
	struct RenderSystemOptions {
		float time;
		bool exponential_shadows;
		bool is_camera_frozen;
		bool stabilize_shadows;
		float slider;
		bool layered_shadow_maps;
		bool sort_by_object;
	};

	class RenderSystem
	{
	protected:
		struct ShadowMap
		{
			Texture depth_tex;
			Texture blurred_tex;
			bool enabled;
			uint32_t splits_count;
			std::array<glm::mat4, 8> casc_mats;
			std::array<std::array<glm::vec4, 6>, 8> frustum_planes;
			std::array<float, 8> splits;
			std::array<float, 8> scales;
		};

		struct Camera {
			CameraUnit cmu;
			SceneUnit scu;
			glm::mat4 proj;
			glm::mat4 view;
			std::array<glm::vec4, 6> planes;
		} cam;
		Camera freeze_cam;
		struct DrawCommand {
			GLuint count;
			GLuint prim_count;
			GLuint first_index;
			GLuint base_vertex;
			GLuint base_instance;
		};

		struct RenderCommand {
			Material& mat() const { return *pmat; }

			//Shader::Handle shader_h;
			Material* pmat;
			int32_t ubo_offset;
			// It would be better to share VBO and just offset
			//const StaticMesh& mesh;
			VertexArray::Short vao_h;
			uint32_t mesh;
			//GLenum mode;
			//const StaticMesh* mesh;
			GLuint prim_count;
			GLuint layer;

			static bool ByMaterial(const RenderCommand& a, const RenderCommand & b);
			static bool ByLayer(const RenderCommand& a, const RenderCommand & b);
		};
		struct SkyPassBlock {
			glm::mat4 vp_inv;
			glm::vec3 sun_dir;
			glm::vec3 cam_pos;
		};
		struct MeshBufferDescriptor {
			VertexFormat format;
			Buffer posbuf;
			Buffer vertbuf;
			Buffer indbuf;
			//uint32_t vao_i;
			//uint32_t posvao_i;
			VertexArray vao;
			VertexArray posvao;
			uint32_t posbuf_last;
			uint32_t vertbuf_last;
			uint32_t indbuf_last;
			uint32_t index;
		};

		//std::vector<VertexArray> mesh_vaos;
		//std::vector<Buffer> mesh_vbos;
		struct MeshDescriptor {
			gl::GLenum mode;
			gl::GLenum index_type;
			//VertexFormat vertex_format;
			//uint32_t vao_i;
			//uint32_t posvao_i;
			//DrawCommand draw_c;
			uint32_t mesh_bd;
			GLuint vert_off;
			GLuint posvert_off;
			GLuint first_index;
			GLuint count;
			glm::vec3 center;
			glm::vec3 extent;
		};

		EntitySystem& entsys;
		Window& window;
		Context& gl;
		UserInterface& ui;

		// Needs to be on top
		Buffer inst_ind_buf{ gl::GL_ARRAY_BUFFER, 4 * 4096, gl::GL_STREAM_DRAW };
		Buffer shinst_ind_buf{ gl::GL_ARRAY_BUFFER, 4 * 4096, gl::GL_STREAM_DRAW };
		std::vector<MeshBufferDescriptor> mesh_buff_descs;
		std::vector<MeshDescriptor> mesh_descs;

		const uint32_t shadow_max_splits = 4;
		const glm::uvec2 shadow_split_size = { 2048, 2048 };
		const glm::uvec2 shadow_downsample_size = { 2048, 2048 };
		ShaderProgram shadow_sp;
		ShaderProgram shadow_blur_sp;
		typedef std::tuple<float, int> LightKey;
		std::vector<LightKey> shadow_light_sort;
		struct alignas(256) ShadowBlurBlock {
			glm::vec2 alignas(8) pixel_offset;
			uint32_t index;
		};
		Buffer shadow_blur_ubo;
		//Uniform<glm::vec2> shadow_pixel_offset_uni;
		FrameBuffer shadow_depth_fbo;
		FrameBuffer shadow_fbo;
		Texture shadow_tex_blur_pre;
		struct alignas(256) ShadowPassBlock {
			glm::mat4 vp;
			glm::mat4 cam_vp;
			uint32_t layer;
		};
		struct alignas(16) ShadowObjectBlock {
			glm::mat4 model;
		};
		//std::array<std::vector<ShadowObjectBlock>, 8> shadow_obj_blocks;
		std::vector<ShadowObjectBlock> shadow_obj_blocks;
		//std::array<std::vector<RenderCommand>, 8> shadow_render_queue;
		std::vector<RenderCommand> shadow_render_queue;
		Buffer shadow_pass_ubo;
		Buffer shadow_obj_ubos;
		uint32_t max_shadow_cube_maps = 6;
		glm::uvec2 shadow_cube_size = { 1024, 1024 };
		Texture shadow_cube_maps;
		ShadowMap sun_shadow;

		ShaderProgram sky_shader;
		Buffer sky_pass_ubo;

		struct alignas(256) IrradCompBlock {
			glm::mat4 vp_inv;
		};
		glm::uvec2 global_probe_size = { 256, 256 };
		glm::uvec2 irrad_probe_size = { 16, 16 };
		glm::quat global_orientation;
		Texture global_probe_hdr_tex;
		Texture global_probe_depth_tex;
		Texture irrad_probe_hdr_tex;
		FrameBuffer global_probe_fbo;
		FrameBuffer irrad_probe_fbo;
		ShaderProgram probe_sp;
		ShaderProgram probe_irradiance_sp;
		std::vector<IrradCompBlock> irrad_comp_blocks;
		Buffer irrad_comp_ubo;

		Buffer screen_vbo;
		VertexArray screen_vao;
		Material shadow_mat;

		struct ObjectBlock {
			glm::mat4 model;
			glm::vec3 color;
			float smoothness;
		};
		struct GeomPassBlock {
			glm::mat4 view_proj;
			glm::mat4 shadow_mvp;
			glm::mat4 real_view_proj;
			glm::vec4 param0;
		};
		Buffer geom_pass_ubo;
		std::vector<ObjectBlock> object_blocks;
		Buffer object_blocks_ubo;
		//std::vector<DrawCommand> draw_commands;
		std::vector<RenderCommand> render_queue;
		std::vector<ObjectBlock> fwdobject_blocks;
		std::vector<RenderCommand> forward_queue;
		//StaticMesh ndc_mesh;
		uint32_t ndc_meshi;

		struct SunLight {
			glm::vec4 color;
			glm::vec4 direction;
			std::array<glm::mat4, 8> vps;
			//std::array<float alignas(16), 8> splits;
			std::array<glm::vec4, 8> splits;
			std::array<glm::vec4, 8> scales;
			int max_split;
			float slider;
		};

		struct LightComputeBlock {
			glm::mat4 proj_inv;
			glm::mat4 view_inv;
			glm::mat4 proj;
			glm::mat4 view;
			glm::vec3 alignas(16) lin_depth_coef;
			glm::vec3 alignas(16) cam_pos_w;
			glm::uvec2 alignas(8) screen_size;
			uint32_t light_count;
			SunLight alignas(16) sun_light;
		};
		struct LightBlock {
			glm::vec4 color;
			glm::vec3 pos_w;
			float radius;
		};

		ShaderProgram light_compute_sp;
		Buffer light_pass_ubo;
		Buffer light_ubo;
		std::vector<LightBlock> lights;
		glm::ivec2 gbuf_size;
		FrameBuffer gbuf;
		FrameBuffer lbuf;
		Texture lbuf_color_tex;
		Texture lbuf_color_ldr_tex;
		Texture lbuf_depth_tex;
		Texture gbuf_albedo_tex;
		Texture gbuf_depth_tex;
		Texture gbuf_normal_tex;
		ShaderProgram color_post_sp;
		Uniform<float> exposure_uni;
		ShaderProgram blit_sp;
		ShaderProgram debug_depth_sp;

		ShaderProgram forward_wire_sp;
		Material forward_mat;
		Buffer forward_pass_ubo;
		Buffer forward_obj_ubo;
		//StaticMesh sphere_mesh;
		uint32_t sphere_meshi;

		//VertexArray static_mesh_vao;
		//VertexArray static_mesh_pos_vao;
		//Buffer static_mesh_pos_vbo;
		//Buffer static_mesh_vbo;
		//Buffer static_mesh_ibo;

		RenderSystemOptions options;
		bool switched_to_layered = false;
		bool switched_to_exponential = false;

		void AddSDraw(glm::mat4& model_mat, RenderUnit& rdu, GLuint layer);
		void AddGDraw(glm::mat4& model_mat, RenderUnit& rdu);
		void AddFDraw(glm::mat4& model_mat, RenderUnit& rdu);

		void ProbePass();
		void BucketDraws();
		void TesselationPass();
		void CascadeShadowPass();
		void ShadowPass(Frame& frame);
		void GeometryPass(Frame& frame);
		void LightPass(Frame& frame);
		void ForwardPass();
		void PostProcessPass();
	public:
		RenderSystem(Window& window, Context& context, EntitySystem& entsys, UserInterface& ui);

		enum FinalPassMode {
			Final,
			Normal,
			Shadow,
			Albedo
		};

		template<typename T>
		uint32_t createMesh(const VertexStorage<T>& storage, const VertexFormat& format);

		MeshBufferDescriptor& findMeshBuffer(const VertexFormat& format);

		void Render(Frame& frame, const RenderSystemOptions& opts);
		void FinalPass(FinalPassMode mode);
	};

	template<typename T>
	uint32_t RenderSystem::createMesh(const VertexStorage<T>& storage, const VertexFormat& format)
	{
		auto& buff_desc = findMeshBuffer(format);

		Buffer &vbo = buff_desc.vertbuf, &posvbo = buff_desc.posbuf, &ibo = buff_desc.indbuf;
		//auto& format = buff_desc.format;
		mesh_descs.push_back({ storage.mode, GL_UNSIGNED_SHORT, buff_desc.index, 0, 0, 0, static_cast<uint32_t>(storage.indices.size()), storage.center, storage.extent });
		auto& mesh_desc = mesh_descs.back();

		vbo.Bind();
		uint32_t vert_size = format.size;
		//Vertex data has to start on position aligned to vertex size
		uint32_t offset = align(buff_desc.vertbuf_last, vert_size);
		vbo.SubData(offset, storage.vertices);
		buff_desc.vertbuf_last = offset + static_cast<uint32_t>(storage.vertices.size()) * vert_size;
		mesh_desc.vert_off = offset / vert_size;

		uint32_t ind_size = 2;
		ibo.Bind();
		offset = align(buff_desc.indbuf_last, ind_size);
		ibo.SubData(offset, storage.indices);
		buff_desc.indbuf_last = offset + static_cast<uint32_t>(storage.indices.size()) * ind_size;
		mesh_desc.first_index = offset;

		uint32_t posvert_size = static_cast<uint32_t>(format.attribs[0].size());
		posvbo.Bind();
		offset = align(buff_desc.posbuf_last, posvert_size);
		posvbo.SubData(offset, storage.positions);
		buff_desc.posbuf_last = offset + static_cast<uint32_t>(storage.positions.size()) * posvert_size;
		mesh_desc.posvert_off = offset / posvert_size;

		std::cout << mesh_descs.size() - 1 << ": " << mesh_desc.mesh_bd << " " << mesh_desc.count <<
			" " << mesh_desc.vert_off << ", " << mesh_desc.first_index << ", " << mesh_desc.posvert_off << "\n";
		return mesh_descs.size() - 1;
	}
}