#include "Shader.hpp"
#include "util/Util.hpp"

using namespace gl;

const std::array<GLenum, 6> types = { GL_FRAGMENT_SHADER, GL_VERTEX_SHADER, GL_COMPUTE_SHADER, GL_GEOMETRY_SHADER, GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER };

ShaderProgram::ShaderProgram(const std::string & source, const std::vector<std::pair<std::string, std::string>>& defines)
	: handle({ 0 })
{
	handle = Handle{ glCreateProgram() };
	std::vector<Shader> shaders;
	for (auto type : types)
	{
		if (source.find(Shader::TypeToString(type)) != std::string::npos) {
			shaders.emplace_back(type, source, defines);
			glAttachShader(handle.value(), shaders.back());
		}
	}
	glLinkProgram(handle.value());

	GLint program_linked;
	glGetProgramiv(handle.value(), GL_LINK_STATUS, &program_linked);
	if (program_linked != GLint{ GL_TRUE })
	{
		GLsizei log_length = 0;
		GLchar message[1024];
		glGetProgramInfoLog(handle.value(), 1024, &log_length, message);
		die("Linking shader program.\n", message);
	}

	for (auto& shader : shaders) {
		glDetachShader(handle.value(), shader);
	}
}

ShaderProgram::ShaderProgram(const std::vector<GLenum>& types, const std::string & source)
	: handle({ 0 })
{
	handle = Handle{ glCreateProgram() };
	std::vector<Shader> shaders;
	for (auto type : types)
	{
		//if (source.find(Shader::TypeToString(type)) != std::string::npos) {
			shaders.emplace_back(type, source);
			glAttachShader(handle.value(), shaders.back());
		//}
	}
	glLinkProgram(handle.value());

	GLint program_linked;
	glGetProgramiv(handle.value(), GL_LINK_STATUS, &program_linked);
	if (program_linked != GLint{ GL_TRUE })
	{
		GLsizei log_length = 0;
		GLchar message[1024];
		glGetProgramInfoLog(handle.value(), 1024, &log_length, message);
		// Write the error to a log
		die("Linking shader program.\n", message);
	}

	for (auto& shader : shaders) {
		glDetachShader(handle.value(), shader);
	}
}

Shader::Shader(GLenum type, const std::string & source, const std::vector<std::pair<std::string, std::string>>& defines)
	: type(type), handle({ 0 })
{
	handle = { glCreateShader(type) };
	std::string version_directive = source.substr(0, source.find("\n") + 1);
	std::string shader_defines = "#define " + TypeToString(type) + "\r\n";
	for ( auto& define : defines)
	{
		shader_defines += "#define " + define.first + " " + define.second + "\r\n";
	}
	GLint sources_lengths[] = { (GLint)version_directive.length(), (GLint)shader_defines.length(), (GLint)source.length() - (GLint)version_directive.length() };
	const GLchar * sources[] = { version_directive.c_str(), shader_defines.c_str(), source.c_str() + version_directive.length() };
	glShaderSource(handle.value(), 3, sources, sources_lengths);
	glCompileShader(handle.value());
	auto fragment_compiled = GLint{ GL_FALSE };
	glGetShaderiv(handle.value(), GL_COMPILE_STATUS, &fragment_compiled);
	if (fragment_compiled != GLint{ GL_TRUE })
	{
		GLsizei log_length = 0;
		GLchar message[1024];
		glGetShaderInfoLog(handle.value(), 1024, &log_length, message);
		// Write the error to a log
		die("Compiling shader.\n", message);
	}
}

std::string Shader::TypeToString(GLenum type)
{
	const char* ret = "UNKNOWN_TYPE";
	switch (type) {
	case GL_VERTEX_SHADER:
		ret = "GLX_VERTEX_SHADER"; break;
	case GL_GEOMETRY_SHADER:
		ret = "GLX_GEOMETRY_SHADER"; break;
	case GL_TESS_CONTROL_SHADER:
		ret = "GLX_TESS_CONTROL_SHADER"; break;
	case GL_TESS_EVALUATION_SHADER:
		ret = "GLX_TESS_EVALUATION_SHADER"; break;
	case GL_COMPUTE_SHADER:
		ret = "GLX_COMPUTE_SHADER"; break;
	case GL_FRAGMENT_SHADER:
		ret = "GLX_FRAGMENT_SHADER"; break;
	}
	return ret;
}
