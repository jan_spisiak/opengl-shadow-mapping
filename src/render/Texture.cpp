#include "Texture.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "system/stb_image.h"
#include "util/Util.hpp"

#include <FastNoiseSIMD.h>

using namespace gl;

Texture::Texture(const GLchar* path)
{
	glGenTextures(1, &handle.value());
	Image image(path);

	GLenum mode = image.glSymbFormat();
	GLenum imode = GL_RGBA8;
	GLenum type = GL_UNSIGNED_BYTE;
	if (image.is_float) {
		type = GL_FLOAT;
		imode = GL_RGBA16F;
	}
	else {
		if (image.bpp == 16)
			type = GL_UNSIGNED_SHORT;
	}

	target = GL_TEXTURE_2D;
	glBindTexture(target, handle.value());
	glTexImage2D(target, 0, imode, image.size.x, image.size.y, 0, mode, type, &*image.data);

	glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

GLenum Image::glSymbFormat()
{
	GLenum mode = GL_RGBA;
	if (channels == 3) {
		mode = GL_RGB;
	} else if (channels == 2) {
		mode = GL_RG;
	} else if (channels == 1) {
		mode = GL_RED;
	}
	return mode;
}

Image::Image(const std::string & path)
{
	int x, y, comp;
	if (stbi_is_hdr(path.c_str()) == 0) {
		stbi_uc* new_data = stbi_load(path.c_str(), &x, &y, &comp, 0);
		if (!new_data) {
			die("IMG_Load: %s\n", stbi_failure_reason());
		}
		is_float = false;
		data = std::unique_ptr<data_t, void(*)(data_t*)>((data_t*)new_data, StbiDeleter);
		bpp = 8;
	}
	else {
		float* new_data = stbi_loadf(path.c_str(), &x, &y, &comp, 0);
		if (!new_data) {
			die("IMG_Load: %s\n", stbi_failure_reason());
		}
		is_float = true;
		data = std::unique_ptr<data_t, void(*)(data_t*)>((data_t*)new_data, StbiDeleter);
		bpp = sizeof(float)*8;
	}
	channels = comp;
	size.x = x; size.y = y; size.z = 0;
}

Image Image::Random(glm::ivec3 size)
{
	Image img;
	FastNoiseSIMD* myNoise = FastNoiseSIMD::NewFastNoiseSIMD();
	float* noiseSet = myNoise->GetPerlinFractalSet(0, 0, 0, size.x, size.y, size.z);
	img.data = std::unique_ptr<data_t, void(*)(data_t*)>((data_t*)noiseSet, NoiseDeleter);
	img.size = size;
	img.channels = size.z;

	return std::move(img);
}

void Image::StbiDeleter(data_t * addr)
{
	stbi_image_free(addr);
}

void Image::NoiseDeleter(data_t * addr)
{
	FastNoiseSIMD::FreeNoiseSet((float*)addr);
}
