#include "render/RenderSystem.hpp"

#include <chrono>

#include "render/imgui/imgui.h"
#include "render/Blurs.hpp"
#include "render/Math.hpp"
#include <glm/gtx/transform.hpp>
#include "engine/Frame.hpp"
#include "util/profile.h"

using namespace gl;
using namespace grow;

#define GL_ERROR() std::cout << __LINE__ << " " << glGetError() << " " << glGetError() << "\n";
namespace {
	// Quad vertices
	GLfloat screen_vertices[] = {
		 1.0f, -1.0f,  1.0f, 0.0f,
		 1.0f,  1.0f,  1.0f, 1.0f,
		-1.0f,  1.0f,  0.0f, 1.0f,

		-1.0f,  1.0f,  0.0f, 1.0f,
		-1.0f, -1.0f,  0.0f, 0.0f,
		 1.0f, -1.0f,  1.0f, 0.0f
	};

	glm::vec3 ndc_cube[] = { {-1, -1, -1}, {1, -1, -1}, { -1, 1, -1 }, { 1, 1, -1 },
		{ -1, -1, 1 },{ 1, -1, 1 },{ -1, 1, 1 },{ 1, 1, 1 } };
	std::vector<uint16_t> ndc_indices{ 0, 1, 1, 3, 3, 2, 2, 0, 0, 4, 1, 5, 2, 6, 3, 7, 4, 5, 5, 7, 7, 6, 6, 4 };
	VertexStorage<PosVertex> ndc_storage{ GLenum::GL_LINES, std::vector<PosVertex>(ndc_cube, ndc_cube + 8), ndc_indices };

	const VertexStorage<MeshVertex>& GetSphereStorage()
	{
		static VertexStorage<MeshVertex> sphere_storage("res/pack/test/sphere.obj", 0);
		return sphere_storage;
	}

	glm::mat4 bias_matrix{
		0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0
	};
}

#define UBO_USAGE GL_STREAM_DRAW
RenderSystem::RenderSystem(Window & window, Context & context, EntitySystem & entsys, UserInterface& ui)
	: window(window), gl(context), entsys(entsys), ui(ui),
	shadow_sp({ GL_VERTEX_SHADER, GL_FRAGMENT_SHADER }, load_file("res/shaders/shadow.glsl")),
	shadow_blur_sp(load_file("res/shaders/shadow_blur.glsl"), { {"GAUSS_CONSTANTS", GenerateGaussShaderKernelWeightsAndOffsets(7, true)} }),
	shadow_blur_ubo(GL_UNIFORM_BUFFER, 2 * 8 * sizeof(ShadowBlurBlock), UBO_USAGE),
	//shadow_pixel_offset_uni(shadow_blur_sp.Bind(), "pixel_offset"),
	shadow_tex_blur_pre(GL_TEXTURE_2D_ARRAY),
	sun_shadow{{ GL_TEXTURE_2D_ARRAY }, { GL_TEXTURE_2D_ARRAY } },
	//shadow_obj_ubos(GL_UNIFORM_BUFFER, sizeof(ShadowObjectBlock) * 1024 * 4, UBO_USAGE),
	shadow_obj_ubos(GL_SHADER_STORAGE_BUFFER, sizeof(ShadowObjectBlock) * 1024 * 4, UBO_USAGE),
	shadow_pass_ubo(GL_UNIFORM_BUFFER, 8 * sizeof(ShadowPassBlock), UBO_USAGE),
	shadow_cube_maps( GL_TEXTURE_CUBE_MAP_ARRAY ),

	//sky_shader(load_file("res/shaders/sky.vert"), load_file("res/shaders/sky.frag")),
	sky_shader(load_file("res/shaders/sky_model.glsl")),
	sky_pass_ubo(GL_UNIFORM_BUFFER, sizeof(SkyPassBlock), UBO_USAGE),
	global_probe_depth_tex(GL_TEXTURE_CUBE_MAP), global_probe_hdr_tex(GL_TEXTURE_CUBE_MAP),
	irrad_probe_hdr_tex(GL_TEXTURE_CUBE_MAP),
	probe_sp(load_file("res/shaders/probe.glsl")), probe_irradiance_sp(load_file("res/shaders/irradiance.glsl")),
	irrad_comp_ubo(GL_UNIFORM_BUFFER, sizeof(IrradCompBlock) * 6, UBO_USAGE),

	screen_vbo(GL_ARRAY_BUFFER, sizeof(screen_vertices), screen_vertices, GL_STATIC_DRAW), screen_vao(),
	shadow_mat{ shadow_sp },

	geom_pass_ubo(GL_UNIFORM_BUFFER, sizeof(GeomPassBlock), UBO_USAGE), object_blocks_ubo(GL_UNIFORM_BUFFER, sizeof(ObjectBlock) * 4 * 2048, UBO_USAGE),
	//ndc_mesh(ndc_storage),
	ndc_meshi(createMesh(ndc_storage, VertexFormat{ {{3, GL_FLOAT}}, 3*4 })),
	color_post_sp(load_file("res/shaders/color_post.glsl")),
	exposure_uni((gl.Bind(color_post_sp), color_post_sp), "exposure"),
	blit_sp(load_file("res/shaders/copy.glsl")),
	debug_depth_sp(load_file("res/shaders/debug_depth.glsl")),
	//dir_light_shadow_sp(load_file("res/shaders/dir_light_shadow.glsl")),
	light_compute_sp(load_file("res/shaders/light_compute.glsl")),
	light_pass_ubo(GL_UNIFORM_BUFFER, sizeof(LightComputeBlock), UBO_USAGE), light_ubo(GL_UNIFORM_BUFFER, sizeof(LightBlock) * 1024, UBO_USAGE),

	forward_wire_sp(load_file("res/shaders/forward_wire.glsl")),
	forward_mat{ forward_wire_sp, {}, {}, 0 },
	forward_pass_ubo(GL_UNIFORM_BUFFER, sizeof(GeomPassBlock), UBO_USAGE), forward_obj_ubo(GL_UNIFORM_BUFFER, sizeof(ObjectBlock) * 2048, UBO_USAGE),
	//sphere_mesh(sphere_storage),
	sphere_meshi(createMesh(GetSphereStorage(), VertexFormat::DefaultFormat())),

	gbuf_size(window.size().x, window.size().y),
	lbuf_color_tex(GL_TEXTURE_2D), lbuf_color_ldr_tex(GL_TEXTURE_2D), lbuf_depth_tex(GL_TEXTURE_2D),
	gbuf_albedo_tex(GL_TEXTURE_2D), gbuf_depth_tex(GL_TEXTURE_2D), gbuf_normal_tex(GL_TEXTURE_2D)
{
	//std::cout << "init0 " << glGetError() << " " << glGetError() << "\n";
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	// Setup the full screen draw_c
	{
		gl.Bind(screen_vao);
		screen_vbo.Bind();
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));
		gl.Bind(VertexArray::Handle{ 0 });
	}

	// Gbuf
	{
		gbuf.Bind(GL_FRAMEBUFFER);
		// Gbuf Render Targets
		gbuf_albedo_tex.Bind().Param(GL_TEXTURE_MIN_FILTER, GL_LINEAR).Param(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, gbuf_size.x, gbuf_size.y);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gbuf_albedo_tex, 0);
		gbuf_normal_tex.Bind().Param(GL_TEXTURE_MIN_FILTER, GL_NEAREST).Param(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB10_A2, gbuf_size.x, gbuf_size.y);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gbuf_normal_tex, 0);
		// Gbuf depth
		gbuf_depth_tex.Bind().Param(GL_TEXTURE_MIN_FILTER, GL_NEAREST).Param(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32, gbuf_size.x, gbuf_size.y);
		//glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, gbuf_depth_tex, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, gbuf_depth_tex, 0);
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			die("Incomplete gbuf.");
	}

	// Lbuf
	{
		lbuf.Bind(GL_FRAMEBUFFER);
		lbuf_color_ldr_tex.Bind().Param(GL_TEXTURE_MIN_FILTER, GL_LINEAR).Param(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, gbuf_size.x, gbuf_size.y);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, lbuf_color_ldr_tex, 0);
		lbuf_color_tex.Bind().Param(GL_TEXTURE_MIN_FILTER, GL_LINEAR).Param(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA16F, gbuf_size.x, gbuf_size.y);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, lbuf_color_tex, 0);
		//lbuf_depth_tex.Bind().Param(GL_TEXTURE_MIN_FILTER, GL_LINEAR).Param(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		lbuf_depth_tex.Bind().Param(GL_TEXTURE_MIN_FILTER, GL_NEAREST).Param(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		//glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, gbuf_size.x, gbuf_size.y, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32, gbuf_size.x, gbuf_size.y);
		//glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, lbuf_depth_tex, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, lbuf_depth_tex, 0);
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			die("Incomplete lbuf.");
	}

	// Create the shadow maps
	{
		//std::cout << "init0 " << glGetError() << " " << glGetError() << "\n";
		shadow_depth_fbo.Bind(GL_FRAMEBUFFER);
		sun_shadow.depth_tex.Bind()
			.Param(GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE)
			.Param(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER).Param(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER)
			.Param(GL_TEXTURE_MIN_FILTER, GL_LINEAR).Param(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		GLfloat depth_border[] = { 1.0, 1.0, 1.0, 1.0 };
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, depth_border);
		glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_DEPTH_COMPONENT32F, shadow_split_size.x, shadow_split_size.y, 8);
		//std::cout << "init1 " << glGetError() << " " << glGetError() << "\n";
		glDrawBuffer(GL_NONE);
		glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, sun_shadow.depth_tex, 0, 0);
		//std::cout << "init2 " << glGetError() << " " << glGetError() << "\n";
		GLenum status;
		if ((status = glCheckFramebufferStatus(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE)
			die("Incomplete shadow_depth_fbo. ", status);

		if (false) { // doesn't work
			shadow_cube_maps.Bind()
				.Param(GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE)
				.Param(GL_TEXTURE_MIN_FILTER, GL_LINEAR).Param(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexStorage3D(GL_TEXTURE_CUBE_MAP_ARRAY, 1, GL_DEPTH_COMPONENT32F, shadow_cube_size.x, shadow_cube_size.y, max_shadow_cube_maps * 6);
		}

		shadow_fbo.Bind(GL_FRAMEBUFFER);
		shadow_tex_blur_pre.Bind()
			//.Param(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER) .Param(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER)
			.Param(GL_TEXTURE_MIN_FILTER, GL_LINEAR) .Param(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		//glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, depth_border);
		glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_R32F, shadow_downsample_size.x, shadow_downsample_size.y, 8);
		glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, shadow_tex_blur_pre, 0, 0);
		sun_shadow.blurred_tex.Bind()
			//.Param(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER) .Param(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER)
			.Param(GL_TEXTURE_MIN_FILTER, GL_LINEAR) .Param(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		//glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, depth_border);
		glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_R32F, shadow_downsample_size.x, shadow_downsample_size.y, 8);
		glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, sun_shadow.blurred_tex, 0, 0);
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			die("Incomplete shadow_fbo.");
	}

	// Global light probe
	{
		global_probe_fbo.Bind(GL_FRAMEBUFFER);
		global_probe_depth_tex.Bind()
			.Param(GL_TEXTURE_WRAP_S, GL_CLAMP).Param(GL_TEXTURE_WRAP_T, GL_CLAMP).Param(GL_TEXTURE_WRAP_T, GL_CLAMP)
			.Param(GL_TEXTURE_MIN_FILTER, GL_LINEAR).Param(GL_TEXTURE_MAG_FILTER, GL_LINEAR)
			.Param(GL_TEXTURE_BASE_LEVEL, 0).Param(GL_TEXTURE_MAX_LEVEL, 0);
		for (int face = 0; face < 6; ++face)
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, 0, GL_DEPTH_COMPONENT32F, global_probe_size.x, global_probe_size.y, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
		global_probe_hdr_tex.Bind()
			.Param(GL_TEXTURE_WRAP_S, GL_CLAMP).Param(GL_TEXTURE_WRAP_T, GL_CLAMP).Param(GL_TEXTURE_WRAP_T, GL_CLAMP)
			.Param(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR).Param(GL_TEXTURE_MAG_FILTER, GL_LINEAR)
			.Param(GL_TEXTURE_BASE_LEVEL, 0).Param(GL_TEXTURE_MAX_LEVEL, 8);
		for (int face = 0; face < 6; ++face)
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, 0, GL_RGBA16F, global_probe_size.x, global_probe_size.y, 0, GL_RGBA, GL_FLOAT, NULL);

		//glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, global_probe_depth_tex, 0);
		//glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, global_probe_hdr_tex, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_CUBE_MAP_POSITIVE_X, global_probe_depth_tex, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X, global_probe_hdr_tex, 0);
		glDrawBuffer(GL_COLOR_ATTACHMENT0);
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			die("Incomplete global_probe_fbo.");

		irrad_probe_fbo.Bind(GL_FRAMEBUFFER);
		irrad_probe_hdr_tex.Bind()
			.Param(GL_TEXTURE_WRAP_S, GL_CLAMP).Param(GL_TEXTURE_WRAP_T, GL_CLAMP).Param(GL_TEXTURE_WRAP_T, GL_CLAMP)
			.Param(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR).Param(GL_TEXTURE_MAG_FILTER, GL_LINEAR)
			.Param(GL_TEXTURE_BASE_LEVEL, 0).Param(GL_TEXTURE_MAX_LEVEL, 1);
		for (int face = 0; face < 6; ++face)
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, 0, GL_RGBA16F, irrad_probe_size.x, irrad_probe_size.y, 0, GL_RGBA, GL_FLOAT, NULL);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X, irrad_probe_hdr_tex, 0);
		glDrawBuffer(GL_COLOR_ATTACHMENT0);
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			die("Incomplete irrad_probe_fbo.");
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	int align = 0;
	glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &align);
	if (align != 256) {
		std::cout << "GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT not 256. We have a problem Houston.\n";
		assert(align == 256);
	}
}

void RenderSystem::ProbePass()
{
	Manager<SceneUnit>& scum = entsys.getManager<SceneUnit>();
	Manager<RenderUnit>& rdum = entsys.getManager<RenderUnit>();
	Manager<LightUnit>& lium = entsys.getManager<LightUnit>();
	auto& sun_liu = lium.get(lium.getHandle(0));
	auto& sun_scu = sun_liu.scu;

	// Render Global Probe
	glBindFramebuffer(GL_FRAMEBUFFER, global_probe_fbo);
	glViewport(0, 0, global_probe_size.x, global_probe_size.y);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_CULL_FACE);

	//glDisable(GL_CULL_FACE);
	glDepthFunc(GL_GEQUAL);
	gl.Bind(probe_sp);
	gl.Bind(screen_vao);

	irrad_comp_blocks.clear();
	for (int i = 0; i < 6; ++i) {
		//glBindTexture(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, global_probe_hdr_tex);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, global_probe_depth_tex, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, global_probe_hdr_tex, 0);
		glViewport(0, 0, global_probe_size.x, global_probe_size.y);
		//glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP, global_probe_hdr_tex, 0);
		//glDrawBuffer(GL_COLOR_ATTACHMENT0);
		glm::mat4 proj = glm::perspective(glm::radians(90.0), 1.0, 0.5, 512.0);
		glm::vec3 viewatoff{ (i == 0) - (i == 1), (i == 2) - (i == 3), (i == 4) - (i == 5) };
		glm::vec3 up = { 0, -((i < 2) || (i > 3)), (i == 2) - (i == 3) };
		glm::mat4 view = glm::lookAt(cam.scu.position, cam.scu.position + viewatoff, up);
		glm::mat4 vp_inv = glm::mat4(glm::mat3(glm::inverse(view))) * glm::inverse(proj);
		SkyPassBlock probe_pass = { vp_inv, glm::normalize(-sun_scu.fwd()), cam.scu.position };
		sky_pass_ubo.BindBase(0);
		sky_pass_ubo.SubData(0, sizeof(SkyPassBlock), &probe_pass);
		gl.Bind(screen_vao);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		viewatoff = { (i == 0) - (i == 1), (i == 3) - (i == 2), (i == 4) - (i == 5) };
		up = { 0, -((i < 2) || (i > 3)), (i == 3) - (i == 2) };
		glm::mat4 irrad_view = glm::lookAt(glm::vec3{}, viewatoff, up);
		irrad_comp_blocks.push_back({ irrad_view });
	}
	irrad_comp_ubo.BindBase(0);
	irrad_comp_ubo.SubData(0, irrad_comp_blocks);

	gl.Bind(0, global_probe_hdr_tex);
	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
	glBindFramebuffer(GL_FRAMEBUFFER, irrad_probe_fbo);
	glViewport(0, 0, irrad_probe_size.x, irrad_probe_size.y);
	glDisable(GL_DEPTH_TEST);
	gl.Bind(probe_irradiance_sp);
	for (int i = 0; i < 6; ++i) {
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, irrad_probe_hdr_tex, 0);
		irrad_comp_ubo.BindRange(0, i * sizeof(IrradCompBlock), sizeof(IrradCompBlock));
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}
}

void RenderSystem::AddSDraw(glm::mat4& model_mat, RenderUnit& rdu, GLuint layer)
{
	shadow_obj_blocks.push_back({ model_mat });
	auto& mbd = mesh_buff_descs[mesh_descs[rdu.mesh_desc].mesh_bd];
	shadow_render_queue.push_back({
		&(rdu.material),
		(int32_t)shadow_obj_blocks.size() - 1,
		mbd.posvao.raw(),
		rdu.mesh_desc,
		1,
		layer });
}
void RenderSystem::AddGDraw(glm::mat4& model_mat, RenderUnit& rdu)
{
	object_blocks.push_back({ model_mat, rdu.material.color, rdu.material.smoothness });
	auto& mbd = mesh_buff_descs[mesh_descs[rdu.mesh_desc].mesh_bd];
	render_queue.push_back({
		&(rdu.material),
		(int32_t)object_blocks.size() - 1,
		mbd.vao.raw(),
		rdu.mesh_desc,
		1,
		0
	});
}

void RenderSystem::BucketDraws()
{
	Manager<SceneUnit>& scum = entsys.getManager<SceneUnit>();
	Manager<RenderUnit>& rdum = entsys.getManager<RenderUnit>();

	object_blocks.clear(); shadow_obj_blocks.clear();
	render_queue.clear(); shadow_render_queue.clear();

	// entities
	for (size_t i = 0; i < scum.size(); ++i)
	{
		auto eh = scum.getHandle(i);
		SceneUnit& scu = scum.getRef(i);
		if (!rdum.find(eh))
			continue;
		RenderUnit& rdu = rdum.get(eh);
		glm::mat4 model = glm::translate(scu.position) * glm::mat4_cast(scu.orientation) * glm::scale(scu.scale);
		// Cull for cascade shadows
		if (sun_shadow.enabled) {
			for (uint32_t sp = 0; sp < sun_shadow.splits_count; ++sp) {
				if (FrustumCullAABB(sun_shadow.frustum_planes[sp], scu.center, scu.extent) > 0) {
					AddSDraw(model, RenderUnit{ rdu.shadow_material ? *rdu.shadow_material : shadow_mat, rdu.mesh_desc }, sp);
				}
			}
		}
		if (FrustumCullAABB(cam.planes, scu.center, scu.extent) > 0) {
			model = glm::translate(scu.position) * glm::mat4_cast(scu.orientation) * glm::scale(scu.scale);
			AddGDraw(model, rdu);
		}
		// Cull for point lights
		/*if (false) { // turned off
		for (int i = 0; i < max_shadow_cube_maps; ++i) {
		auto& liu = lium.getRef(std::get<1>(shadow_light_sort[i]));
		for (int v = 0; v < 6; ++v) {
		glm::mat4 vp = {};
		if (FrustumCullAABB(point_frustums[i * 6 + v], scu.center, scu.extent) > 0) {
		glm::vec4 pos = point_vps[i * 6 + v] * model * glm::vec4{ scu.center, 1 };
		pos /= pos.w;
		AddSDraw(model, RenderUnit{ Material{ shadow_sp }, rdu.mesh_desc }, (GLuint)i * 6 + v );
		}
		}
		}
		}*/
	}
}

void RenderSystem::CascadeShadowPass()
{
	Manager<SceneUnit>& scum = entsys.getManager<SceneUnit>();
	Manager<RenderUnit>& rdum = entsys.getManager<RenderUnit>();
	Manager<LightUnit>& lium = entsys.getManager<LightUnit>();
	auto& sun_liu = lium.get(lium.getHandle(0));
	auto& sun_scu = sun_liu.scu;

	Camera& cam = freeze_cam;
	sun_shadow.splits_count = 4;
	uint32_t num_splits = sun_shadow.splits_count;
	//sun_shadow.splits = { 0.01f, 0.03f, 0.09f, 0.36f };
	//sun_shadow.splits = { 0.02f, 0.04f, 0.09f, 0.16f, 0.36f };
	sun_shadow.splits = { 0.015f, 0.03f, 0.07f, 0.14f, 0.28f, 0.56f };
	for (uint32_t sp = 0; sp < num_splits; ++sp)
	{
		float nearp_split = (sp > 0) ? sun_shadow.splits[sp - 1] : 0.0f;
		float farp_split = sun_shadow.splits[sp];
		float c = 0, r = 0;
		{
			float nearp_dist = cam.cmu.nearp + nearp_split * (cam.cmu.farp - cam.cmu.nearp);
			float farp_dist = cam.cmu.nearp + farp_split * (cam.cmu.farp - cam.cmu.nearp);
			float h = 2 * nearp_dist * tan(cam.cmu.fovy / 2.0f);
			float w = h*cam.cmu.ratio;
			float u_sq = (h*h + w*w) / 4.0f;
			float sn_squared = u_sq + nearp_dist*nearp_dist;
			float s_ratio = farp_dist / nearp_dist;
			c = sn_squared * (s_ratio * s_ratio - 1) / (2.0f * (farp_dist - nearp_dist));
			//c = glm::min(farp_dist, c);
			r = sqrt((farp_dist - c)*(farp_dist - c) + s_ratio * s_ratio * u_sq);
		}
		glm::vec3 frustum_mid = cam.scu.position + cam.scu.fwd() * c;
		glm::vec3 lview_up = glm::vec3(0, 1, 0);
		glm::mat4 lview_mat = glm::lookAt(frustum_mid, frustum_mid + glm::normalize(sun_scu.fwd()), lview_up);
		glm::vec4 cent, dim;

		cent = lview_mat * glm::vec4{ frustum_mid, 1 };
		dim = { r, r, r, 0 };
		glm::mat4 lproj_mat = glm::ortho<float>(cent.x - dim.x, cent.x + dim.x, cent.y - dim.y, cent.y + dim.y, -(cent.z + dim.z), -(cent.z - dim.z));

		if (options.stabilize_shadows) {
			float scale = shadow_split_size.x / 2.0f;
			glm::vec4 origin = lproj_mat * lview_mat * glm::vec4{ 0,0,0,1 };
			origin *= shadow_split_size.x / 2.0f;
			glm::vec4 grid_origin = glm::round(origin);
			glm::vec4 offset = (grid_origin - origin) * (2.0f / shadow_split_size.x);
			lproj_mat[3][0] += offset.x;
			lproj_mat[3][1] += offset.y;
		}

		// scales used for normal bias
		sun_shadow.scales[sp] = 2.0f / lproj_mat[0][0] / shadow_split_size.x;
		// cascade matrices
		sun_shadow.casc_mats[sp] = lproj_mat * lview_mat;
	}

	for (int sp = num_splits - 1; sp >= 0; --sp) {
		glm::mat4 mat = sun_shadow.casc_mats[sp];
		FrustumPlanes(mat, sun_shadow.frustum_planes[sp]);
		// Pull near plane to encompass all shadow casters
		sun_shadow.frustum_planes[sp][4].w += 1000.0f;
	}
}

void RenderSystem::ShadowPass(Frame& frame)
{
	Manager<SceneUnit>& scum = entsys.getManager<SceneUnit>();
	Manager<RenderUnit>& rdum = entsys.getManager<RenderUnit>();
	Manager<LightUnit>& lium = entsys.getManager<LightUnit>();
	auto& sun_liu = lium.get(lium.getHandle(0));
	auto& sun_scu = sun_liu.scu;

	shadow_obj_ubos.Bind().Orphan();
	shadow_pass_ubo.Bind().Orphan();

	// if sun is shining calc shadows
	if (sun_liu.power > 0.01)
		sun_shadow.enabled = true;
	else
		sun_shadow.enabled = false;

	if (sun_shadow.enabled)
		CascadeShadowPass();

	// Select point lights for shadows
	// doesn't work
	if (lium.size() > 1) {
		shadow_light_sort.clear();
		for (size_t i = 1; i < lium.size(); ++i)
		{
			auto& liu = lium.getRef(i);
			auto& scu = liu.scu;
			float score = glm::distance(cam.scu.position, scu.position) - liu.radius;
			shadow_light_sort.emplace_back(score, 1);
		}
		std::sort(shadow_light_sort.begin(), shadow_light_sort.end()); //, [](const LightKey& a, const LightKey& b) {}
		std::array<std::array<glm::vec4, 6>, 6 * 6> point_frustums;
		std::array<glm::mat4, 6 * 6> point_vps;
		for (uint32_t i = 0; i < max_shadow_cube_maps; ++i) {
			auto& liu = lium.getRef(std::get<1>(shadow_light_sort[i]));
			for (int v = 0; v < 6; ++v) {
				glm::vec3 viewatoff{ (v == 0) - (v == 1), (v == 2) - (v == 3), (v == 4) - (v == 5) };
				glm::vec3 up = { 0, -((v < 2) || (v > 3)), (v == 2) - (v == 3) };
				glm::mat4 view = glm::lookAt(liu.scu.position, liu.scu.position + viewatoff, up);
				glm::mat4 proj = glm::perspective(glm::radians(90.0f), 1.0f, 0.2f, liu.radius * 2);
				point_vps[i * 6 + v] = proj * view;
				FrustumPlanes(point_vps[i * 6 + v], point_frustums[i * 6 + v]);
			}
		}
	}

	if (options.sort_by_object) {
		std::sort(shadow_render_queue.begin(), shadow_render_queue.end(), RenderCommand::ByMaterial);
	}
	else {
		std::sort(shadow_render_queue.begin(), shadow_render_queue.end(), RenderCommand::ByLayer);
	}

	std::vector<uint32_t> inst_indexes;
	// Attempt to batch
	if (!shadow_render_queue.empty()) {
		std::vector<RenderCommand> new_render_queue;
		new_render_queue.push_back(shadow_render_queue[0]);
		inst_indexes.push_back(shadow_render_queue[0].ubo_offset);
		uint32_t latest = 0;
		for (uint32_t i = 1; i < shadow_render_queue.size(); ++i) {
			auto& comm = new_render_queue[latest];
			auto& next = shadow_render_queue[i];
			if (next.mat().shader_prog != comm.mat().shader_prog ||
				next.vao_h != comm.vao_h ||
				next.mesh != comm.mesh ||
				next.layer != comm.layer)
				//next.ubo_offset != comm.ubo_offset + comm.prim_count)
			{
				new_render_queue.push_back(next);
				inst_indexes.push_back(next.ubo_offset);
				++latest;
				continue;
			}
			else {
				inst_indexes.push_back(next.ubo_offset);
				comm.prim_count += next.prim_count;
			}
		}
		std::swap(new_render_queue, shadow_render_queue);
	}

	// instance indexes
	//std::iota(std::begin(inst_indexes), std::end(inst_indexes), 0);
	shinst_ind_buf.Bind().Orphan();
	shinst_ind_buf.SubData(0, inst_indexes);

	// Upload data
	GLintptr last_obj_offset = 0;
	shadow_obj_ubos.Bind();
	shadow_obj_ubos.SubData(0, shadow_obj_blocks);
	for (uint32_t sp = 0; sp < sun_shadow.splits_count; ++sp) {
		shadow_pass_ubo.Bind();
		ShadowPassBlock pass_block = { sun_shadow.casc_mats[sp], this->cam.proj * this->cam.view, sp };
		shadow_pass_ubo.SubData(sizeof(ShadowPassBlock) * sp, sizeof(ShadowPassBlock), &pass_block);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, shadow_depth_fbo);

	glViewport(0, 0, shadow_split_size.x, shadow_split_size.y);
	glEnable(GL_DEPTH_TEST);
	glReadBuffer(GL_NONE);
	glDrawBuffer(GL_NONE);
	glDepthFunc(GL_LESS);
	glDepthMask(GL_TRUE);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_CLAMP);
	glCullFace(GL_BACK);

	glPatchParameteri(GL_PATCH_VERTICES, 4);

	if (options.layered_shadow_maps) {
		glClear(GL_DEPTH_BUFFER_BIT);
	}

	shadow_obj_ubos.BindRange(1, 0, sizeof(ShadowObjectBlock) * shadow_obj_blocks.size());

	// layer number can be zero so lets use overflown -1
	GLuint active_layer = (GLuint)-1;
	GLuint active_texture = (GLuint)-1;
	frame.renderer.shadow_draw_calls = shadow_render_queue.size();
	int32_t base_inst = 0;
	for (auto& comm : shadow_render_queue)
	{
		auto& mb = mesh_descs[comm.mesh];
		auto& mat = comm.mat();
		if (!options.layered_shadow_maps && comm.layer != active_layer) {
			shadow_pass_ubo.BindRange(0, sizeof(ShadowPassBlock) * comm.layer, sizeof(ShadowPassBlock));
			//glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, comm.texture, 0, comm.layer);
			glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, sun_shadow.depth_tex, 0, comm.layer);
			glClear(GL_DEPTH_BUFFER_BIT);
			active_layer = comm.layer;
		}
		else if (comm.layer != active_layer) {
			//if (comm.texture != active_texture) {
				glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, sun_shadow.depth_tex, 0);
			//}
			shadow_pass_ubo.BindRange(0, sizeof(ShadowPassBlock) * comm.layer, sizeof(ShadowPassBlock));
			active_layer = comm.layer;
		}
		//const auto& draw_c = comm.draw_c;
		const DrawCommand draw_c = {mb.count, comm.prim_count, mb.first_index, mb.posvert_off, (uint32_t)base_inst };
		gl.Bind(mat.shader_prog);
		gl.Bind(comm.vao_h);
		int t = 0;
		for (auto* ptex : mat.textures)
		{
			auto& tex = *ptex;
			gl.Bind(t++, tex);
		}


		glDrawElementsInstancedBaseVertexBaseInstance(mb.mode, draw_c.count, GL_UNSIGNED_SHORT, (void*)(GLint64)draw_c.first_index, draw_c.prim_count, draw_c.base_vertex, draw_c.base_instance);
		base_inst += draw_c.prim_count;
	}
	//std::cout << "Shadow " << glGetError() << " " << glGetError() << "\n";
	glDisable(GL_DEPTH_CLAMP);
	//GL_ERROR()
	if (options.exponential_shadows) { // Filtering the shadow maps
		// Blur
		glBindFramebuffer(GL_FRAMEBUFFER, shadow_fbo);
		glDrawBuffer(GL_COLOR_ATTACHMENT0);
		glViewport(0, 0, shadow_downsample_size.x, shadow_downsample_size.y);
		//glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
		gl.Bind(screen_vao);

		shadow_blur_ubo.Bind();
		for (uint32_t sp = 0; sp < sun_shadow.splits_count; ++sp) {
			ShadowBlurBlock sbbv = { { 1.0f / shadow_downsample_size.x, 0.0f }, sp };
			ShadowBlurBlock sbbh = { { 0.0f, 1.0f / shadow_downsample_size.y }, sp };
			shadow_blur_ubo.SubData(sp * sizeof(ShadowBlurBlock), sizeof(ShadowBlurBlock), &sbbv);
			shadow_blur_ubo.SubData((sp + sun_shadow.splits_count) * sizeof(ShadowBlurBlock), sizeof(ShadowBlurBlock), &sbbh);
		}

		gl.Bind(shadow_blur_sp);
		gl.Bind(0, sun_shadow.depth_tex);
		//shadow_pixel_offset_uni.Update(1.0f / shadow_downsample_size.x, 0.0f);
		for (uint32_t sp = 0; sp < sun_shadow.splits_count; ++sp) {
			glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, shadow_tex_blur_pre, 0, sp);
			shadow_blur_ubo.BindRange(0, sp * sizeof(ShadowBlurBlock), sizeof(ShadowBlurBlock));
			glDrawArrays(GL_TRIANGLES, 0, 6);
		}

		glDrawBuffer(GL_COLOR_ATTACHMENT1);
		//shadow_pixel_offset_uni.Update(0.0f, 1.0f / shadow_downsample_size.y);
		shadow_tex_blur_pre.Bind();
		for (uint32_t sp = 0; sp < sun_shadow.splits_count; ++sp) {
			glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, sun_shadow.blurred_tex, 0, sp);
			shadow_blur_ubo.BindRange(0, (sp + sun_shadow.splits_count) * sizeof(ShadowBlurBlock), sizeof(ShadowBlurBlock));
			glDrawArrays(GL_TRIANGLES, 0, 6);
		}
	}
}

void RenderSystem::Render(Frame& frame, const RenderSystemOptions& opts)
{
	options = opts;
	Manager<CameraUnit>& cmum = entsys.getManager<CameraUnit>();
	Manager<SceneUnit>& scum = entsys.getManager<SceneUnit>();
	Manager<RenderUnit>& rdum = entsys.getManager<RenderUnit>();
	cam.cmu = cmum.getRef(0);
	auto hcam = cmum.getHandle(0);
	cam.scu = entsys.getUnit<SceneUnit>(hcam);
	cam.proj = glm::perspective(cam.cmu.fovy, cam.cmu.ratio, cam.cmu.nearp, cam.cmu.farp);
	cam.view = glm::lookAt( cam.scu.position, cam.scu.position + cam.scu.fwd(), cam.scu.up());
	if (!options.is_camera_frozen)
		freeze_cam = cam;
	FrustumPlanes(freeze_cam.proj * freeze_cam.view, cam.planes);

	// Recalculate AABBs
	{
		MICROPROFILE_SCOPEI("Render", "AABBs", 0xff00ff);
		for (size_t i = 0; i < scum.size(); ++i)
		{
			auto eh = scum.getHandle(i);
			SceneUnit& scu = scum.getRef(i);
			if (!rdum.find(eh))
				continue;
			RenderUnit& rdu = rdum.get(eh);
			glm::mat4 model = glm::translate(scu.position) * glm::mat4_cast(scu.orientation) * glm::scale(scu.scale);
			glm::mat4 abs_model{ glm::abs(model[0]),glm::abs(model[1]),glm::abs(model[2]),glm::abs(model[3]) };
			auto& mb = mesh_descs[rdu.mesh_desc];
			scu.center = model * glm::vec4{ mb.center, 1.0 };
			//scu.extent = glm::abs(model) * glm::vec4{scu.extent, 0.0};
			scu.extent = abs_model * glm::vec4{ mb.extent, 0.0 };
		}
	}

	if (options.exponential_shadows != switched_to_exponential) {
		if (options.exponential_shadows) {
			sun_shadow.depth_tex.Bind().Param(GL_TEXTURE_COMPARE_MODE, GL_NONE);
			light_compute_sp = ShaderProgram(load_file("res/shaders/light_compute.glsl"), { {"EXP_SHADOWS", ""} });
		}
		else {
			sun_shadow.depth_tex.Bind().Param(GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
			light_compute_sp = ShaderProgram(load_file("res/shaders/light_compute.glsl"), { {"PCF_SHADOWS", ""} });
		}
		switched_to_exponential = !switched_to_exponential;
	}

	if (options.layered_shadow_maps != switched_to_layered) {
		if (options.layered_shadow_maps) {
			// Enable geometry shader if needed
			if (frame.glob.rend.extensions.count(GLextension::GL_ARB_shader_viewport_layer_array)) {
				shadow_sp = ShaderProgram({ GL_VERTEX_SHADER, GL_FRAGMENT_SHADER }, load_file("res/shaders/shadow.glsl"));
			}
			else {
				shadow_sp = ShaderProgram(load_file("res/shaders/shadow.glsl"));
			}

			shadow_depth_fbo.Bind();
			glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, sun_shadow.depth_tex, 0);
			//shadow_fbo.Bind();
			//glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, shadow_tex_blur_pre, 0);
			//glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, sun_shadow.blurred_tex, 0);
		}
		else {
			// Disable geometry shader
			shadow_sp = ShaderProgram({ GL_VERTEX_SHADER, GL_FRAGMENT_SHADER }, load_file("res/shaders/shadow.glsl"));
		}
		switched_to_layered = !switched_to_layered;
	}
	BucketDraws();
	TesselationPass();
	frame.profiler.begin_cgpu("Render/ShadowPass");
	ShadowPass(frame);
	frame.profiler.end_cgpu("Render/ShadowPass");
	frame.profiler.begin_cgpu("Render/GeometryPass");
	GeometryPass(frame);
	frame.profiler.end_cgpu("Render/GeometryPass");
	frame.profiler.begin_cgpu("Render/LightPass");
	LightPass(frame);
	frame.profiler.end_cgpu("Render/LightPass");
	ForwardPass();

	PostProcessPass();
}

void RenderSystem::TesselationPass()
{

}

void RenderSystem::GeometryPass(Frame& frame)
{
	Manager<SceneUnit>& scum = entsys.getManager<SceneUnit>();
	Manager<RenderUnit>& rdum = entsys.getManager<RenderUnit>();

	GLintptr obj_ubo_offset = 0;

	std::vector<RenderCommand> new_render_queue;
	assert(!render_queue.empty());
	new_render_queue.push_back(render_queue[0]);
	uint32_t latest = 0;
	for (uint32_t i = 1; i < render_queue.size(); ++i) {
		auto& comm = new_render_queue[latest];
		auto& next = render_queue[i];
		if (next.mat().shader_prog != comm.mat().shader_prog ||
			next.vao_h != comm.vao_h ||
			next.mesh != comm.mesh ||
			next.ubo_offset != comm.ubo_offset + comm.prim_count) {
			new_render_queue.push_back(next);
			++latest;
			continue;
		}
		else {
			comm.prim_count += next.prim_count;
		}
	}
	std::swap(new_render_queue, render_queue);
	object_blocks_ubo.Bind().Orphan();
	object_blocks_ubo.MultiSubDataAligned(0, render_queue, object_blocks);

	// instance indexes
	std::vector<uint32_t> inst_indexes(2048);
	std::iota(std::begin(inst_indexes), std::end(inst_indexes), 0);
	inst_ind_buf.Bind().Orphan();
	inst_ind_buf.SubData(0, inst_indexes);

	glViewport(0, 0, window.size().x, window.size().y);
	glBindFramebuffer(GL_FRAMEBUFFER, gbuf);
	std::array<GLenum, 2> draw_buffers = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	glDrawBuffers(draw_buffers.size(), draw_buffers.data());
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);
	glCullFace(GL_BACK);
	// Clear the screen to black
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPatchParameteri(GL_PATCH_VERTICES, 4);

	static float param0 = 0.1f;
	ImGui::SliderFloat("param0", &param0, 0.0f, 1.0f, "exp = %.2f");
	GeomPassBlock geom_pass_block = { cam.proj * cam.view, {}, {}, glm::vec4{ param0 } };
	geom_pass_ubo.BindBase(0).Orphan();
	geom_pass_ubo.SubData(0, sizeof(GeomPassBlock), &geom_pass_block);

	frame.renderer.geom_draw_calls = render_queue.size();
	auto base_instance = 0;
	for (auto& comm : render_queue)
	{
		auto& mb = mesh_descs[comm.mesh];
		auto& mat = comm.mat();
		//const auto& draw_c = comm.draw_c;
		const DrawCommand draw_c = { mb.count, comm.prim_count, mb.first_index, mb.posvert_off, 0 };
		base_instance += draw_c.prim_count;
		gl.Bind(mat.shader_prog);
		gl.Bind(comm.vao_h);
		int t = 0;
		for (auto* ptex : mat.textures)
		{
			auto& tex = *ptex;
			gl.Bind(t++, tex);
		}

		object_blocks_ubo.BindRange(1, comm.ubo_offset, sizeof(ObjectBlock) * draw_c.prim_count);

		glDrawElementsInstancedBaseVertexBaseInstance(mb.mode, draw_c.count, GL_UNSIGNED_SHORT, (void*)(GLint64)draw_c.first_index, draw_c.prim_count, draw_c.base_vertex, draw_c.base_instance);
		base_instance++;
	}
}

void RenderSystem::LightPass(Frame& frame)
{
	Manager<RenderUnit>& rdum = entsys.getManager<RenderUnit>();
	Manager<LightUnit>& lium = entsys.getManager<LightUnit>();
	auto& sun_liu = lium.get(lium.getHandle(0));
	auto& sun_scu = sun_liu.scu;

	// if the sun changed refresh irradiance
	if (global_orientation != sun_scu.orientation) {
		ProbePass();
		global_orientation = sun_scu.orientation;
	}

	// Can sample + test at same time?
	// Need to swap for different DEPTH BUFFER!!!
	glViewport(0, 0, window.size().x, window.size().y);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, gbuf);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, lbuf);
	glBlitFramebuffer(0, 0, gbuf_size.x, gbuf_size.y, 0, 0, gbuf_size.x, gbuf_size.y, GL_DEPTH_BUFFER_BIT, GL_NEAREST);
	lbuf.Bind();
	glDrawBuffer(GL_COLOR_ATTACHMENT1);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_GREATER);
	glDepthMask(GL_FALSE);

	gl.Bind(0, gbuf_depth_tex);
	gl.Bind(1, gbuf_normal_tex);
	gl.Bind(2, gbuf_albedo_tex);
	gl.Bind(3, sun_shadow.depth_tex);
	gl.Bind(5, sun_shadow.blurred_tex);
	gl.Bind(4, irrad_probe_hdr_tex);
	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

	// We don't need to clear if we overwrite everything
	//glClear(GL_COLOR_BUFFER_BIT);

	lights.clear();
	// Skip sun (ligth 0)
	// Loop over lights, cull and build tile light list
	for (size_t i = 1; i < lium.size(); ++i)
	{
		auto& liu = lium.getRef(i);
		auto& scu = liu.scu;

		if (FrustumCullSphere(cam.planes, scu.position, liu.radius))
			lights.push_back({ glm::vec4{ liu.color,liu.power }, scu.position, liu.radius });
	}

	SunLight sun_light = { glm::vec4{ sun_liu.color, sun_liu.power }, glm::vec4(glm::normalize(-sun_scu.fwd()), 1.0) };
	for (int i = 0; i < 8; ++i) {
		sun_light.vps[i] = bias_matrix * sun_shadow.casc_mats[i];
		sun_light.splits[i].x = sun_shadow.splits[i];
		sun_light.scales[i].x = sun_shadow.scales[i];
	}
	sun_light.max_split = sun_shadow.splits_count - 1;
	sun_light.slider = options.slider;

	bool compute = true;
	if (compute) {
		LightComputeBlock light_compute_block = { glm::inverse(cam.proj * cam.view),  glm::inverse(cam.proj), cam.proj, cam.view, { cam.cmu.nearp, (cam.cmu.farp - cam.cmu.nearp) / cam.cmu.farp, cam.cmu.farp },
			cam.scu.position, window.size(), (uint32_t)lights.size(), sun_light};
		light_pass_ubo.Bind().Orphan();
		light_pass_ubo.SubData(0, sizeof(LightComputeBlock), &light_compute_block);
	}
	light_pass_ubo.BindBase(0);
	if (lights.size() > 0) {
		light_ubo.Bind().Orphan();
		light_ubo.SubData(0, lights);
		light_ubo.BindRange(1, 0, lights.size() * sizeof(LightBlock));
	}

	if (compute) {
		//glMemoryBarrier(GL_ALL_BARRIER_BITS);
		gl.Bind(light_compute_sp);
		glBindImageTexture(0, lbuf_color_tex, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA16F);
		//glBindImageTexture(1, gbuf_depth_tex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32F);
		glDispatchCompute(window.size().x / 16, window.size().y / 16, 1);
		//glMemoryBarrier(GL_ALL_BARRIER_BITS);
	}

	glDisable(GL_BLEND);

	glm::mat4 vp_inv = glm::mat4{ glm::mat3{ glm::inverse(cam.view) } } * glm::inverse(cam.proj);

	// render sky
	SkyPassBlock sky_pass = { vp_inv, glm::normalize(-sun_scu.fwd()), cam.scu.position };
	sky_pass_ubo.BindBase(0);
	sky_pass_ubo.SubData(0, sizeof(SkyPassBlock), &sky_pass);
	glDepthFunc(GL_LEQUAL);
	gl.Bind(sky_shader);
	gl.Bind(screen_vao);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}

void RenderSystem::ForwardPass()
{
	Manager<SceneUnit>& scum = entsys.getManager<SceneUnit>();
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_ONE, GL_ONE);
	//glBlendEquation(GL_FUNC_ADD);

	fwdobject_blocks.clear();
	forward_queue.clear();
	glm::mat4 model;

	if (options.is_camera_frozen) {
		for (uint32_t sp = 0; sp < sun_shadow.splits_count; ++sp) {
			glm::mat4 freeze_proj = glm::perspective(freeze_cam.cmu.fovy, freeze_cam.cmu.ratio, freeze_cam.cmu.nearp, freeze_cam.cmu.nearp + (freeze_cam.cmu.farp - freeze_cam.cmu.nearp) * sun_shadow.splits[sp]);
			model = glm::inverse(freeze_proj * freeze_cam.view);
			fwdobject_blocks.push_back({ model,{ 3,0,0 }, 0.01f });
			model = glm::inverse(sun_shadow.casc_mats[sp]);
			fwdobject_blocks.push_back({ model,{0,3,0}, 0.01f });
			model = glm::translate((glm::vec3)(glm::inverse(sun_shadow.casc_mats[sp]) * glm::vec4(0, 0, 0, 1))) * glm::scale(glm::vec3{ 0.1f });
			fwdobject_blocks.push_back({ model,{0,0,2}, 0.01f });
		}
		GLuint count = 3 * (GLuint)sun_shadow.splits_count;
		auto& mb = mesh_descs[ndc_meshi];
		auto& mbd = mesh_buff_descs[mb.mesh_bd];
		forward_queue.push_back({
			&forward_mat,
			(int32_t)shadow_obj_blocks.size() - (int32_t)count,
			mbd.vao.raw(),
			ndc_meshi,
			count,
			0
		});
	}

	if (!lights.empty()) {
		for (uint32_t i = 0; i < lights.size(); ++i) {
			model = glm::translate(lights[i].pos_w) * glm::scale(glm::vec3{ 0.1f });
			fwdobject_blocks.push_back({ model, {lights[i].color * lights[i].color.w}, 0.005f });
		}
		//forward_queue.push_back({ forward_wire_sp, 0, (GLsizei)lights.size() - 1, &sphere_mesh });
		GLuint count = (GLuint)lights.size() - 1;
		auto& mb = mesh_descs[sphere_meshi];
		auto& mbd = mesh_buff_descs[mb.mesh_bd];
		forward_queue.push_back({
			&forward_mat,
			(int32_t)shadow_obj_blocks.size() - (int32_t)count,
			mbd.vao.raw(),
			sphere_meshi,
			count,
			0
		});
	}

	forward_obj_ubo.Bind().Orphan();
	forward_obj_ubo.MultiSubDataAligned(0, forward_queue, fwdobject_blocks);

	GeomPassBlock geom_pass_block = { cam.proj * cam.view, glm::mat4{} };
	forward_pass_ubo.Bind().BindBase(0).Orphan();
	forward_pass_ubo.SubData(0, sizeof(GeomPassBlock), &geom_pass_block);

	glLineWidth(2.0f);

	for (auto& comm : forward_queue)
	{
		auto& mb = mesh_descs[comm.mesh];
		//const auto& draw_c = comm.draw_c;
		const DrawCommand draw_c = { mb.count, comm.prim_count, mb.posvert_off, mb.first_index, 0 };
		gl.Bind(comm.mat().shader_prog);
		gl.Bind(comm.vao_h);

		forward_obj_ubo.BindRange(1, comm.ubo_offset, sizeof(ObjectBlock) * draw_c.prim_count);

		glDrawElementsInstanced(mb.mode, draw_c.count, GL_UNSIGNED_SHORT, (void*)(GLint64)draw_c.first_index, draw_c.prim_count);
	}

	glDisable(GL_BLEND);
}

void RenderSystem::PostProcessPass()
{
	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	glDisable(GL_DEPTH_TEST);
	gl.Bind(color_post_sp);

	static float exposure = 1.0f;
	ImGui::SliderFloat("slider float", &exposure, 0.0f, 5.0f, "exp = %.2f");
	exposure_uni.Update(exposure);
	gl.Bind(0, lbuf_color_tex);

	gl.Bind(screen_vao);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}

void RenderSystem::FinalPass(FinalPassMode mode)
{
	// Bind default framebuffer and draw contents of our framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	gl.Bind(screen_vao);
	glDisable(GL_DEPTH_TEST);

	switch (mode) {
	case FinalPassMode::Albedo:
		gl.Bind(blit_sp);
		gl.Bind(0, gbuf_albedo_tex);
		break;
	case FinalPassMode::Normal:
		gl.Bind(blit_sp);
		gl.Bind(0, gbuf_normal_tex);
		break;
	case FinalPassMode::Shadow:
		gl.Bind(debug_depth_sp);
		if (options.exponential_shadows)
			gl.Bind(0, sun_shadow.blurred_tex);
		else
			gl.Bind(0, sun_shadow.depth_tex);
			sun_shadow.depth_tex.Param(GL_TEXTURE_COMPARE_MODE, GL_NONE);
		break;
	case FinalPassMode::Final:
	default:
		gl.Bind(blit_sp);
		gl.Bind(0, lbuf_color_ldr_tex);
		break;
	}

	glDrawArrays(GL_TRIANGLES, 0, 6);
	if (!options.exponential_shadows && mode == FinalPassMode::Shadow)
		sun_shadow.depth_tex.Bind().Param(GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
}

RenderSystem::MeshBufferDescriptor& RenderSystem::findMeshBuffer(const VertexFormat& format)
{
	auto it = std::find_if(mesh_buff_descs.begin(), mesh_buff_descs.end(), [&](MeshBufferDescriptor& desc) { return desc.format == format; });
	if (it != std::end(mesh_buff_descs)) {
		return *it;
	}
	else {
		mesh_buff_descs.push_back(MeshBufferDescriptor{
			format,
			{ GL_ARRAY_BUFFER, GL_STATIC_DRAW },
			{ GL_ARRAY_BUFFER, GL_STATIC_DRAW },
			{ GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW },
			{},{},
			0,0,0,
			static_cast<uint32_t>(mesh_buff_descs.size())
		});
		auto& desc = mesh_buff_descs.back();
		uint32_t pos_size = format.attribs[0].size();
		uint32_t ind_size = 2;
		uint32_t vert_size = format.size;
		VertexArray &vao = desc.vao, &posvao = desc.posvao;
		gl.Bind(vao);
		desc.vertbuf.Bind();
		desc.vertbuf.Data(vert_size * 160000, NULL);
		desc.indbuf.Bind();
		desc.indbuf.Data(ind_size * 240000, NULL);
		size_t attrib_off = 0;
		size_t next_location = 0;
		for (size_t i = next_location; i < next_location + format.attribs.size(); ++i) {
			auto& attrib = format.attribs[i];
			glEnableVertexAttribArray(i);
			glVertexAttribPointer(i, attrib.count, attrib.type, GL_FALSE, format.size, (void*)attrib_off);
			attrib_off += attrib.size();
		}
		next_location += format.attribs.size();

		// For the instancing and multi draw
		inst_ind_buf.Bind();
		glEnableVertexAttribArray(next_location);
		glVertexAttribIPointer(next_location, 1, GL_UNSIGNED_INT, sizeof(uint32_t), 0);
		glVertexAttribDivisor(next_location, 1);
		++next_location;

		gl.Bind(posvao);
		desc.indbuf.Bind();
		desc.posbuf.Bind();
		desc.posbuf.Data(pos_size * 160000, NULL);

		next_location = 0;
		attrib_off = 0;
		for (size_t i = next_location; i < next_location + 1; ++i) {
			auto& attrib = format.attribs[i];
			glEnableVertexAttribArray(i);
			glVertexAttribPointer(i, attrib.count, attrib.type, GL_FALSE, attrib.size(), (void*)attrib_off);
			attrib_off += attrib.size();
		}
		next_location += 1;

		shinst_ind_buf.Bind();
		glEnableVertexAttribArray(next_location);
		glVertexAttribIPointer(next_location, 1, GL_UNSIGNED_INT, sizeof(uint32_t), 0);
		glVertexAttribDivisor(next_location, 1);
		++next_location;

		gl.Bind(VertexArray::Handle{0});
		return desc;
	}
}

inline bool RenderSystem::RenderCommand::ByMaterial(const RenderCommand & a, const RenderCommand & b) {
	if (a.mat().shader_prog == b.mat().shader_prog) {
		if (a.vao_h == b.vao_h) {
			return a.layer < b.layer;
		}
		return a.vao_h < b.vao_h;
	}
	return a.mat().shader_prog < b.mat().shader_prog;
}

inline bool RenderSystem::RenderCommand::ByLayer(const RenderCommand & a, const RenderCommand & b) {
	if (a.layer == b.layer) {
		if (a.mat().shader_prog == b.mat().shader_prog) {
			return a.vao_h < b.vao_h;
		}
		return a.mat().shader_prog < b.mat().shader_prog;
	}
	return a.layer < b.layer;
}
