#pragma once

#include <cstdint>
#include <vector>
#include <unordered_map>
#include <glm/glm.hpp>

#include "render/Buffer.hpp"
#include "render/Shader.hpp"
#include "render/Texture.hpp"
#include "engine/Entity.hpp"

namespace grow
{
	struct Material
	{
		std::reference_wrapper<ShaderProgram> shader_prog;
		std::vector<Texture*> textures;
		glm::vec3 color;
		float smoothness;
	};

	struct RenderUnit
	{
		Material& material;
		//const StaticMesh& mesh;
		uint32_t mesh_desc;
		Material* shadow_material;
		//RenderUnit(Material& mat, StaticMesh& mesh) : material(mat), mesh(mesh) {}
	};

	struct LightUnit
	{
		glm::vec3 color;
		float power;
		float radius;
		SceneUnit scu;
	};

	struct CameraUnit
	{
		float fovy;
		float ratio;
		float nearp;
		float farp;
	};

	template<typename T>
	class VectorManager
	{
		std::vector<T> units_;
		std::vector<Entity::Handle> handles_;
		std::unordered_map<Entity::Handle, typename std::vector<T>::size_type> references_;
	public:
		typedef typename std::vector<T>::size_type size_type;
		typedef typename std::vector<T>::iterator iterator;

		template<typename... Args>
		const T& emplace(Entity::Handle h, Args&& ... args) { units_.emplace_back(std::forward<Args>(args)...); handles_.emplace_back(h); }

		void add(Entity::Handle h, T&& t) { units_.emplace_back(t); handles_.emplace_back(h); references_[h] = units_.size() - 1; }
		T& get(Entity::Handle h) { return units_[references_[h]]; }
		bool find(Entity::Handle h) { return references_.find(h) != references_.end(); }
		T& getRef(size_type i) { return units_[i]; }
		Entity::Handle getHandle(size_type i) { return handles_[i]; }

		size_type size() { return units_.size(); }
		iterator begin() { return units_.begin(); }
		iterator end() { return units_.end(); }
	};

	template <class T>
	class Manager : public VectorManager<T>
	{};
}