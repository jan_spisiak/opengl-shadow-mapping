#include "render/UserInterface.hpp"

using namespace gl;

void UserInterface::LoadFont(const std::string & filename)
{
	//font_tex.Load(filename);
}

void UserInterface::DrawString(const std::string& str, float x, float y, float size)
{
	font_strings.emplace_back(str);
	font_params.push_back(FontParams({ { x, y }, {size, size} }));
}

void UserInterface::Render()
{
	// Slow? Doesn't matter
	std::string final_str;
	for (auto& s : font_strings) {
		final_str += s;
	}

	font_vbo.Bind();
	font_vbo.Data(final_str.length(), final_str.c_str());

	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, font_tex);

	gl.Bind(font_shader);
	uint32_t offset = 0;
	for (uint32_t i = 0; i < font_strings.size(); ++i)
	{
		gl.Bind(font_vao);
		font_vbo.Bind();
		glm::vec2 origin(font_params[i].position.x / window.size().x * 2.0 - 1.0, font_params[i].position.y / window.size().y * 2.0 - 1.0);
		float sizem = 0.5;
		glm::vec2 size(sizem * 16.0 / window.size().x, sizem * 33.33 / window.size().y);

		font_vertex_offset.Update(offset);
		font_uni_origin.Update(origin);
		font_uni_size.Update(size);

		glDrawArrays(GL_POINTS, offset, font_strings[i].length());
		offset += font_strings[i].length();
	}
	glDisable(GL_BLEND);

	// Clear the draw commands
	font_strings.clear();
	font_params.clear();
}