#pragma once

#include <string>
#include <vector>
#include <memory>

//#include <GL/glew.h>
//#include <GL/gl.h>
#include <glbinding/gl/gl.h>
#include <glm/glm.hpp>
#include <util/Util.hpp>

using namespace gl;

class Texture
{
public:
	using Handle = strong_type<Texture, gl::GLuint>;
protected:
	Handle handle;
	GLenum target;
	GLint internal_format;
	glm::ivec3 size;
public:
	Texture(Texture&& other) : handle(other.handle), target(other.target), size(other.size) { other.handle = { 0 }; }
	//Texture() { glGenTextures(1, &handle); }
	Texture(GLenum target) : target(target) { glGenTextures(1, &(handle.value())); }
	Texture(const GLchar* path);
	virtual ~Texture() { glDeleteTextures(1, &(handle.value())); };

	Texture& Bind(GLenum _target) { target = _target; glBindTexture(target, handle.value()); return *this; }
	Texture& Bind() { glBindTexture(target, handle.value()); return *this; }
	const Texture& Bind() const { glBindTexture(target, handle.value()); return *this; }
	Texture& Param(GLenum pname, GLenum param) { glTexParameteri(target, pname, param); return *this; }
	Texture& Param(GLenum pname, GLint param) { glTexParameteri(target, pname, param); return *this; }
	void Image2D(GLint level, GLint internalFormat, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid * data)
	{
		glTexImage2D(target, level, internalFormat, width, height, 0, format, type, data); size = {width, height, 0};
	}
	//void Load(const std::string & file);

	Handle get() const { return handle; }
	operator GLuint() inline const { return handle.value(); }
};

enum class ImageFormat
{
	RGBA8,
	R32F,
};

struct Image
{
	glm::ivec3 size;
	uint8_t channels;
	uint8_t bpp;
	bool is_generated = false;
	bool is_float;
	using data_t = uint8_t;
	std::unique_ptr<data_t, void(*)(data_t*)> data = {nullptr, StbiDeleter};

	bool IsGenerated() { return is_generated; }
	GLenum glSymbFormat();

	template<typename T>
	T fetch(glm::ivec2 offset, int channel)
	{
		assert(offset.x >= 0 && offset.x < size.x && offset.y >= 0 && offset.y < size.y);
		size_t cell_size = channels * sizeof(typename T);
		size_t off = offset.y * size.x + offset.x;
		auto choff = channel * sizeof(typename T);
		//assert(glm::lessThan(offset, size));
		return *(T*)(&*data + cell_size * off + choff);
	};

	Image() {}
	Image(const std::string & path);

	static Image Random(glm::ivec3 size);

private:
	static void StbiDeleter(data_t* addr);
	static void NoiseDeleter(data_t* addr);
};

class ImageManager
{
	std::vector<Image> images_;
public:
};
