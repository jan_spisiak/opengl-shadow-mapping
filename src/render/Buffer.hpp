#pragma once

#include <string>
#include <vector>
#include <iostream>

#include <glbinding/gl/gl.h>
#include <glm/glm.hpp>

#include "util/Util.hpp"
using namespace gl;

class Buffer
{
	gl::GLuint handle = 0;
	gl::GLenum target;
	gl::GLenum usage;
	GLsizeiptr size = 0;
public:
	Buffer(Buffer&& other) : handle(other.handle), target(other.target), usage(other.usage), size(other.size) { other.handle = 0; }
	Buffer(Buffer const& other) = delete;
	Buffer(gl::GLenum target, gl::GLenum usage) : target(target), usage(usage) { gl::glGenBuffers(1, &handle); };
	Buffer(gl::GLenum target, gl::GLsizeiptr size, const gl::GLvoid* data, gl::GLenum usage);
	template<typename T>
		Buffer(gl::GLenum target, const std::vector<T>& buffer, gl::GLenum usage) : Buffer(target, sizeof(T) * buffer.size(), buffer.data(), usage) {};
	Buffer(gl::GLenum target, gl::GLsizeiptr size, gl::GLenum usage) : Buffer(target, size, NULL, usage) {};
	~Buffer() { gl::glDeleteBuffers(1, &handle); }
	operator GLuint() const { return handle; }

	const Buffer& Bind() const { gl::glBindBuffer(target, handle); return *this; };
	void Orphan() const { gl::glBufferData(target, size, NULL, usage); }
	void Data(gl::GLsizeiptr size_, const gl::GLvoid* data) { size = size_; gl::glBufferData(target, size, data, usage); };
	void SubData(gl::GLintptr offset, gl::GLsizeiptr data_size, const gl::GLvoid* data) { assert(offset + data_size <= size); gl::glBufferSubData(target, offset, data_size, data); };
	template<typename T>
		void Data(const std::vector<T>& buffer) { this->Data(sizeof(T) * buffer.size(), buffer.data()); }
	template<typename T>
		void SubData(gl::GLintptr offset, const std::vector<T>& buffer) { this->SubData(offset, sizeof(T) * buffer.size(), buffer.data()); }
	const Buffer& BindBase(gl::GLuint index) const { glBindBufferBase(target, index, handle); return *this; };
	void BindRange(gl::GLuint index, gl::GLintptr offset, gl::GLsizeiptr size) const { assert((target != GL_UNIFORM_BUFFER) || (offset % 256 == 0)); gl::glBindBufferRange(target, index, handle, offset, size); };

	template<typename Q, typename T>
	gl::GLintptr MultiSubDataAligned(gl::GLintptr offset_, std::vector<Q>& queue, const std::vector<T>& buffer)
	{
		gl::GLintptr last_offset = offset_;
		size_t count = 0;
		for (size_t i = 0; i < queue.size(); ++i)
		{
			gl::GLintptr offset = last_offset;
			int remainder = offset % 256;
			offset += ((remainder != 0) ? (256 - remainder) : 0);
			this->SubData(offset, sizeof(T) * queue[i].prim_count, &buffer[count]);
			last_offset = offset + sizeof(T) * queue[i].prim_count;
			count += queue[i].prim_count;
			queue[i].ubo_offset = (int32_t)offset;
		}
		return last_offset;
	}
};

struct MeshVertex
{
	glm::vec3 pos;
	glm::u16vec3 normal;
	glm::u16vec3 tangent;
	glm::u16vec2 uv;

	static void BindAttribs();
};

struct PosVertex
{
	glm::vec3 pos;

	PosVertex(const glm::vec3& ref) : pos(ref) {};

	static void BindAttribs();
};

class VertexArray
{
public:
	using Handle = strong_type<VertexArray, GLuint>;
	using Short = narrow_type<VertexArray, Handle, uint16_t>;
protected:
	Handle handle;
public:
	VertexArray(VertexArray&& other) : handle(other.handle) { other.handle = { 0 }; }
	VertexArray() { glGenVertexArrays(1, &handle.value()); }
	VertexArray(const VertexArray& other) = delete;
	~VertexArray() { glDeleteVertexArrays(1, &handle.value()); }
	Handle raw() const { return handle; }
	//operator GLuint() const { return handle.value(); }
protected:
	friend class Context;
	void Bind() const { glBindVertexArray(handle.value()); }
	static void Bind(Handle handle) { glBindVertexArray(handle.value()); }
	static void Unbind() { glBindVertexArray(0); }
};

class FrameBuffer
{
	GLuint handle;
	GLenum target;
public:
	FrameBuffer(FrameBuffer&& other) : handle(other.handle), target(other.target) { other.handle = 0; }
	FrameBuffer() { glGenFramebuffers(1, &handle); }
	~FrameBuffer() { glDeleteFramebuffers(1, &handle); }
	operator GLuint() const { return handle; }

	FrameBuffer& Bind() { assert(target != 0); glBindFramebuffer(target, handle); return *this; }
	FrameBuffer& Bind(GLenum target_) { target = target_; glBindFramebuffer(target, handle); return *this; }
};

class RenderBuffer
{
	GLuint handle;
public:
	RenderBuffer(RenderBuffer&& other) : handle(other.handle) { other.handle = 0; }
	RenderBuffer() { glGenRenderbuffers(1, &handle); }
	~RenderBuffer() { glDeleteRenderbuffers(1, &handle); }
	operator GLuint() const { return handle; }
};

struct AttribFormat {
	GLint count;
	GLenum type;

	uint32_t size() const {
		return count * Sizeof(type);
	}

	static uint32_t Sizeof(GLenum type);
};
bool operator==(const AttribFormat& lhs, const AttribFormat& rhs);

struct VertexFormat {
	std::vector<AttribFormat> attribs;
	uint32_t size;

	static VertexFormat DefaultFormat() {
		return {{{3, GL_FLOAT},{3, GL_HALF_FLOAT},{3, GL_HALF_FLOAT},{2, GL_HALF_FLOAT}},
			3*AttribFormat::Sizeof(GL_FLOAT) + 8* AttribFormat::Sizeof(GL_HALF_FLOAT) };
	}
};
bool operator==(const VertexFormat& lhs, const VertexFormat& rhs);

template<typename T>
struct VertexStorage
{
	GLenum mode;
	glm::vec3 center;
	glm::vec3 extent;
	std::vector<glm::vec3> positions;
	std::vector<T> vertices;
	// Max 65k verts
	std::vector<uint16_t> indices;

	bool Load(const std::string& filename, uint32_t id);
	VertexStorage(const std::string& filename, uint32_t id) : mode(GL_TRIANGLES) { Load(filename, id); }
	VertexStorage(gl::GLenum mode_, const std::vector<T>& vert_vec, const std::vector<uint16_t>& ind_vec,
		glm::vec3 const& cnt = glm::vec3{ 0.0f }, glm::vec3 const& ext = glm::vec3{0.0f})
		: mode(mode_),
		center(cnt),
		extent(ext),
		vertices(vert_vec),
		indices(ind_vec)
	{
		for (auto& vert : vert_vec) {
			positions.push_back(vert.pos);
		}
	}

	uint32_t VertexSize() {
		return sizeof(T);
	}
};

struct StaticMesh
{
	glm::vec3 center;
	glm::vec3 extent;
	Buffer posbuff_;
	Buffer vertbuff_;
	Buffer indibuff_;
	size_t indi_size;
	VertexArray vao_;
	VertexArray pos_vao_;
	GLenum mode_;

	//StaticMesh(VertexStorageI* vsi);
	template<typename T>
	StaticMesh(const VertexStorage<T>& storage);
	StaticMesh(StaticMesh&& other) = delete;//: posbuff_(std::move(other.posbuff_)), vertbuff_(std::move(other.vertbuff_)), indibuff_(std::move(other.indibuff_)), vao_(std::move(other.vao_)) {}
	StaticMesh(const StaticMesh& other) = delete;
	//void Draw() const { vao_.Bind(); glDrawElements(mode_, indices_.size(), GL_UNSIGNED_SHORT, NULL); }
	//void PosDraw() const { pos_vao_.Bind(); glDrawElements(mode_, indices_.size(), GL_UNSIGNED_SHORT, NULL); }
};

template<typename T>
StaticMesh::StaticMesh(const VertexStorage<T>& storage) :
	posbuff_(GL_ARRAY_BUFFER, GL_STATIC_DRAW),
	vertbuff_(GL_ARRAY_BUFFER, GL_STATIC_DRAW),
	indibuff_(GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW),
	indi_size(storage.indices.size()),
	mode_(storage.mode),
	center(storage.center), extent(storage.extent)
{
	vao_.Bind();
	vertbuff_.Bind();
	vertbuff_.Data(storage.vertices);
	T::BindAttribs();
	indibuff_.Bind();
	indibuff_.Data(storage.indices);

	pos_vao_.Bind();
	indibuff_.Bind();
	posbuff_.Bind();
	posbuff_.Data(storage.positions);
	PosVertex::BindAttribs();
	VertexArray::Unbind();
}